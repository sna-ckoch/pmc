/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 * 
 * Script brief description: 
   Query ServicePro API for appointment info related to an order and upsert custom records
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/12/02                           ckoch           Moved appt upsert to module
 * 2020/11/25                           ckoch           Initial version
 *
 */
define(['N/http', 'N/record', 'N/redirect', './sna_pmc_mod_sp10_globals.js'], function (http, record, redirect, globals) {

    function onRequest(context) {
        log.debug({ title: 'REQUEST', details: JSON.stringify(context) });

        if (context.request.method == http.Method.GET) {
            handleGet(context);
        } else {
            context.response.write({ output: 'Request not supported' });
        }
    }

    function handleGet(context) {
        var nsorderid = context.request.parameters['custpage_nsorderid'] || null;
        var sporderid = context.request.parameters['custpage_sporderid'] || null;

        if (nsorderid == null || sporderid == null) {
            context.response.write({ output: 'Invalid request' });
        } else {
            syncAppointments(nsorderid, sporderid);

            redirect.toRecord({
                type: record.Type.SALES_ORDER,
                id: nsorderid
            });
        }
    }

    function syncAppointments(nsorderid, sporderid) {
        var appointments = null;

        try {
            appointments = globals.callapi('/Appointment?$filter=OrderId eq ' + sporderid + '&$expand=AppointmentStatus,AssignedTech', true, 'get').d.results;
            log.debug({ title: 'APPOINTMENTS', details: JSON.stringify(appointments) });
        } catch (e) {
            log.error({
                title: 'API_ERROR',
                details: JSON.stringify({
                    nsorderid: nsorderid,
                    sporderid: sporderid,
                    e: JSON.stringify(e)
                })
            });
        }

        if (util.isArray(appointments)) {
            for (var i in appointments) {
                var recid = globals.upsertAppointment(appointments[i], nsorderid);
                log.debug({ title: 'RECID', details: recid });
            }
        }
    }

    return {
        onRequest: onRequest
    }

});