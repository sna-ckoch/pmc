/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Syncs Appointment records with Events
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/02/05                           ckoch           Initial version
 * 2021/04/30                           ckoch           Support for location mapping from custom appt field
 *
 */
define(['N/record', 'N/search', 'N/runtime', 'N/format', './sna_pmc_mod_sp10_globals.js'],
    function (record, search, runtime, format, spGlobals) {

        function afterSubmit(context) {
            var events = {};
            events[context.UserEventType.CREATE] = syncEvent;
            events[context.UserEventType.EDIT] = syncEvent;

            if (typeof events[context.type] === 'function') {
                events[context.type](context);
            }
        }

        function syncEvent(context) {
            var PARAM_RESOURCE = runtime.getCurrentScript().getParameter({ name: 'custscript_sna_pmc_event_resource' }) || null;
            var PARAM_OWNER = runtime.getCurrentScript().getParameter({ name: 'custscript_sna_pmc_event_organizer' }) || null;

            if (PARAM_RESOURCE == null || PARAM_OWNER == null) {
                log.error({
                    title: 'SYNC_EVENT',
                    details: 'Missing required parameters. Exiting'
                });
                return;
            }

            var eventRec = null;
            var origEventId = null;

            var apptRec = record.load({
                type: context.newRecord.type,
                id: context.newRecord.id,
                isDynamic: true
            });

            var soId = apptRec.getValue({
                fieldId: spGlobals.records.appointment.fields.nsSalesOrder
            });
            //if (soId == null || soId == '') return;

            var scheduledDate = apptRec.getValue({
                fieldId: spGlobals.records.appointment.fields.ScheduledDateTime
            }) || null;
            log.debug({ title: 'scheduledDate.orig', details: scheduledDate });

            if (!util.isDate(scheduledDate)) return;

            var tempDate = format.format({
                value: scheduledDate,
                type: format.Type.DATETIME,
                timezone: format.Timezone.AMERICA_NEW_YORK
            });
            scheduledDate = new Date(tempDate);
            log.debug({ title: 'scheduledDate.fixed', details: scheduledDate });

            if (!util.isDate(scheduledDate)) return;

            var scheduledDuration = fixnum(apptRec.getValue({
                fieldId: spGlobals.records.appointment.fields.ScheduledDuration
            }));
            log.debug({ title: 'scheduledDuration', details: scheduledDuration });

            try {
                origEventId = apptRec.getValue({
                    fieldId: spGlobals.records.appointment.fields.nsLinkedEvent
                }) || null;

                if (origEventId == null) {
                    var assignedTech = apptRec.getValue({
                        fieldId: spGlobals.records.appointment.fields.nsAssignedTech
                    }) || null;
                    log.debug({ title: 'assignedTech', details: assignedTech });
                    var leadTech = apptRec.getValue({
                        fieldId: spGlobals.records.appointment.fields.nsLeadTech
                    }) || null;
                    log.debug({ title: 'leadTech', details: leadTech });

                    if (assignedTech != leadTech) {
                        log.debug({title: 'Tech Check', details: 'Skipping event creation, assigned tech != lead tech'});
                        return;
                    }

                    eventRec = record.create({
                        type: record.Type.CALENDAR_EVENT,
                        isDynamic: true
                    });

                    eventRec.setValue({
                        fieldId: 'organizer',
                        value: PARAM_OWNER
                    }).setValue({
                        fieldId: 'owner',
                        value: PARAM_OWNER
                    }).setValue({
                        fieldId: 'accesslevel',
                        value: 'PUBLIC'
                    });

                    var soCustomer = getOrderCustomer(soId);

                    if (soCustomer != null) {
                        eventRec.setValue({
                            fieldId: 'company',
                            value: soCustomer
                        }).setValue({
                            fieldId: 'transaction',
                            value: soId
                        });
                    }
                } else {
                    eventRec = record.load({
                        type: record.Type.CALENDAR_EVENT,
                        id: origEventId,
                        isDynamic: true
                    });
                }
            } catch (e) {
                log.error({
                    title: 'SYNC_EVENT',
                    details: JSON.stringify(e)
                });
            }

            if (eventRec == null) return;

            eventRec.setValue({
                fieldId: 'title',
                value: getEventTitle(apptRec)
            }).setValue({
                fieldId: 'startdate',
                value: scheduledDate
            }).setValue({
                fieldId: 'starttime',
                value: scheduledDate
            }).setValue({
                fieldId: 'custevent_sna_pmc_sp10_linked_appt',
                value: apptRec.id
            });

            var eventStatus = apptRec.getText({
                fieldId: 'custrecord_sna_pmc_appt_event_status'
            }) || null;
            if (origEventId != null && eventStatus != null) {
                eventRec.setValue({
                    fieldId: 'status',
                    value: eventStatus
                });
            }

            if (util.isDate(scheduledDate)) {
                // cap calculated end date at midnight of scheduled day
                var midnight = new Date(scheduledDate);
                midnight.setHours(23, 59, 59, 0);
                log.debug({ title: 'midnight', details: midnight });

                var scheduledEnd = new Date();
                scheduledEnd.setTime(scheduledDate.getTime() + (scheduledDuration * 60 * 60 * 1000));
                log.debug({ title: 'scheduledEnd', details: scheduledEnd });

                var finalEndDate = new Date(Math.min(midnight, scheduledEnd));
                log.debug({ title: 'finalEndDate', details: finalEndDate });

                //var diffMins = Math.round((((finalEndDate - scheduledDate) % 86400000) % 3600000) / 60000)
                //log.debug({ title: 'diffMins', details: diffMins });

                //if (diffMins > 0) {
                eventRec.setValue({
                    fieldId: 'endtime',
                    value: finalEndDate
                });
                //}
            }

            var resourceCount = eventRec.getLineCount({sublistId: 'resource'});
            for (var i = 0; i < resourceCount; i++) {
                eventRec.removeLine({
                    sublistId: 'resource',
                    line: 0
                });
            }

            eventRec.selectNewLine({
                sublistId: 'resource'
            }).setCurrentSublistValue({
                sublistId: 'resource',
                fieldId: 'resource',
                value: PARAM_RESOURCE
            }).commitLine({
                sublistId: 'resource'
            });

            var locationName = apptRec.getValue({
                fieldId: spGlobals.records.appointment.fields.nsLocation
            }) || null;

            var locationResource = getLocationResource(soId, locationName);

            if (locationResource != null) {
                eventRec.selectNewLine({
                    sublistId: 'resource'
                }).setCurrentSublistValue({
                    sublistId: 'resource',
                    fieldId: 'resource',
                    value: locationResource
                }).commitLine({
                    sublistId: 'resource'
                });
            }

            var eventId = null;

            try {
                eventId = eventRec.save();
            } catch (e) {
                log.error({
                    title: 'SYNC_EVENT',
                    details: JSON.stringify(e)
                });
            }

            if (eventId != null && eventId != origEventId) {
                var values = {};
                values[spGlobals.records.appointment.fields.nsLinkedEvent] = eventId;

                record.submitFields({
                    type: apptRec.type,
                    id: apptRec.id,
                    values: values
                });
            }
        }

        function getLocationResource(soId, locationName) {
            var output = null;

            log.debug('soId', soId);
            log.debug('locationName', locationName);

            var orderLocation = null;

            try {
                if (fixnum(soId) > 0) {
                    orderLocation = search.lookupFields({
                        type: search.Type.SALES_ORDER,
                        id: soId,
                        columns: ['location']
                    })['location'][0].value;
                }
            } catch (e) {
                log.debug ('getLocationResource', e);
            }

            try {
                if (orderLocation == null && locationName != null) {
                    search.create({
                        type: search.Type.LOCATION,
                        filters: [
                            ['name', 'is', locationName]
                        ],
                        columns: ['internalid']
                    }).run().each(function (result) {
                        orderLocation = result.id;
                        return false;
                    });
                }
            } catch (e) {
                log.debug('getLocationResource', e);
            }

            try {
                output = search.lookupFields({
                    type: search.Type.LOCATION,
                    id: orderLocation,
                    columns: ['custrecord_sna_pmc_sp10_cal_resource_id']
                })['custrecord_sna_pmc_sp10_cal_resource_id'] || null;
            } catch (e) {
                log.error('getLocationResource', e);
            }

            return output;
        }

        function getOrderCustomer(soId) {
            var output = null;

            try {
                output = search.lookupFields({
                    type: search.Type.SALES_ORDER,
                    id: soId,
                    columns: ['entity']
                })['entity'][0].value;
            } catch (e) {

            }

            return output;
        }

        function getEventTitle(rec) {
            var apptOrder = rec.getValue({
                fieldId: spGlobals.records.appointment.fields.nsSalesOrder
            }) || null;
            var apptAssignedTech = rec.getText({
                fieldId: spGlobals.records.appointment.fields.nsAssignedTech
            }) || null;
            var apptPMCTech = rec.getText({
                fieldId: spGlobals.records.appointment.fields.UDF_Total_PMC_Techs
            }) || null;
            var apptSubTech = rec.getText({
                fieldId: spGlobals.records.appointment.fields.UDF_Total_Sub_Techs
            }) || null;
            var apptName = rec.getText({
                fieldId: 'name'
            }) || null;
            var apptDesc = rec.getText({
                fieldId: spGlobals.records.appointment.fields.Description
            }) || null;

            var output = getOrderInfo(apptOrder);
            if (output == 'Unlinked ServicePro Appointment') {
                if (apptName != null && apptName != '') {
                    output = apptName;
                }
                if (apptDesc != null && apptDesc != null) {
                    if (output == 'Unlinked ServicePro Appointment') {
                        output = apptDesc;
                    } else {
                        output += ' | ' + apptDesc;
                    }
                }
            }

            if (apptAssignedTech != null && apptAssignedTech != '') {
                output += ' | ' + apptAssignedTech;
            }

            if (apptPMCTech != null && apptPMCTech != '') {
                output += ' | PMC Techs: ' + apptPMCTech;
            }

            if (apptSubTech != null && apptSubTech != '') {
                output += ' | Sub Techs: ' + apptSubTech;
            }

            return output;
        }

        function getOrderInfo(soId) {
            var output = 'Unlinked ServicePro Appointment';

            if (fixnum(soId) > 0) {
                var colInfo = search.createColumn({
                    name: "formulatext",
                    formula: "{number}||' | '||NVL2({jobmain.entityid},{name}||' | '||{jobmain.entityid},{name})"
                });

                search.create({
                    type: search.Type.SALES_ORDER,
                    filters: [
                        ['internalid', 'anyof', soId],
                        'and',
                        ['mainline', 'is', 'T']
                    ],
                    columns: [colInfo]
                }).run().each(function (result) {
                    var temp = result.getValue(colInfo);

                    if (temp != null && temp != '') {
                        output = temp;
                    }

                    return false;
                });
            }

            return output;
        }

        function fixnum(what) {
            return (isNaN(parseFloat(what)) ? 0.00 : parseFloat(what));
        }

        return {
            afterSubmit: afterSubmit
        }

    });