/**
 * Copyright (c) 2021, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Disables description field on lines with certain items (job completion)
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2021/07/13                           ckoch           Initial version
 *
 */
define([], function () {

	var PARAM_ITEMIDS = ['1167561'];

	function pageInit(context) {
		var rec = context.currentRecord;

		for (var i = 0; i < rec.getLineCount({sublistId: 'item'}); i++) {
			console.log('pageInit.line = ' + i);

			toggleDisabled(context, i);
		}
	}

	function lineInit(context) {
		if (context.sublistId == 'item') {
			toggleDisabled(context);
			disableCompletedBy(context);
		}
	}

	function postSourcing(context) {
		if (context.sublistId == 'item' && context.fieldId == 'item') {
			toggleDisabled(context);
		}
	}

	function fieldChanged(context) {
		if (context.sublistId == 'item' && context.fieldId == 'custcol_sna_sp10_tech_entered') {
			disableCompletedBy(context);
		}
	}

	function disableCompletedBy(context) {
		var rec = context.currentRecord;

		var completedBy = rec.getCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_sna_sp10_completed_by'
		}) || null;

		var techEntered = rec.getCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_sna_sp10_tech_entered'
		}) || null;

		disableSublistField(rec, 'custcol_sna_sp10_completed_by', (techEntered != null), 0);
		disableSublistField(rec, 'custcol_pmc_expense_preapproved', (completedBy != null || techEntered == null), 0);
	}

	function toggleDisabled(context, lineId) {
		var rec = context.currentRecord;

		var itemId = null;

		if (lineId) {
			itemId = rec.getSublistValue({
				sublistId: 'item',
				fieldId: 'item',
				line: lineId
			});
		} else {
			itemId = rec.getCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'item'
			});
		}

		disableSublistField(rec, 'description', (PARAM_ITEMIDS.indexOf(itemId) > -1), lineId);
	}

	// HACK! https://netsuitehub.com/forums/topic/client-script-hack-that-could-save-your-life/
	/**
	 * This function attempts to disable an item sublist field through SuiteScript API, and disables the field
	 * through the DOM if that fails.
	 *
	 * @param rec {N/currentRecord.CurrentRecord}
	 * @param fieldId {String} Internal id of a column field
	 * @param isDisabled {Boolean} disable/true enable/false
	 * @return {void}
	 */
	function disableSublistField(rec, fieldId, isDisabled, lineId) {
		/* {Number} The current item index */
		//var line = rec.getCurrentSublistIndex({sublistId: 'item'});

		try {
			/* {N/record.Field} */
			// not using current line number anymore - 0 seems more reliable
			var field = rec.getSublistField({sublistId: 'item', fieldId: fieldId, line: (lineId ? lineId : 0)});

			field.isDisabled = isDisabled;
		} catch (e) {
			/*
			* The getSublistField method will throw an error if the line has not yet been committed, so we have
			* to disable the field through the DOM and let a validateField function guard it from edit.
			*/
			jQuery("input[name=inpt_" + fieldId + "]").prop('disabled', isDisabled); //For select fields
			jQuery("input[name=" + fieldId + "_formattedValue]").prop('disabled', isDisabled); // For text/number fields
		}
	}

	return {
		lineInit: lineInit,
		postSourcing: postSourcing,
		fieldChanged: fieldChanged
	}

});