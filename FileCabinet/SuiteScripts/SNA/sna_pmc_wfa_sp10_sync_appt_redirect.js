/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType WorkflowActionScript
 * @NModuleScope SameAccount
 * 
 * Script brief description: 
   Calls appointment sync suitelet from workflow
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/11/30                           ckoch           Initial version
 *
 */
define(['N/redirect'], function (redirect) {

    function onAction(context) {
        var spOrderLink = context.newRecord.getValue({ fieldId: 'custbodyserviceprolink' }) || null;
        var spOrderId = null;

        if (spOrderLink != null) {
            var parts = spOrderLink.split('=');
            if (parts.length == 2 && fixnum(parts[1]) > 0) {
                spOrderId = parts[1];
            }
        }

        if (spOrderId != null) {
            redirect.toSuitelet({
                scriptId: 'customscript_sna_pmc_sl_sync_appts',
                deploymentId: 'customdeploy_sna_pmc_sl_sync_appts',
                parameters: {
                    'custpage_nsorderid': context.newRecord.id,
                    'custpage_sporderid': spOrderId
                }
            });
        }
    }

    function fixnum(what) {
        return (isNaN(parseFloat(what)) ? 0 : parseFloat(what));
    }

    return {
        onAction: onAction
    }

});