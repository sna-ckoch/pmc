/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 * 
 * Script brief description: 
   Creates Box Webhooks for specified folder name
   Creates Box File to Service Pro Note records for each file in folder
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/12/07                           ckoch           Initial version
 *
 */
define(['N/http', 'N/runtime', 'N/url', 'N/redirect', './sna_pmc_mod_box_globals.js'],
    function (http, runtime, url, redirect, boxGlobals) {

        function onRequest(context) {
            log.debug({ title: 'REQUEST', details: JSON.stringify(context) });

            if (context.request.method == http.Method.GET) {
                handleGet(context);
            } else {
                context.response.write({ output: 'Request not supported' });
            }
        }

        function handleGet(context) {
            var recType = context.request.parameters['custpage_rectype'] || null;
            var recId = context.request.parameters['custpage_recid'] || null;

            if (recType == null || recId == null) {
                context.response.write({ output: 'Invalid request' });
            } else {
                var boxFolderId = boxGlobals.getBoxFolderId(recType, recId);

                if (boxFolderId != null) {
                    var folderContents = boxGlobals.listFolder(boxFolderId);

                    if (util.isArray(folderContents.entries)) {
                        var monitorFolder = runtime.getCurrentScript().getParameter({
                            name: 'custscript_sna_pmc_box_monitored_folder'
                        }) || '';

                        if (monitorFolder != '') {
                            for (var f in folderContents.entries) {
                                var itemType = folderContents.entries[f].type || null;
                                var itemName = folderContents.entries[f].name || '';
    
                                if (itemType == 'folder' && itemName.toLowerCase() == monitorFolder.toLowerCase()) {
                                    var outFolderId = folderContents.entries[f].id;
                                    var callbackUrl = url.resolveScript({
                                        scriptId: 'customscript_sna_pmc_sl_box_callback',
                                        deploymentId: 'customdeploy_sna_pmc_sl_box_callback',
                                        returnExternalUrl: true
                                    });
    
                                    boxGlobals.createWebhookRecord(outFolderId, callbackUrl, recId);
    
                                    boxGlobals.createBoxToSPRecords(outFolderId, recId);
                                }
                            }
                        }
                    }
                }
            }

            redirect.toRecord({
                type: recType,
                id: recId
            });
        }

        return {
            onRequest: onRequest
        }

    });