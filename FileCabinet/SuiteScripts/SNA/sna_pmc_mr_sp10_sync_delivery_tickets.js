/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 * 
 * Script brief description: 
 * Searches for fulfillments that haven't been sent to sp yet and tries sending them
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2021/04/01                           ckoch           Initial version
 *
 */
define(['N/runtime', 'N/record', 'N/search', 'N/file', 'N/render', './sna_pmc_mod_sp10_globals.js'],
    function (runtime, record, search, file, render, globals) {

        function getInputData(context) {
            return search.create({
                type: search.Type.ITEM_FULFILLMENT,
                filters: [
                    ['custbody_sna_pmc_sp10_note_id', 'isempty', ''],
                    'and',
                    ['createdfrom.custbodysendtoservicepro', 'is', 'T'],
                    'and',
                    ['mainline', 'is', 'T']
                ],
                columns: [
                    'internalid', 'tranid', 'createdfrom', 'custbody_sna_pmc_child_salesorder'
                ]
            });
        }

        function map(context) {
            log.debug({ title: 'MAP.CONTEXT', details: context });

            if (!isEmpty(context.value)) {
                var recId = context.key;
                var result = JSON.parse(context.value);

                log.debug({ title: 'result', details: result });

                var salesOrder = null;
                if (util.isObject(result.values['custbody_sna_pmc_child_salesorder'])) {
                    salesOrder = nullIfEmpty(result.values['custbody_sna_pmc_child_salesorder']['value']);
                }
                if (isEmpty(salesOrder)) {
                    if (util.isObject(result.values['createdfrom'])) {
                        salesOrder = nullIfEmpty(result.values['createdfrom']['value']);
                    }
                }

                var tranId = result.values['tranid'] || recId;

                if (!isEmpty(salesOrder)) {
                    var noteId = createFulfillmentNote(salesOrder, recId, tranId);

                    if (!isEmpty(noteId)) {
                        try {
                            record.submitFields({
                                type: record.Type.ITEM_FULFILLMENT,
                                id: recId,
                                values: {
                                    'custbody_sna_pmc_sp10_note_id': noteId
                                }
                            });
                        } catch (e) {
                            log.error({
                                title: 'UPDATE_NOTEID',
                                details: {
                                    recId: recId,
                                    noteId: noteId,
                                    e: e
                                }
                            });
                        }
                    }
                }
            }
        }

        function createFulfillmentNote(orderId, ifId, tranId) {
            var output = null;

            var spOrderId = globals.getSpOrderId(orderId);
            var deliveryForm = nullIfEmpty(runtime.getCurrentScript().getParameter({ name: 'custscript_sna_pmc_del_ticket_form' }));

            log.debug({ title: 'spOrderId', details: spOrderId });
            log.debug({ title: 'deliveryForm', details: deliveryForm });

            if (spOrderId != null && deliveryForm != null) {
                var pdf = render.packingSlip({
                    entityId: Number(ifId),
                    printMode: render.PrintMode.PDF,
                    formId: Number(deliveryForm)
                });

                try {
                    output = globals.uploadFileNoteNew(String(spOrderId), tranId + '.pdf', tranId, 'application/pdf', pdf.getContents());

                    log.debug({ title: 'noteId.result', details: output });
                } catch (e) {
                    log.error({
                        title: 'NOTE_CREATE',
                        details: {
                            orderId: orderId,
                            spOrderId: spOrderId,
                            e: e
                        }
                    });
                }
            }

            return nullIfEmpty(output);
        }

        function summarize(context) {
            log.debug({ title: 'SUMMARIZE', details: JSON.stringify(context) });
        }

        function isEmpty(stValue) {
            return ((stValue === '' || stValue == null || stValue == undefined) || (stValue.constructor === Array && stValue.length == 0) || (stValue.constructor === Object && (function (v) {
                for (var k in v)
                    return false;
                return true;
            })(stValue)));
        }

        function nullIfEmpty(what) {
            return (isEmpty(what) ? null : what);
        }

        return {
            getInputData: getInputData,
            map: map,
            summarize: summarize
        }

    });