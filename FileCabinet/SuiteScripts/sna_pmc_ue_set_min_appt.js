/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Sets 2 fields on related sales order - minimum scheduled date and it's status
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2021/03/23                           ckoch           Initial version
 *
 */
define(['N/search', 'N/record'], function (search, record) {

    function afterSubmit(context) {
        var events = {};
        events[context.UserEventType.CREATE] = handleCreate;
        events[context.UserEventType.EDIT] = handleCreate;

        if (typeof events[context.type] === 'function') {
            events[context.type](context);
        }
    }

    function handleCreate(context) {
        var rec = context.newRecord;

        var soId = nullIfEmpty(rec.getValue({ fieldId: 'custrecord_sna_pmc_appt_salesorder' }));
        if (soId == null) return;

        var apptInfo = getApptInfo(soId);

        log.debug({
            title: 'apptInfo',
            details: {
                soId: soId,
                apptInfo: apptInfo
            }
        });

        try {
            if (apptInfo.apptMinDate !== apptInfo.soMinDate || apptInfo.apptMaxDate !== apptInfo.soMaxDate || apptInfo.apptMinStatus !== apptInfo.soMinStatus) {
                record.submitFields({
                    type: record.Type.SALES_ORDER,
                    id: soId,
                    values: {
                        'custbody_sna_min_appt_date': apptInfo.apptMinDate,
                        'custbody_sna_max_appt_date': apptInfo.apptMaxDate,
                        'custbody_sna_min_appt_status': apptInfo.apptMinStatus
                    },
                    enablesourcing: false,
                    ignoreMandatoryFields: true
                });
            }
        } catch (e) {
            log.error({
                title: 'MIN_APPT_UPDATE',
                details: {
                    soId: soId,
                    e: e
                }
            });
        }
    }

    function getApptInfo(soId) {
        var output = {
            apptMinDate: null,
            apptMinStatus: null,
            apptMaxDate: null,
            apptMaxStatus: null,
            soMinDate: null,
            soMinStatus: null,
            soMaxDate: null,
            soMaxStatus: null
        };

        var colMinApptDate = search.createColumn({
            name: 'custrecord_sna_pmc_appt_scheduledate',
            summary: search.Summary.MIN
        });
        var colMinApptStatus = search.createColumn({
            name: 'formulatext',
            summary: search.Summary.MIN,
            formula: '{custrecord_sna_pmc_appt_status.id}'
        }).setWhenOrderedBy({
            join: 'customrecord_sna_pmc_appointment',
            name: 'custrecord_sna_pmc_appt_scheduledate'
        });
        var colMaxApptDate = search.createColumn({
            name: 'custrecord_sna_pmc_appt_scheduledate',
            summary: search.Summary.MAX
        });
        var colMaxApptStatus = search.createColumn({
            name: 'formulatext',
            summary: search.Summary.MAX,
            formula: '{custrecord_sna_pmc_appt_status.id}'
        }).setWhenOrderedBy({
            join: 'customrecord_sna_pmc_appointment',
            name: 'custrecord_sna_pmc_appt_scheduledate'
        });
        var colMinOrderDate = search.createColumn({
            name: 'custbody_sna_min_appt_date',
            join: 'custrecord_sna_pmc_appt_salesorder',
            summary: search.Summary.MAX
        });
        var colMinOrderStatus = search.createColumn({
            name: 'custbody_sna_min_appt_status',
            join: 'custrecord_sna_pmc_appt_salesorder',
            summary: search.Summary.GROUP
        });
        var colMaxOrderDate = search.createColumn({
            name: 'custbody_sna_max_appt_date',
            join: 'custrecord_sna_pmc_appt_salesorder',
            summary: search.Summary.MAX
        });

        search.create({
            type: 'customrecord_sna_pmc_appointment',
            filters: [
                ['custrecord_sna_pmc_appt_salesorder', 'anyof', soId],
                'and',
                ['custrecord_sna_pmc_appt_scheduledate', 'isnotempty', ''],
                'and',
                ['custrecord_sna_pmc_appt_salesorder.mainline', 'is', 'T'],
                'and',
                ['isinactive', 'is', 'F'],
                'and',
                ['custrecord_sna_pmc_appt_status', 'anyof', ['102','211','2']]
            ],
            columns: [
                colMinApptDate, colMinApptStatus, colMaxApptDate, colMaxApptStatus,
                colMinOrderDate, colMinOrderStatus, colMaxOrderDate
            ]
        }).run().each(function (result) {
            output = {
                apptMinDate: nullIfEmpty(result.getValue(colMinApptDate)),
                apptMinStatus: nullIfEmpty(result.getValue(colMinApptStatus)),
                apptMaxDate: nullIfEmpty(result.getValue(colMaxApptDate)),
                apptMaxStatus: nullIfEmpty(result.getValue(colMaxApptStatus)),
                soMinDate: nullIfEmpty(result.getValue(colMinOrderDate)),
                soMinStatus: nullIfEmpty(result.getValue(colMinOrderStatus)),
                soMaxDate: nullIfEmpty(result.getValue(colMaxOrderDate))
            }

            return false; // only take first result
        });

        return output;
    }

    function getApptInfoOLD(soId) {
        var output = {
            apptMinDate: null,
            apptMinStatus: null,
            soMinDate: null,
            soMinStatus: null
        };

        search.create({
            type: 'customrecord_sna_pmc_appointment',
            filters: [
                ['custrecord_sna_pmc_appt_salesorder', 'anyof', soId],
                'and',
                ['custrecord_sna_pmc_appt_scheduledate', 'isnotempty', ''],
                'and',
                ['custrecord_sna_pmc_appt_salesorder.mainline', 'is', 'T'],
                'and',
                ['isinactive', 'is', 'F'],
                'and',
                ['custrecord_sna_pmc_appt_status', 'anyof', ['102','211','2']]
            ],
            columns: [
                { name: "custrecord_sna_pmc_appt_scheduledate", sort: search.Sort.ASC },
                'custrecord_sna_pmc_appt_status',
                { name: 'custbody_sna_min_appt_date', join: 'custrecord_sna_pmc_appt_salesorder' },
                { name: 'custbody_sna_min_appt_status', join: 'custrecord_sna_pmc_appt_salesorder' }
            ]
        }).run().each(function (result) {
            output = {
                apptMinDate: nullIfEmpty(result.getValue({ name: "custrecord_sna_pmc_appt_scheduledate", sort: search.Sort.ASC })),
                apptMinStatus: nullIfEmpty(result.getValue({ name: 'custrecord_sna_pmc_appt_status' })),
                soMinDate: nullIfEmpty(result.getValue({ name: 'custbody_sna_min_appt_date', join: 'custrecord_sna_pmc_appt_salesorder' })),
                soMinStatus: nullIfEmpty(result.getValue({ name: 'custbody_sna_min_appt_status', join: 'custrecord_sna_pmc_appt_salesorder' }))
            }

            return false; // only take first result
        });

        return output;
    }

    function isEmpty(stValue) {
        return ((stValue === '' || stValue == null || stValue == undefined) || (stValue.constructor === Array && stValue.length == 0) || (stValue.constructor === Object && (function (v) {
            for (var k in v)
                return false;
            return true;
        })(stValue)));
    }

    function nullIfEmpty(what) {
        return (isEmpty(what) ? null : what);
    }

    function lookupField(recType, recId, fieldId) {
        var output = null;

        try {
            output = search.lookupFields({
                type: recType,
                id: recId,
                columns: [fieldId]
            })[fieldId] || null;

            if (util.isArray(output)) {
                if (output.length > 0) {
                    output = output[0].value;
                }
            }
        } catch (e) {
            log.error({
                title: 'lookupField',
                details: JSON.stringify({
                    recType: recType,
                    recId: recId,
                    fieldId: fieldId,
                    e: e
                })
            });
        }

        log.debug({
            title: 'lookupField',
            details: JSON.stringify({
                recType: recType,
                recId: recId,
                fieldId: fieldId,
                output: output
            })
        });

        return output;
    }

    return {
        afterSubmit: afterSubmit
    }

});