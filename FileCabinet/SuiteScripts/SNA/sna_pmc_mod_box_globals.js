/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NModuleScope Public
 *
 * Script brief description: 
   Globals and support functions for Box integration
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/12/04                           ckoch           Initial version
 *
 */
define(['N/runtime', 'N/https', 'N/cache', 'N/search', 'N/record', 'N/file',
		'./sna_pmc_mod_sp10_globals.js', './jsrsasign-master/jsrsasign-all-min.js'],
	function (runtime, https, cache, search, record, file, spGlobals, jsrsasign) {

		var SP10_NOTE_TYPE = '10';

		var settings = {};
		settings[runtime.EnvType.SANDBOX] = {
			apiDomain: 'https://api.box.com',
			uploadDomain: 'https://upload.box.com',
			config: {
				"boxAppSettings": {
					"clientID": "d7imwa8yr6ch2z8c65nvn6gcyax5rrmp",
					"clientSecret": "uMx044VkH1uuQwae6ayog7h2Teq9mKbS",
					"appAuth": {
						"publicKeyID": "0x9j9nab",
						"privateKey": "-----BEGIN ENCRYPTED PRIVATE KEY-----\nMIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQI/1EBVvX9hFsCAggA\nMBQGCCqGSIb3DQMHBAiyE3pGYso+ZASCBMioS+viuWFRnXi/V8unnw3j70C1lmrJ\n7b3C/r4dmLnKyYarYspn0VqEB895Q0ZtF/EzGF1Mid5xsscJeQnIQ72vzF3ddGCb\n5cdYuJIve8il/XoqOP7MPYMijbKN+4JZi1o9Rxpb+//agh8D9x4iJOda0Egwe2FI\nQz1Nfr2VJdPqFHdQUaAXAuHV5UONju9W+npoMMHj5phvFWPGEQtJFzNk5BvCo0Mn\n/C0onWlKJc7/zdhyM85plLxIWpnMVLmsocmEucy+MuB+dnXEvoShkmQYfKMdfdQ5\nTlCl/dokkTTL/cqq11h6TCoOCPMbZcYGrjHFAAmEDpx52rx7fW1w2rGF0azrslwI\n+k+TDSBduB3GBApp5T8BJC1p5GBKRjR/iKt5Px0BVQ541l1bRDZkPl4X9JO0B77N\nctenFXParN91EmAZSKx1wujr/QtB6hzJt28BP3hZMIJSs0JxmiDlyl5EFpQHt+mI\nq7AUh4eT9hora0IA2W94htS3di8dX/pMdhhIajUQFkUhWrJqMNLdwfbDOqIrtSrl\nAJc5PAOw23cs56rGQUvp72gvuS5XiMO2r9t+iIfdrE/nXWE7NwCfWgLFSc0OTHYM\nuNO2xlrmyaQmu3M99mAuIhyu/P5KU8Ui3zuGkp22mtDDZiHdy+9Keyx65WoYKx19\nyf/TJKOJVt3qsPVl35cr0gpcWnfS4ghCYgB2tueInB/F5pHo4Vn0Z5258q4bIn2a\nyKde0Db8617egfgrPseRIZ3wS4jWX4MDTBs3sx9xrzNI3Mp7VSuy2l44ewjx5rBo\nPFGdhRmqu0QgMqQxkqepNV3eKFSkgcsbnIv3NzGiFncq6uksaxyjtL19WKXpXsm5\nAnm8/oJ4kKiHN+evaOGAVCPgvPVsY/Cegz0iRxwmQTho7PxSU2HVG72vwDWrWnCy\nc2aT24xTwUZkoyWYbGXFx7uaOUzUay7LVqBEDDygks69uHQQVZz+kxDjGOqlQt8l\nYtciuUFmt9O6WmWCAsNrPvvqHSA+B+8cwpPxVxM5KpXieptPmalOTuDwqocIinzh\n80oLwI0sxzo7/fS/DRK2afKHp/lVZUUuVf+daHfftr+p2C4A+HOaVGoeFnoP5/WQ\nIs0SerwZF4TZrw/UEYeiht7BwCU19XxJRjwJQh1711aXFA8g2ZcnEwdwTJTSoUAM\n4xovsVEU5N2AEGTmSG2BcQ9AOxIe4pf6S9aWzMNosM/GHEP+k+XCuPkPadxpvE1T\nnS+7jwT4n59+LVGn2up6wviccUD6mLxh14kgVTMdPtQX2mIFTvTg6A1NaudXW4sv\nPdLURKk10i5041sLVzGYq8jX0/kS1sSDp4gxSQhquncEs87L9v5BEBMS6cIJvGDr\n1qwq/kE/6OY5p9h87h9a6NaaCTVcmzc3EsRJh1KTjKfvMDX2FGNUF6wrPn2m4VTl\nYXmyupYb/EHWsvWMLTnPq36fzNxBz0fofX8vzmZWPneYlNFdWcunEhJd4LwSsc6K\nzVjRlX1hl4VA9+zgsXu2bgXyfuBtQdf0LY7jSscoFSeMlrwaVcJxl+lEQ3zLSI6T\n+PwxaT12/lBp7vf1ItAqRDSKTGOeKg2KweCgm8II3f5ZATCsvdwJIhUrtDCJh/9m\n7HA=\n-----END ENCRYPTED PRIVATE KEY-----\n",
						"passphrase": "be17d9683be35a0edfe06d7ec1e9f068"
					}
				},
				"enterpriseID": "955074"
			}
		};
		settings[runtime.EnvType.PRODUCTION] = {
			apiDomain: 'https://api.box.com',
			uploadDomain: 'https://upload.box.com',
			config: {
				"boxAppSettings": {
					"clientID": "d7imwa8yr6ch2z8c65nvn6gcyax5rrmp",
					"clientSecret": "uMx044VkH1uuQwae6ayog7h2Teq9mKbS",
					"appAuth": {
						"publicKeyID": "0x9j9nab",
						"privateKey": "-----BEGIN ENCRYPTED PRIVATE KEY-----\nMIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQI/1EBVvX9hFsCAggA\nMBQGCCqGSIb3DQMHBAiyE3pGYso+ZASCBMioS+viuWFRnXi/V8unnw3j70C1lmrJ\n7b3C/r4dmLnKyYarYspn0VqEB895Q0ZtF/EzGF1Mid5xsscJeQnIQ72vzF3ddGCb\n5cdYuJIve8il/XoqOP7MPYMijbKN+4JZi1o9Rxpb+//agh8D9x4iJOda0Egwe2FI\nQz1Nfr2VJdPqFHdQUaAXAuHV5UONju9W+npoMMHj5phvFWPGEQtJFzNk5BvCo0Mn\n/C0onWlKJc7/zdhyM85plLxIWpnMVLmsocmEucy+MuB+dnXEvoShkmQYfKMdfdQ5\nTlCl/dokkTTL/cqq11h6TCoOCPMbZcYGrjHFAAmEDpx52rx7fW1w2rGF0azrslwI\n+k+TDSBduB3GBApp5T8BJC1p5GBKRjR/iKt5Px0BVQ541l1bRDZkPl4X9JO0B77N\nctenFXParN91EmAZSKx1wujr/QtB6hzJt28BP3hZMIJSs0JxmiDlyl5EFpQHt+mI\nq7AUh4eT9hora0IA2W94htS3di8dX/pMdhhIajUQFkUhWrJqMNLdwfbDOqIrtSrl\nAJc5PAOw23cs56rGQUvp72gvuS5XiMO2r9t+iIfdrE/nXWE7NwCfWgLFSc0OTHYM\nuNO2xlrmyaQmu3M99mAuIhyu/P5KU8Ui3zuGkp22mtDDZiHdy+9Keyx65WoYKx19\nyf/TJKOJVt3qsPVl35cr0gpcWnfS4ghCYgB2tueInB/F5pHo4Vn0Z5258q4bIn2a\nyKde0Db8617egfgrPseRIZ3wS4jWX4MDTBs3sx9xrzNI3Mp7VSuy2l44ewjx5rBo\nPFGdhRmqu0QgMqQxkqepNV3eKFSkgcsbnIv3NzGiFncq6uksaxyjtL19WKXpXsm5\nAnm8/oJ4kKiHN+evaOGAVCPgvPVsY/Cegz0iRxwmQTho7PxSU2HVG72vwDWrWnCy\nc2aT24xTwUZkoyWYbGXFx7uaOUzUay7LVqBEDDygks69uHQQVZz+kxDjGOqlQt8l\nYtciuUFmt9O6WmWCAsNrPvvqHSA+B+8cwpPxVxM5KpXieptPmalOTuDwqocIinzh\n80oLwI0sxzo7/fS/DRK2afKHp/lVZUUuVf+daHfftr+p2C4A+HOaVGoeFnoP5/WQ\nIs0SerwZF4TZrw/UEYeiht7BwCU19XxJRjwJQh1711aXFA8g2ZcnEwdwTJTSoUAM\n4xovsVEU5N2AEGTmSG2BcQ9AOxIe4pf6S9aWzMNosM/GHEP+k+XCuPkPadxpvE1T\nnS+7jwT4n59+LVGn2up6wviccUD6mLxh14kgVTMdPtQX2mIFTvTg6A1NaudXW4sv\nPdLURKk10i5041sLVzGYq8jX0/kS1sSDp4gxSQhquncEs87L9v5BEBMS6cIJvGDr\n1qwq/kE/6OY5p9h87h9a6NaaCTVcmzc3EsRJh1KTjKfvMDX2FGNUF6wrPn2m4VTl\nYXmyupYb/EHWsvWMLTnPq36fzNxBz0fofX8vzmZWPneYlNFdWcunEhJd4LwSsc6K\nzVjRlX1hl4VA9+zgsXu2bgXyfuBtQdf0LY7jSscoFSeMlrwaVcJxl+lEQ3zLSI6T\n+PwxaT12/lBp7vf1ItAqRDSKTGOeKg2KweCgm8II3f5ZATCsvdwJIhUrtDCJh/9m\n7HA=\n-----END ENCRYPTED PRIVATE KEY-----\n",
						"passphrase": "be17d9683be35a0edfe06d7ec1e9f068"
					}
				},
				"enterpriseID": "955074"
			}
		};

		var records = {
			boxRecordFolder: {
				id: 'customrecord_box_record_folder',
				fields: {
					recordType: 'custrecord_netsuite_record_type',
					recordId: 'custrecord_ns_record_id',
					folderId: 'custrecord_box_record_folder_id'
				}
			},
			webhook: {
				id: 'customrecord_sna_pmc_box_webhook',
				fields: {
					webhookId: 'custrecord_sna_pmc_box_webhook_id',
					salesOrder: 'custrecord_sna_pmc_box_sales_order',
					targetId: 'custrecord_sna_pmc_box_target_id',
					targetType: 'custrecord_sna_pmc_box_target_type',
					triggers: 'custrecord_sna_pmc_box_triggers',
					callbackUrl: 'custrecord_sna_pmc_box_callback_url',
					externalKey: 'custrecord_sna_box_pmc_external_key',
					rawResponse: 'custrecord_sna_pmc_box_raw_response'
				}
			},
			boxToSP: {
				id: 'customrecord_sna_pmc_box_to_sp10_note',
				fields: {
					salesOrder: 'custrecord_sna_pmc_box_salesorder',
					boxFileId: 'custrecord_sna_pmc_box_file_id',
					boxVersionId: 'custrecord_sna_pmc_box_file_version_id',
					boxTrigger: 'custrecord_sna_pmc_box_trigger',
					spNoteId: 'custrecord_sna_pmc_box_sp10_note_id',
					description: 'custrecord_sna_pmc_box_file_description'
				}
			},
			spToBox: {
				id: 'customrecord_sna_pmc_sp10_email_to_box',
				fields: {
					salesOrder: 'custrecord_sna_pmc_box2sp_sales_order',
					emailFrom: 'custrecord_sna_pmc_box2sp_email_from',
					emailSubject: 'custrecord_sna_pmc_box2sp_email_subject',
					emailMessage: 'custrecord_sna_pmc_box2sp_email_message',
					emailAttachment: 'custrecord_sna_pmc_box2sp_email_file',
					boxFileId: 'custrecord_sna_pmc_box2sp_box_fileid'
				}
			},
			spToBox: {
				id: 'customrecord_sna_pmc_sp10_email_to_box',
				fields: {
					salesOrder: 'custrecord_sna_pmc_box2sp_sales_order',
					emailFrom: 'custrecord_sna_pmc_box2sp_email_from',
					emailSubject: 'custrecord_sna_pmc_box2sp_email_subject',
					emailMessage: 'custrecord_sna_pmc_box2sp_email_message',
					emailAttachment: 'custrecord_sna_pmc_box2sp_email_file',
					boxFolderId: 'custrecord_sna_pmc_box2sp_box_folderid',
					boxUploadEmail: 'custrecord_sna_pmc_box2sp_box_email'
				}
			}
		};

		function getSettings() {
			if (settings.hasOwnProperty(runtime.envType)) {
				return settings[runtime.envType];
			} else {
				return null;
			}
		}

		function apiCall(controller, parse, method, payload, headers) {
			var settings = getSettings();

			if (settings != null) {
				var res = null;

				if (headers == null) {
					var token = getToken();

					headers = {
						'Authorization': 'Bearer ' + token.access_token
					};
				}

				if (method == 'get') {
					res = https.get({
						url: settings.apiDomain + controller,
						headers: headers
					});
				} else if (method == 'delete') {
					res = https.delete({
						url: settings.apiDomain + controller,
						headers: headers
					});
				} else if (method == 'post') {
					if (util.isObject(payload)) {
						headers['Content-Type'] = 'application/json';

						res = https.post({
							url: settings.apiDomain + controller,
							headers: headers,
							body: JSON.stringify(payload)
						});
					} else if (util.isArray(payload)) {
						var boundary = new Date().getTime().toString(16);

						headers['Content-Type'] = 'multipart/form-data; boundary=' + boundary;

						var parts = [];
						payload.forEach(function (p, idx) {
							parts.push('--' + boundary);
							parts.push('Content-Disposition: ' + p['Content-Disposition']);
							if (p['Content-Type']) {
								parts.push('Content-Type: ' + p['Content-Type']);
							}
							if (p['Content-Transfer-Encoding']) {
								parts.push('Content-Transfer-Encoding: ' + p['Content-Transfer-Encoding']);
							}
							parts.push('');

							if (util.isObject(p['data'])) {
								parts.push(JSON.stringify(p['data']));
							} else {
								parts.push(p['data']);
							}

							if (idx == payload.length - 1) {
								parts.push('--' + boundary + '--');
								parts.push('');
							}
						});

						res = https.post({
							url: settings.uploadDomain + controller,
							headers: headers,
							body: parts.join('\r\n')
						});
					} else {
						res = https.post({
							url: settings.apiDomain + controller,
							headers: headers,
							body: payload
						});
					}
				}

				if (res != null && parse == true) {
					return JSON.parse(res.body);
				} else {
					return res;
				}
			} else {
				return null;
			}
		}

		function getToken() {
			var output = cache.getCache({
				name: 'sna_pmc_box',
				scope: cache.Scope.PUBLIC
			}).get({
				key: 'token'
			});

			if (output != null) {
				output = JSON.parse(output);

				if (validateToken(output) == false) {
					output = refreshToken();
				}
			} else {
				output = refreshToken();
			}

			return output;
		}

		function putToken(token) {
			cache.getCache({
				name: 'sna_pmc_box',
				scope: cache.Scope.PUBLIC
			}).put({
				key: 'token',
				value: JSON.stringify(token)
			});
		}

		function validateToken(token) {
			var output = false;

			if (token != null) {
				var timeStamp = new Date(token.timestamp);
				var rightNow = new Date();

				output = ((rightNow - timeStamp) / 1000) < token.expires_in;
			}

			return output;
		}

		function refreshToken() {
			var output = null;
			var settings = getSettings();

			if (settings != null) {
				try {
					var headers = {
						'Content-Type': 'application/json',
						'Accept': 'application/json'
					};

					var jwt = getJWT(settings.config);

					if (jwt != null) {
						var payload = {
							'grant_type': 'urn:ietf:params:oauth:grant-type:jwt-bearer',
							'assertion': jwt,
							'client_id': settings.config.boxAppSettings.clientID,
							'client_secret': settings.config.boxAppSettings.clientSecret
						};

						var timeStamp = new Date().toISOString();

						var token = apiCall('/oauth2/token', true, 'post', payload, headers);

						if (token !== null && token['access_token']) {
							token['timestamp'] = timeStamp;

							putToken(token);

							output = token;
						}
					} else {
						log.debug({
							title: 'GET_JWT',
							details: 'Blank JWT generated'
						});
					}
				} catch (e) {
					log.error({
						title: 'REFRESH_TOKEN',
						details: JSON.stringify({
							token: token,
							e: JSON.stringify(e)
						})
					});
				}
			}

			return output;
		}

		function getJWT(config) {
			var output = null;

			try {
				var key = {
					key: config.boxAppSettings.appAuth.privateKey,
					passphrase: config.boxAppSettings.appAuth.passphrase
				};

				var claims = {
					iss: config.boxAppSettings.clientID,
					sub: config.enterpriseID,
					box_sub_type: 'enterprise',
					aud: 'https://api.box.com/oauth2/token',
					jti: KJUR.crypto.Util.getRandomHexOfNbytes(64),
					exp: Math.floor(Date.now() / 1000) + 45
				};

				var headers = {
					'algorithm': 'RS256',
					'keyid': config.boxAppSettings.appAuth.publicKeyID,
				};

				output = KJUR.jws.JWS.sign('RS256', headers, claims, key.key, key.passphrase);
			} catch (e) {
				log.error({
					title: 'GET_JWT',
					details: JSON.stringify({
						config: JSON.stringify(config),
						e: JSON.stringify(e)
					})
				});
			}

			return output;
		}

		function listFolder(folderId) {
			var output = null;

			try {
				output = apiCall('/2.0/folders/' + folderId + '/items', true, 'get');
			} catch (e) {
				log.error({
					title: 'LIST_FOLDERS',
					details: JSON.stringify({
						folderId: folderId,
						e: JSON.stringify(e)
					})
				});
			}

			return output;
		}

		function getFolderInfo(folderId) {
			var output = null;

			try {
				output = apiCall('/2.0/folders/' + folderId, true, 'get');
			} catch (e) {
				log.error({
					title: 'GET_FOLDER_EMAIL',
					details: JSON.stringify({
						folderId: folderId,
						e: JSON.stringify(e)
					})
				});
			}

			return output;
		}

		function makeFolder(name, parentId, uploadEmail) {
			var output = null;

			try {
				var payload = {
					name: name,
					parent: {
						id: parentId
					}
				};

				if (uploadEmail == true) {
					payload['folder_upload_email'] = {
						access: 'open'
					};
				}

				output = apiCall('/2.0/folders/', true, 'post', payload);
			} catch (e) {
				log.error({
					title: 'MAKE_FOLDER',
					details: JSON.stringify({
						name: name,
						parentId: parentId,
						e: JSON.stringify(e)
					})
				});
			}

			return output;
		}

		function downloadFile(fileId, versionId) {
			var output = null;

			try {
				var endpointUrl = '/2.0/files/' + fileId + '/content';
				if (versionId) {
					endpointUrl += '?version=' + versionId;
				}
				var response = apiCall(endpointUrl, false, 'get');

				if (response.code == 200) {
					output = {
						'Content-Disposition': response.headers['Content-Disposition'],
						'Content-Type': response.headers['Content-Type'],
						'fileName': /filename="(.*?)"/.exec(response.headers['Content-Disposition'])[1],
						'content': response.body
					};
				} else {
					log.error({
						title: 'DOWNLOAD_FILE',
						details: JSON.stringify({
							fileId: fileId,
							response: JSON.stringify(response)
						})
					});
				}
			} catch (e) {
				log.error({
					title: 'DOWNLOAD_FILE',
					details: JSON.stringify({
						fileId: fileId,
						e: JSON.stringify(e)
					})
				});
			}

			return output;
		}

		function uploadFile(name, type, contents, parentId) {
			// TODO FINISH THIS

			var output = null;

			try {
				var attributes = {
					'name': name,
					'parent': {
						'id': parentId
					}
				};

				var parts = [];
				parts.push({
					'Content-Disposition': 'form-data; name="attributes"',
					'data': JSON.stringify(attributes)
				});
				parts.push({
					'Content-Disposition': 'form-data; name="file"; filename="' + name + '"',
					'Content-Type': type,
					'Content-Transfer-Encoding': 'base64',
					'data': contents
				});

				var response = apiCall('/2.0/files/content', false, 'post', parts);

				if (response.code == 200) {
					// TODO
				}
			} catch (e) {
				log.error({
					title: 'UPLOAD_FILE',
					details: JSON.stringify({
						name: name,
						type: type,
						parentId: parentId,
						e: JSON.stringify(e)
					})
				});
			}

			return output;
		}

		function createWebhook(rec) {
			var output = null;

			try {
				var payload = {
					target: {
						id: rec.getValue({fieldId: records.webhook.fields.targetId}),
						type: rec.getValue({fieldId: records.webhook.fields.targetType})
					},
					address: rec.getValue({fieldId: records.webhook.fields.callbackUrl}),
					triggers: rec.getValue({fieldId: records.webhook.fields.triggers}).split(',')
				};

				output = apiCall('/2.0/webhooks', true, 'post', payload);

				rec.setValue({fieldId: records.webhook.fields.webhookId, value: output.id});
				rec.setValue({fieldId: records.webhook.fields.rawResponse, value: JSON.stringify(output)});
			} catch (e) {
				log.error({
					title: 'CREATE_WEBHOOK',
					details: JSON.stringify({
						rec: JSON.stringify(rec),
						e: JSON.stringify(e)
					})
				});
			}

			return output;
		}

		function deleteWebhook(webhookId) {
			var output = false;

			try {
				apiCall('/2.0/webhooks/' + webhookId, false, 'delete');
				output = true;
			} catch (e) {
				log.error({
					title: 'DELETE_WEBHOOK',
					details: JSON.stringify({
						webhookId: webhookId,
						e: JSON.stringify(e)
					})
				});
			}

			return output;
		}

		function createWebhookRecord(targetId, callbackUrl, salesOrderId) {
			var output = null;

			try {
				if (webhookExists(targetId) == false) {
					var extKey = new Date().getTime().toString(16);

					var rec = record.create({
						type: records.webhook.id
					}).setValue({
						fieldId: records.webhook.fields.targetId,
						value: targetId
					}).setValue({
						fieldId: records.webhook.fields.targetType,
						value: 'folder'
					}).setValue({
						fieldId: records.webhook.fields.triggers,
						value: 'FILE.UPLOADED'
					}).setValue({
						fieldId: records.webhook.fields.callbackUrl,
						value: callbackUrl + '?custpage_extkey=' + extKey
					}).setValue({
						fieldId: records.webhook.fields.externalKey,
						value: extKey
					});

					if (salesOrderId) {
						rec.setValue({
							fieldId: records.webhook.fields.salesOrder,
							value: salesOrderId
						});
					}

					output = rec.save();
				}
			} catch (e) {
				log.error({
					title: 'CREATE_WEBHOOK_REC',
					details: JSON.stringify({
						targetId: targetId,
						callbackUrl: callbackUrl,
						e: JSON.stringify(e)
					})
				});
			}

			return output;
		}

		function getOrderFromExtKey(extKey) {
			var output = false;

			if (extKey) {
				search.create({
					type: records.webhook.id,
					filters: [
						[records.webhook.fields.externalKey, 'is', extKey],
						'and',
						[records.webhook.fields.salesOrder, 'noneof', '@NONE@'],
						'and',
						['isinactive', 'is', 'F']
					],
					columns: [records.webhook.fields.salesOrder]
				}).run().each(function (result) {
					output = result.getValue({name: records.webhook.fields.salesOrder}) || null;
					return false;
				});
			}

			return output;
		}

		// can only have 1 webhook per target
		function webhookExists(targetId) {
			var output = false;

			if (targetId) {
				search.create({
					type: records.webhook.id,
					filters: [
						[records.webhook.fields.targetId, 'is', targetId],
						'and',
						['isinactive', 'is', 'F']
					],
					columns: ['internalid']
				}).run().each(function (result) {
					output = true;
					return false;
				});
			}

			return output;
		}

		function getBoxFolderId(recType, recId) {
			var output = null;

			try {
				search.create({
					type: records.boxRecordFolder.id,
					filters: [
						[records.boxRecordFolder.fields.recordType, 'is', recType],
						'and',
						[records.boxRecordFolder.fields.recordId, 'is', recId],
						'and',
						['isinactive', 'is', 'F']
					],
					columns: [records.boxRecordFolder.fields.folderId]
				}).run().each(function (result) {
					output = result.getValue({name: records.boxRecordFolder.fields.folderId}) || null;
					return false; // just get first one
				});
			} catch (e) {
				log.error({
					title: 'GET_BOX_FOLDER',
					details: JSON.stringify({
						recType: recType,
						recId: recId,
						e: JSON.stringify(e)
					})
				});
			}

			return output;
		}

		function createBoxToSPRecords(boxFolderId, salesOrderId) {
			var items = listFolder(boxFolderId);

			if (items != null) {
				if (util.isArray(items.entries)) {
					for (var i in items.entries) {
						var itemId = items.entries[i].id;
						var itemType = items.entries[i].type;
						var itemName = items.entries[i].name;
						var itemVersion = null;
						if (items.entries[i].file_version) {
							itemVersion = items.entries[i].file_version.id;
						}
						if (itemType == 'file' && itemId != null && itemName != null) {
							createBoxToSPRecord(itemId, itemVersion, itemName, salesOrderId);
						}
					}
				}
			}
		}

		function createBoxToSPRecord(boxFileId, boxFileVersionId, description, salesOrderId, trigger) {
			var output = null;

			try {
				if (boxToSPRecordExists(boxFileId, boxFileVersionId) == false) {
					trigger = trigger || 'NETSUITE.PULL';

					var rec = record.create({
						type: records.boxToSP.id
					}).setValue({
						fieldId: records.boxToSP.fields.boxFileId,
						value: boxFileId
					}).setValue({
						fieldId: records.boxToSP.fields.boxTrigger,
						value: trigger
					}).setValue({
						fieldId: records.boxToSP.fields.description,
						value: description
					});

					if (boxFileVersionId) {
						rec.setValue({
							fieldId: records.boxToSP.fields.boxVersionId,
							value: boxFileVersionId
						});
					}

					if (salesOrderId) {
						rec.setValue({
							fieldId: records.boxToSP.fields.salesOrder,
							value: salesOrderId
						});
					}

					output = rec.save();
				}
			} catch (e) {
				log.error({
					title: 'CREATE_BOX2SP_REC',
					details: JSON.stringify({
						boxFileId: boxFileId,
						boxFileVersionId: boxFileVersionId,
						description: description,
						e: JSON.stringify(e)
					})
				});
			}

			return output;
		}

		function boxToSPRecordExists(boxFileId, boxFileVersionId) {
			var output = false;

			var filters = [
				[records.boxToSP.fields.boxFileId, 'is', boxFileId]
			];

			if (boxFileVersionId) {
				filters.push('and');
				filters.push([records.boxToSP.fields.boxVersionId, 'is', boxFileVersionId]);
			}

			search.create({
				type: records.boxToSP.id,
				filters: filters,
				columns: ['internalid', records.boxToSP.fields.spNoteId]
			}).run().each(function (result) {
				var noteId = result.getValue({name: records.boxToSP.fields.spNoteId}) || null;
				if (noteId == null) {
					boxFileRecordToSP(result.id);
				}
				output = true;
				return false;
			});

			return output;
		}

		function boxInfoFromRecord(recid) {
			var output = null;

			try {
				output = search.lookupFields({
					type: records.boxToSP.id,
					id: recid,
					columns: [
						records.boxToSP.fields.boxFileId,
						records.boxToSP.fields.boxVersionId,
						records.boxToSP.fields.description,
						records.boxToSP.fields.salesOrder
					]
				});
			} catch (e) {
				log.error({
					title: 'NOTE_FROM_RECORD',
					details: JSON.stringify({
						recid: recid,
						e: JSON.stringify(e)
					})
				});
			}

			return output;
		}

		function boxFileRecordToSP(recid) {
		    var settings = getSettings();
		    if (settings == null) return;

		    var output = false;
		    var boxInfo = boxInfoFromRecord(recid);

		    if (boxInfo) {
                if (boxInfo[records.boxToSP.fields.salesOrder].length > 0) {
                    var orderId = spGlobals.getSpOrderIdAPI(boxInfo[records.boxToSP.fields.salesOrder][0].value); //spGlobals.getSpOrderId(boxInfo[records.boxToSP.fields.salesOrder][0].value);
                    log.debug({title: 'boxFileRecordToSP', details: 'orderId=' + orderId});

                    if (orderId != null) {
                        var noteId = null;

                        try {
                            var boxFileId = boxInfo[records.boxToSP.fields.boxFileId];
                            var boxVersionId = boxInfo[records.boxToSP.fields.boxVersionId];

                            var getUrl = settings.apiDomain + '/2.0/files/' + boxFileId + '/content';

                            if (boxVersionId) {
                                getUrl += '?version=' + boxVersionId;
                            }

                            var payload = {
                                "get": {
                                    "url": getUrl,
                                    "headers": {
                                        "Authorization": "Bearer " + getToken().access_token
                                    }
                                },
                                "post": {
                                    "url": "https://servicepro10.com/service/api/OrderNote/Upload/" + orderId,
                                    "headers": {
                                        "APIKey": spGlobals.keys[runtime.envType].apikey,
                                        "SK": spGlobals.keys[runtime.envType].sk
                                    }
                                }
                            };

                            var response = https.post({
	                            url: 'https://76t9n9mamf.execute-api.us-west-1.amazonaws.com/default/box2sp',
	                            body: JSON.stringify(payload)
                            });

                            log.debug('response', response);

                            noteId = JSON.parse(response.body).d.results[0];
                        } catch (e) {
                            log.error({
                                title: 'boxFileRecordToSP.aws',
                                details: {
                                    boxInfo: boxInfo,
                                    orderId: orderId,
                                    e: e
                                }
                            });
                        }

                        if (noteId != null) {
                            try {
                                var patchLoad = {
                                    "Id": noteId,
                                    "NoteTypeId": SP10_NOTE_TYPE
                                };
                                spGlobals.callapi('/OrderNote', false, 'patch', patchLoad);
                            } catch (e) {
                                log.error({
                                    title: 'boxFileRecordToSP.patch',
                                    details: {
                                        boxInfo: boxInfo,
                                        orderId: orderId,
                                        noteId: noteId,
                                        e: e
                                    }
                                });
                            }

                            try {
                                var values = {};
                                values[records.boxToSP.fields.spNoteId] = noteId;
                                record.submitFields({
                                    type: records.boxToSP.id,
                                    id: recid,
                                    values: values
                                });

                                output = true;
                            } catch (e) {
                                log.error({
                                    title: 'boxFileRecordToSP.submitFields',
                                    details: {
                                        recid: recid,
                                        values: values,
                                        e: e
                                    }
                                });
                            }
                        }
                    }
                }
            }

		    return output;
        }

		function boxFileRecordToSPOLD(recid) {
			var output = false;
			var boxInfo = boxInfoFromRecord(recid);

			if (boxInfo) {
				if (boxInfo[records.boxToSP.fields.salesOrder].length > 0) {
					var orderId = spGlobals.getSpOrderIdAPI(boxInfo[records.boxToSP.fields.salesOrder][0].value); //spGlobals.getSpOrderId(boxInfo[records.boxToSP.fields.salesOrder][0].value);
					log.debug({title: 'boxFileRecordToSP', details: 'orderId=' + orderId});
					if (orderId != null) {
						var boxFile = downloadFile(boxInfo[records.boxToSP.fields.boxFileId],
							boxInfo[records.boxToSP.fields.boxVersionId]);

						if (boxFile) {
							var noteId = spGlobals.uploadFileNote(orderId.toString(),
								boxFile.fileName,
								boxInfo[records.boxToSP.fields.description],
								boxFile['Content-Type'],
								boxFile.content);

							if (noteId) {
								try {
									var patchLoad = {
										"Id": noteId,
										"NoteTypeId": SP10_NOTE_TYPE
									};
									spGlobals.callapi('/OrderNote', false, 'patch', patchLoad);
								} catch (e) {
									log.error({
										title: 'NOTE_UPDATE_NOTETYPE',
										details: {
											e: e
										}
									});
								}

								var values = {};
								values[records.boxToSP.fields.spNoteId] = noteId;
								record.submitFields({
									type: records.boxToSP.id,
									id: recid,
									values: values
								});

								output = true;
							}
						}
					} else {
						// TODO could attempt lookup with api search, sp external id = ns internal id
						log.debug({
							title: 'BOX_TO_SP',
							details: JSON.stringify({
								recid: recid,
								notes: 'Unable to determine Service Pro order id'
							})
						});
					}
				}
			}

			return output;
		}

		return {
			records: records,
			apiCall: apiCall,
			listFolder: listFolder,
			makeFolder: makeFolder,
			getFolderInfo: getFolderInfo,
			downloadFile: downloadFile,
			uploadFile: uploadFile,
			createWebhook: createWebhook,
			deleteWebhook: deleteWebhook,
			createWebhookRecord: createWebhookRecord,
			getOrderFromExtKey: getOrderFromExtKey,
			getBoxFolderId: getBoxFolderId,
			createBoxToSPRecords: createBoxToSPRecords,
			createBoxToSPRecord: createBoxToSPRecord,
			boxFileRecordToSP: boxFileRecordToSP,
			getToken: getToken
		};

	});