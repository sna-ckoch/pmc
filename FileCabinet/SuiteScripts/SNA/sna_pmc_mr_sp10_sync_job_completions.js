/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 * 
 * Script brief description: 
 * Syncs the answers from job completion inspections to item fulfillments
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/03/05                           ckoch           Initial version
 *
 */
define(['N/runtime', 'N/record', 'N/search', 'N/file', './sna_pmc_mod_sp10_globals.js'],
    function (runtime, record, search, file, globals) {

        function getInputData(context) {
            var inspections = [];

            var inspectionLinkId = nullIfEmpty(runtime.getCurrentScript().getParameter({ name: 'custscript_sna_pmc_jc_inspection_linkid' }));
            if (inspectionLinkId == null) {
                log.error({ title: 'Inspection Link ID parameter is blank. Exiting' });
                return;
            }

            var cacheKey = '__clock_' + inspectionLinkId;

            var lastClock = nullIfEmpty(runtime.getCurrentScript().getParameter({ name: 'custscript_sna_pmc_jc_date_complete_over' }));
            if (lastClock == null) {
                lastClock = globals.getCache().get({
                    key: cacheKey
                });
            }

            if (lastClock == null) {
                var d = new Date();
                d.setHours(d.getHours() - 24); // default to 24hr lookback if no last run time
                lastClock = d.toISOString();
            }
            log.debug({ title: 'LASTCLOCK_FINAL', details: lastClock });

            var maxPages = 25;
            var pages = 0;

            try {
                // get all inspection results since last clock
                var url = '/InspectionQuestionResult?$filter=(InspectionResult/Inspection/InspectionLinkId eq ' + inspectionLinkId;
                //url += ' and InspectionResult/CompletedDateTime ge ' + lastClock;
                url += ' and ((InspectionResult/CompletedDateTime ge ' + lastClock + ') or ';
                url += ' (InspectionResult/CreatedDateTime ge ' + lastClock + ' and InspectionResult/CompletedDateTime ne null) or ';
                url += ' (InspectionResult/LastUpdatedDateTime ge ' + lastClock + ' and InspectionResult/CompletedDateTime ne null)))';
              	url += '&$expand=InspectionResult,InspectionResult/OrderLine,InspectionQuestion,InspectionQuestionChoiceResult,InspectionQuestionChoiceResult/InspectionQuestionChoice';

                do {
                    log.debug({ title: 'GET_INSPECTIONS_REQ', details: url });
                    var response = globals.callapi(url, true, 'get');
                    log.debug({ title: 'GET_INSPECTIONS_RES', details: JSON.stringify(response) });

                    // fix and store the clock from this request to be used next time
                    var newClock = response.d['__clock'] || null;

                    if (newClock != null) {
                        var parts = newClock.split(/\D+/);
                        if (parts.length == 6) {
                            // yyyy-mm-dd-hh-mm-ss UTC
                            var d = new Date(Date.UTC(parts[0], --parts[1], parts[2], parts[3], parts[4], parts[5]));
                          	d.setMinutes(d.getMinutes() - 10);
                            newClock = d.toISOString();

                            log.debug({ title: 'NEWCLOCK_FINAL', details: newClock });

                            globals.getCache().put({
                                key: cacheKey,
                                value: newClock
                            });
                        }
                    }

                    // pages of 20, gives us url to next page if any
                    url = response.d['__next'] || null;

                    if (url != null) {
                        url = url.replace('/api/', '/'); // callapi already has this bit
                        log.debug({ title: 'NEXT_FINAL', details: url });
                    }

                    inspections = inspections.concat(response.d.results);

                    pages++;
                } while (url != null && pages < maxPages);
            } catch (e) {
                log.error({ title: 'GET_ERROR', details: JSON.stringify(e) });
            }

            log.debug({ title: 'INSPECTIONS_LEN', details: inspections.length });

            return inspections;
        }

        function map(context) {
            var result = JSON.parse(context.value);

          	log.debug({ title: 'MAP.RESULT', details: result });

            var masterResultId = null;
            try {
                masterResultId = result['InspectionResult']['MasterInspectionResultId'] || null;
            } catch (e) {
                log.error({ title: 'MAP_INSPECTION', details: JSON.stringify(e) });
            }

            if (masterResultId != null) {
                context.write({
                    key: masterResultId,
                    value: result
                });
            }
        }

        function reduce(context) {
            var answers = context.values.map(JSON.parse);

            log.debug({ title: 'reduce', details: 'answers=' + answers.length });

            createStagingRecord(context.key, answers);
        }

        function createStagingRecord(resultId, answers) {
            if (answers.length == 0) {
                log.debug({ title: 'No answers. Exiting' });
                return;
            }

            if (recordExists(resultId) == true) {
                log.debug({ title: 'Record already exists. Exiting' });
                return;
            }

            var lineId = null;
            try {
                lineId = nullIfEmpty(answers[0]['InspectionResult']['OrderLine'][0]['Id']); // all answers will be linked to the same order line
            } catch (e) {
                log.error({
                    title: 'updateLineFields.lineId',
                    details: e
                });
            }

            log.debug({ title: 'updateLineFields.lineId', details: lineId });

            var sRec = record.create({
                type: 'customrecord_sna_pmc_job_completion'
            }).setValue({
                fieldId: 'custrecord_sna_pmc_jc_resultid',
                value: resultId
            }).setValue({
                fieldId: 'custrecord_sna_pmc_jc_linenumber',
                value: lineId
            });

            for (var a in answers) {
                log.debug({ title: 'answers[a]', details: answers[a] });

                var questionLinkId = nullIfEmpty(answers[a]['InspectionQuestion']['InspectionQuestionLinkId']);

                if (questionLinkId == '55e12f72-d408-4e66-af3c-7fa68073f096') {
                    // Is everything installed/repaired/delivered 100% complete?

                    try {
                        var choiceVal = (answers[a]['InspectionQuestionChoiceResult'][0]['InspectionQuestionChoice']['Value'] || '').toLowerCase();
                        log.debug({ title: 'complete.choiceVal', details: choiceVal });

                        if (choiceVal == 'y' || choiceVal == 'yes') {
                            sRec.setValue({
                                fieldId: 'custrecord_sna_pmc_jc_complete',
                                value: true
                            });
                        }
                    } catch (e) {

                    }
                } else if (questionLinkId == '139dcc29-fe5b-4cfe-9e16-ba25d6ae6de0') {
                    // Is everything installed PER PLAN and Instructions?

                    try {
                        var choiceVal = (answers[a]['InspectionQuestionChoiceResult'][0]['InspectionQuestionChoice']['Value'] || '').toLowerCase();
                        log.debug({ title: 'plan.choiceVal', details: choiceVal });

                        if (choiceVal == 'y' || choiceVal == 'yes') {
                            sRec.setValue({
                                fieldId: 'custrecord_sna_pmc_jc_plan',
                                value: true
                            });
                        }
                    } catch (e) {

                    }
                } else if (questionLinkId == '946ec646-4f98-4e2b-b2e6-7603a81ffaaa') {
                    // Customer Signature

                    var attachmentId = nullIfEmpty(answers[a]['AttachmentId']);
                    log.debug({ title: 'signature.attachmentId', details: attachmentId });

                    if (attachmentId != null) {
                        var attFile = getAttachment(attachmentId);

                        var sigFolder = nullIfEmpty(runtime.getCurrentScript().getParameter({ name: 'custscript_sna_pmc_jc_cust_sig_folderid' }));
                        if (sigFolder != null) {
                            attFile.folder = Number(sigFolder);

                            var fileId = null;
                            try {
                                fileId = attFile.save();

                                sRec.setValue({
                                    fieldId: 'custrecord_sna_pmc_jc_signature',
                                    value: fileId
                                });
                            } catch (e) {
                                log.error({
                                    title: 'attFile.save',
                                    details: e
                                });
                            }
                        }
                    }
                }
            }

            try {
                sRec.save();
            } catch (e) {
                log.error({
                    title: 'sRec.save',
                    details: e
                });
            }
        }

        function getAttachment(attachmentId) {
            var output = null;

            var attachmentFile = null;

            try {
                attachmentFile = globals.callapi('/Attachment/GetAttachment?id=' + attachmentId, false, 'get');

                log.debug({ title: 'attachmentFile', details: attachmentFile });
            } catch (e) {
                log.error({
                    title: 'DOWNLOAD_ATTACHMENT',
                    details: {
                        attachmentId: attachmentId,
                        e: e
                    }
                });
            }

            var contentDisp = nullIfEmpty(attachmentFile.headers['Content-Disposition']);

            if (attachmentFile != null && contentDisp != null) {
                var attFileName = null;
                try {
                    attFileName = /filename[^;=\n]*=(?:(\\?['"])(.*?)\1|(?:[^\s]+'.*?')?([^;\n]*))/.exec(contentDisp)[3] || null;
                } catch (e) {
                    log.error({
                        title: 'FILENAME_PARSE',
                        details: {
                            contentDisp: contentDisp,
                            e: e
                        }
                    });
                }

                var attFileType = mimeFileType(attachmentFile.headers['Content-Type']);

                log.debug({ title: 'attFileName', details: attFileName });
                log.debug({ title: 'attFileType', details: attFileType });

                if (attFileName != null && attFileType != null) {
                    try {
                        output = file.create({
                            name: attFileName,
                            fileType: attFileType,
                            contents: attachmentFile.body
                        });
                    } catch (e) {
                        log.error({
                            title: 'FILE_CREATE',
                            details: {
                                attFileName: attFileName,
                                attFileType: attFileType,
                                e: e
                            }
                        });
                    }
                }
            }

            return output;
        }

        function recordExists(resultId) {
            var output = false;

            search.create({
                type: 'customrecord_sna_pmc_job_completion',
                filters: [
                    ['custrecord_sna_pmc_jc_resultid', 'is', resultId]
                ],
                columns: ['internalid']
            }).run().each(function (result) {
                output = true;
                return false;
            });

            return output;
        }

        function summarize(context) {
            log.debug({ title: 'SUMMARIZE', details: JSON.stringify(context) });
        }

        function isEmpty(stValue) {
            return ((stValue === '' || stValue == null || stValue == undefined) || (stValue.constructor === Array && stValue.length == 0) || (stValue.constructor === Object && (function (v) {
                for (var k in v)
                    return false;
                return true;
            })(stValue)));
        }

        function nullIfEmpty(what) {
            return (isEmpty(what) ? null : what);
        }

        function mimeFileType(contentType) {
            var lookup = {};

            log.debug({ title: 'mimeFileType', details: contentType });

            lookup['application/x-autocad'] = file.Type.AUTOCAD;
            lookup['image/x-xbitmap'] = file.Type.BMPIMAGE;
            lookup['text/csv'] = file.Type.CSV;
            lookup['application/vnd.ms-excel'] = file.Type.EXCEL;
            lookup['application/x-shockwave-flash'] = file.Type.FLASH;
            lookup['image/gif'] = file.Type.GIFIMAGE;
            lookup['application/?x-?gzip-?compressed'] = file.Type.GZIP;
            lookup['text/html'] = file.Type.HTMLDOC;
            lookup['image/ico'] = file.Type.ICON;
            lookup['text/javascript'] = file.Type.JAVASCRIPT;
            lookup['image/jpeg'] = file.Type.JPGIMAGE;
            lookup['application/json'] = file.Type.JSON;
            lookup['message/rfc822'] = file.Type.MESSAGERFC;
            lookup['audio/mpeg'] = file.Type.MP3;
            lookup['video/mpeg'] = file.Type.MPEGMOVIE;
            lookup['application/vnd.ms-project'] = file.Type.MSPROJECT;
            lookup['application/pdf'] = file.Type.PDF;
            lookup['image/pjpeg'] = file.Type.PJPGIMAGE;
            lookup['text/plain'] = file.Type.PLAINTEXT;
            lookup['image/x-png'] = file.Type.PNGIMAGE;
            lookup['image/png'] = file.Type.PNGIMAGE;
            lookup['application/postscript'] = file.Type.POSTSCRIPT;
            lookup['application/?vnd.?ms-?powerpoint'] = file.Type.POWERPOINT;
            lookup['video/quicktime'] = file.Type.QUICKTIME;
            lookup['application/rtf'] = file.Type.RTF;
            lookup['application/sms'] = file.Type.SMS;
            lookup['text/css'] = file.Type.STYLESHEET;
            lookup['image/tiff'] = file.Type.TIFFIMAGE;
            lookup['application/vnd.visio'] = file.Type.VISIO;
            lookup['application/msword'] = file.Type.WORD;
            lookup['text/xml'] = file.Type.XMLDOC;
            lookup['application/zip'] = file.Type.ZIP;

            if (lookup.hasOwnProperty(contentType)) {
                return lookup[contentType];
            } else {
                return null;
            }
        }

        return {
            getInputData: getInputData,
            map: map,
            reduce: reduce,
            summarize: summarize
        }

    });