/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Query Service Pro for new inspection results
 * Upsert case mapping records
 * Create cases for existing mapping records
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/12/27                           ckoch           Initial version
 * 2020/02/15                           ckoch           Update mapping to look at parent rather than child inspections
 *
 */
define(['N/runtime', 'N/search', 'N/record', 'N/file', 'N/email', './sna_pmc_mod_sp10_globals.js', './sna_pmc_mod_box_globals.js'],
	function (runtime, search, record, file, email, globals, boxGlobals) {

		function getInputData(context) {
			var inspections = [];

			var lastClock = runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_sp10_inspdate_over'}) || null;

			if (lastClock == null) {
				lastClock = globals.getCache().get({
					key: '__inspclock'
				});
			}

			if (lastClock == null) {
				var d = new Date();
				d.setHours(d.getHours() - 24); // default to 24hr lookback if no last run time
				lastClock = d.toISOString();
			}
			log.debug({title: 'LASTCLOCK_FINAL', details: lastClock});

			var maxPages = 50;
			var pages = 0;

			try {
				// all inspections completed >= lastClock
				//var url = '/InspectionQuestionResult?$filter=InspectionResult/MasterInspectionResult/CompletedDateTime ge ' + lastClock + '&$expand=InspectionResult,InspectionResult/Inspection,InspectionResult/OrderLine,InspectionResult/OrderLine/OrderSegment,InspectionResult/OrderLine/OrderSegment/Order,InspectionQuestion,InspectionQuestionChoiceResult,InspectionQuestionChoiceResult/InspectionQuestionChoice';
				var url = '/InspectionQuestionResult?$filter=((InspectionResult/MasterInspectionResult/CompletedDateTime ge ' + lastClock + ') or ';
				url += ' (InspectionResult/MasterInspectionResult/CreatedDateTime ge ' + lastClock + ' and InspectionResult/MasterInspectionResult/CompletedDateTime ne null) or ';
				url += ' (InspectionResult/MasterInspectionResult/LastUpdatedDateTime ge ' + lastClock + ' and InspectionResult/MasterInspectionResult/CompletedDateTime ne null))';
				url += '&$expand=InspectionResult,InspectionResult/Inspection,InspectionResult/OrderLine,';
				url += 'InspectionResult/OrderLine/OrderSegment,InspectionResult/OrderLine/OrderSegment/Order,InspectionQuestion,';
				url += 'InspectionQuestionChoiceResult,InspectionQuestionChoiceResult/InspectionQuestionChoice';

				do {
					log.debug({title: 'GET_INSPECTIONS_REQ', details: url});
					var response = globals.callapi(url, true, 'get');
					log.debug({title: 'GET_INSPECTIONS_RES', details: JSON.stringify(response)});

					// fix and store the clock from this request to be used next time
					var newClock = response.d['__clock'] || null;

					if (newClock != null) {
						var parts = newClock.split(/\D+/);
						if (parts.length == 6) {
							// yyyy-mm-dd-hh-mm-ss UTC
							var d = new Date(Date.UTC(parts[0], --parts[1], parts[2], parts[3], parts[4], parts[5]));
							newClock = d.toISOString();

							log.debug({title: 'NEWCLOCK_FINAL', details: newClock});

							globals.getCache().put({
								key: '__inspclock',
								value: newClock
							});
						}
					}

					// pages of 20, gives us url to next page if any
					url = response.d['__next'] || null;

					if (url != null) {
						url = url.replace('/api/', '/'); // callapi already has this bit
						log.debug({title: 'NEXT_FINAL', details: url});
					}

					inspections = inspections.concat(response.d.results);

					pages++;
				} while (url != null && pages < maxPages);
			} catch (e) {
				log.error({title: 'GET_ERROR', details: JSON.stringify(e)});
			}

			log.debug({title: 'INSPECTION_LEN', details: inspections.length});

			return inspections;
		}

		function map(context) {
			//log.debug({ title: 'MAP_INSPECTION', details: JSON.stringify(context) });

			context.errors.iterator().each(function (key, error, executionNo) {

				log.error({
					title: 'Map error for key: ' + key + ', execution no  ' + executionNo,
					details: error
				});

				return true;
			});

			var inspection = JSON.parse(context.value);

			context.write({
				key: inspection['InspectionResult']['MasterInspectionResultId'],
				value: inspection
			});
		}

		function reduce(context) {
			context.errors.iterator().each(function (key, error, executionNo) {

				log.error({
					title: 'Reduce error for key: ' + key + ', execution no  ' + executionNo,
					details: error
				});

				return true;
			});

			var hasCase = hasExistingCase(context.key);
			if (hasCase == true) return;

			var values = context.values.map(JSON.parse);
			if (util.isArray(values) == false || values.length == 0) return;

			var orderId = getOrderId(values);
			log.debug({title: 'orderid', details: orderId});

			if (orderId != null) {
				processInspection(values, orderId);
			}
		}

		function hasExistingCase(inspectionId) {
			var output = false;

			log.debug({title: 'hasExistingCase.inspectionId', details: inspectionId});

			search.create({
				type: search.Type.SUPPORT_CASE,
				filters: [
					[globals.records.case.fields.inspectionId, 'is', inspectionId],
					'and',
					['isinactive', 'is', 'F']
				],
				columns: ['internalid']
			}).run().each(function (result) {
				output = true;
				return false;
			});

			log.debug({title: 'hasExistingCase.output', details: output});

			return output;
		}

		function processInspection(inspectionResult, orderId) {
			var inspectionLinkId = inspectionResult[0]['InspectionResult']['Inspection']['InspectionLinkId'] || null;
			var inspectionName = inspectionResult[0]['InspectionResult']['Inspection']['InspectionName'] || null;

			if (inspectionLinkId != null && inspectionName != null) {
				var ignoreIds = globals.getCache().get({
					key: 'ignoreids'
				});
				if (ignoreIds == null) {
					ignoreIds = [];
				} else {
					ignoreIds = JSON.parse(ignoreIds);
				}

				log.debug({title: 'ignoreIds', details: JSON.stringify(ignoreIds)});

				var mapping = getInspectionMapping(inspectionLinkId);

				if (mapping.length == 0) {
					if (ignoreIds.indexOf(inspectionLinkId) == -1) {
						ignoreIds.push(inspectionLinkId);

						globals.getCache().put({
							key: 'ignoreids',
							value: ignoreIds
						});

						createMappingRecords(inspectionLinkId, inspectionName, inspectionResult);
					}
				} else {
					var punchList = createCase(inspectionResult[0]['InspectionResult']['MasterInspectionResultId'], mapping, inspectionResult, orderId);

					log.debug({title: 'createCase.punchList', details: JSON.stringify(punchList)});

					syncAttachments(punchList);
				}
			}
		}

		function summarize(context) {
			context.mapSummary.errors.iterator().each(function (key, error, executionNo) {

				log.error({
					title: 'Map error for key: ' + key + ', execution no  ' + executionNo,
					details: error
				});

				return true;
			});

			log.debug({title: 'SUMMARIZE', details: JSON.stringify(context)});
		}

		function syncAttachments(punchList) {
			if (punchList.caseRecordId != null && punchList.attachments.length > 0) {
				var boxFolderId = boxGlobals.getBoxFolderId('supportcase', punchList.caseRecordId);

				log.debug({title: 'boxFolderId.before', details: boxFolderId});

				if (boxFolderId == null) {
					boxFolderId = createNewBoxFolder(punchList.caseRecordId);
				}

				log.debug({title: 'boxFolderId.after', details: boxFolderId});

				var inboundFolder = getOrCreateInboundFolder(boxFolderId);

				log.debug({title: 'inboundFolder', details: JSON.stringify(inboundFolder)});

				if (inboundFolder.id != null && inboundFolder.email != null) {
					for (var i in punchList.attachments) {
						var attachmentId = punchList.attachments[i].attachmentId || null;
						var questionLabel = punchList.attachments[i].questionLabel || null;

						if (attachmentId != null && questionLabel != null) {
							var attachmentFile = null;

							try {
								attachmentFile = globals.callapi('/Attachment/GetAttachment?id=' + attachmentId, false, 'get');
							} catch (e) {
								log.error({
									title: 'DOWNLOAD_ATTACHMENT',
									details: JSON.stringify({
										attachment: punchList.attachments[i],
										e: e
									})
								});
							}

							if (attachmentFile != null && attachmentFile.headers['Content-Disposition'] != null) {
								var attFileName = null;
								try {
									attFileName = /filename[^;=\n]*=(?:(\\?['"])(.*?)\1|(?:[^\s]+'.*?')?([^;\n]*))/.exec(attachmentFile.headers['Content-Disposition'])[3] || null;
								} catch (e) {
									log.error({
										title: 'FILENAME_PARSE',
										details: JSON.stringify({
											contentDisp: attachmentFile.headers['Content-Disposition'],
											e: e
										})
									});
								}

								if (attFileName == null) {
									try {
										attFileName = /filename="(.[^"]*)"/.exec(attachmentFile.headers['Content-Disposition'])[1] || null;
									} catch (e) {
										log.error({
											title: 'FILENAME_PARSE',
											details: JSON.stringify({
												contentDisp: attachmentFile.headers['Content-Disposition'],
												e: e
											})
										});
									}
								}

								if (attFileName != null && questionLabel != null) {
									attFileName = questionLabel + ' ' + attFileName;
								}

								var attFileType = mimeFileType(attachmentFile.headers['Content-Type']);

								log.debug({title: 'attFileName', details: attFileName});
								log.debug({title: 'attFileType', details: attFileType});

								if (attFileName != null && attFileType != null) {
									var attFile = null;

									try {
										attFile = file.create({
											name: attFileName,
											fileType: attFileType,
											contents: attachmentFile.body
										});
									} catch (e) {
										log.error({
											title: 'FILE_CREATE',
											details: JSON.stringify({
												attFileName: attFileName,
												attFileType: attFileType,
												e: e
											})
										});
									}

									if (attFile != null) {
										email.send({
											author: runtime.getCurrentScript().getParameter({
												name: 'custscript_sna_pmc_sp10_email_sender'
											}),
											recipients: [
												inboundFolder.email
											],
											subject: 'ServicePro Punchlist File',
											body: 'ServicePro Punchlist File',
											attachments: [
												attFile
											]
										});
									}
								}
							}
						}
					}
				}
			}
		}

		function getOrCreateInboundFolder(boxFolderId) {
			var output = {
				id: null,
				email: null
			};

			if (boxFolderId != null) {
				try {
					var inbound = getInboundFolder(boxFolderId);

					if (inbound == null) {
						var inboundFolder = runtime.getCurrentScript().getParameter({
							name: 'custscript_sna_pmc_sp10_inbound_folder'
						}) || 'ServicePro In';

						var response = boxGlobals.makeFolder(inboundFolder, boxFolderId, true);

						inbound = {
							id: response.id,
							email: (response.folder_upload_email || {}).email
						}
					}

					if (inbound != null) {
						output = inbound;
					}
				} catch (e) {
					log.error({
						title: 'GET_OR_CREATE_INBOUND_FOLDER',
						details: JSON.stringify({
							boxFolderId: boxFolderId,
							e: e
						})
					});
				}
			}

			return output;
		}

		function getInboundFolder(boxFolderId) {
			var output = null;

			if (boxFolderId != null) {
				var folderContents = boxGlobals.listFolder(boxFolderId);

				if (util.isArray(folderContents['entries'])) {
					var inboundFolder = runtime.getCurrentScript().getParameter({
						name: 'custscript_sna_pmc_sp10_inbound_folder'
					}) || 'ServicePro In';

					for (var f in folderContents.entries) {
						var itemType = folderContents.entries[f].type || null;
						var itemName = folderContents.entries[f].name || '';

						if (itemType == 'folder' && itemName.toLowerCase() == inboundFolder.toLowerCase()) {
							var folderInfo = boxGlobals.getFolderInfo(folderContents.entries[f].id);

							output = {
								id: folderInfo.id,
								email: (folderInfo.folder_upload_email || {}).email
							}
						}
					}
				}
			}

			return output;
		}

		function createNewBoxFolder(caseRecordId) {
			var output = null;

			var caseNumber = getCaseNumber(caseRecordId);

			if (caseNumber) {
				var parentFolderId = runtime.getCurrentScript().getParameter({
					name: 'custscript_sna_pmc_sp10_box_parent_id'
				});

				var orderFolder = boxGlobals.makeFolder(caseNumber, parentFolderId);

				output = (orderFolder || {}).id || null;

				if (output != null) {
					try {
						record.create({
							type: boxGlobals.records.boxRecordFolder.id
						}).setValue({
							fieldId: boxGlobals.records.boxRecordFolder.fields.recordType,
							value: 'supportcase'
						}).setValue({
							fieldId: boxGlobals.records.boxRecordFolder.fields.recordId,
							value: caseRecordId
						}).setValue({
							fieldId: boxGlobals.records.boxRecordFolder.fields.folderId,
							value: output
						}).save();
					} catch (e) {
						log.error({
							title: 'CREATE_BOX_FOLDER_REC',
							details: JSON.stringify({
								salesOrderId: caseRecordId,
								e: JSON.stringify(e)
							})
						});
					}
				}
			}

			return output;
		}

		function getCaseNumber(caseRecordId) {
			var output = null;

			try {
				output = search.lookupFields({
					type: search.Type.SUPPORT_CASE,
					id: caseRecordId,
					columns: ['casenumber']
				})['casenumber'] || null;
			} catch (e) {
				log.debug({
					title: 'GET_CASENUMBER',
					details: JSON.stringify({
						caseRecordId: caseRecordId,
						e: e
					})
				});
			}

			return output;
		}

		function getInspectionMapping(linkId) {
			var output = [];

			search.create({
				type: globals.records.caseQuestionMapping.id,
				filters: [
					[globals.records.caseQuestionMapping.fields.inspectionParent + '.' + globals.records.caseMapping.fields.inspectionLinkId, 'is', linkId],
					'and',
					['isinactive', 'is', 'F']
				],
				columns: [
					globals.records.caseQuestionMapping.fields.questionLinkId,
					globals.records.caseQuestionMapping.fields.mapCaseField,
					globals.records.caseQuestionMapping.fields.defaultValue,
					globals.records.caseQuestionMapping.fields.boxAttachment
				]
			}).run().each(function (result) {
				output.push({
					questionLinkId: result.getValue({name: globals.records.caseQuestionMapping.fields.questionLinkId}),
					mapCaseField: result.getValue({name: globals.records.caseQuestionMapping.fields.mapCaseField}),
					defaultValue: result.getValue({name: globals.records.caseQuestionMapping.fields.defaultValue}),
					boxAttachment: (result.getValue({name: globals.records.caseQuestionMapping.fields.boxAttachment}) == true)
				});

				return true;
			});

			log.debug({
				title: 'GET_INSPECTION_MAPPING',
				details: JSON.stringify({
					linkId: linkId,
					output: output
				})
			});

			return output;
		}

		function createMappingRecords(linkId, inspectionName, inspectionResult) {
			var caseMappingId = null;

			log.debug({
				title: 'CREATE_MAPPING_RECORDS',
				details: JSON.stringify({
					linkId: linkId,
					inspectionName: inspectionName,
					inspectionResult: inspectionResult
				})
			});

			try {
				caseMappingId = record.create({
					type: globals.records.caseMapping.id
				}).setValue({
					fieldId: globals.records.caseMapping.fields.inspectionName,
					value: inspectionName
				}).setValue({
					fieldId: globals.records.caseMapping.fields.inspectionLinkId,
					value: linkId
				}).save();
			} catch (e) {
				log.error({
					title: 'MAPPING_CREATE',
					details: JSON.stringify({
						linkId: linkId,
						inspectionName: inspectionName,
						e: e
					})
				});
			}

			if (caseMappingId != null) {
				var answers = inspectionResult || [];

				log.debug({title: 'answers', details: JSON.stringify(answers)});

				for (var i in answers) {
					try {
						var questionLabel = answers[i]['InspectionQuestion']['QuestionLabel'] || null;
						var questionLinkId = answers[i]['InspectionQuestion']['InspectionQuestionLinkId'] || null;

						if (questionLabel != null && questionLinkId != null) {
							record.create({
								type: globals.records.caseQuestionMapping.id
							}).setValue({
								fieldId: globals.records.caseQuestionMapping.fields.inspectionParent,
								value: caseMappingId
							}).setValue({
								fieldId: globals.records.caseQuestionMapping.fields.questionLabel,
								value: questionLabel
							}).setValue({
								fieldId: globals.records.caseQuestionMapping.fields.questionLinkId,
								value: questionLinkId
							}).save();
						}
					} catch (e) {
						log.error({
							title: 'MAPPING_QUESTION_CREATE',
							details: JSON.stringify({
								linkId: linkId,
								inspectionName: inspectionName
							})
						});
					}
				}
			}
		}

		function getOrderId(inspectionResult) {
			var output = null;

			try {
				output = inspectionResult[0]['InspectionResult']['OrderLine'][0]['OrderSegment']['Order']['ExternalId'] || null;
			} catch (e) {
				log.debug({
					title: 'GET_ORDER_ID',
					details: JSON.stringify({
						e: e,
						inspectionResult: inspectionResult
					})
				});
			}

			return output;
		}

		function getEntity(orderId) {
			var output = null;

			try {
				output = search.lookupFields({
					type: 'transaction',
					id: orderId,
					columns: ['entity']
				})['entity'][0].value;
			} catch (e) {
				log.error({
					title: 'GET_ENTITY',
					details: JSON.stringify({
						orderId: orderId,
						e: e
					})
				});
			}

			return output;
		}

		function forceFloat(what) {
		    return (isNaN(parseFloat(what)) ? 0 : parseFloat(what));
        }

		function getClientAdvocate(orderId) {
		    var output = null;

		    var employeeId = null;

		    if (forceFloat(orderId) > 0) {
                search.create({
                    type: search.Type.SALES_ORDER,
                    filters: [
                        ['internalid', search.Operator.ANYOF, orderId],
                        'and',
                        ['mainline', search.Operator.IS, 'T']
                    ],
                    columns: [
                        'custbody_pmc_order_type', 'custbody_pmc_project_coordinator',
                        'custbody_esp_parent_order.custbody_pmc_order_type', 'custbody_esp_parent_order.custbody_pmc_project_coordinator'
                    ]
                }).run().each(function(result){
                    var orderType = forceFloat(result.getValue({name: 'custbody_pmc_order_type'}));
                    var clientAdvocate = forceFloat(result.getValue({name: 'custbody_pmc_project_coordinator'}));
                    var parentOrderType = forceFloat(result.getValue({name: 'custbody_pmc_order_type', join: 'custbody_esp_parent_order'}));
                    var parentClientAdvocate = forceFloat(result.getValue({name: 'custbody_pmc_project_coordinator', join: 'custbody_esp_parent_order'}));

                    if (orderType === 6 || orderType === 7) {
                        // 6=Warranty, 7=Service
                        if (clientAdvocate > 0) {
                            employeeId = clientAdvocate;
                        }
                    } else if ((orderType === 17) && (parentOrderType === 6 || parentOrderType === 7)) {
                        // 17=Work Order
                        if (parentClientAdvocate > 0) {
                            employeeId = parentClientAdvocate;
                        }
                    }
                });
            }

		    if (forceFloat(employeeId) > 0) {
		        search.create({
                    type: search.Type.EMPLOYEE,
                    filters: [
                        ['internalid', search.Operator.ANYOF, employeeId],
                        'and',
                        ['isinactive', search.Operator.IS, 'F'],
                        'and',
                        ['supportrep', search.Operator.IS, 'T']
                    ],
                    columns: ['internalid']
                }).run().each(function(result){
                    output = result.id;
                });
            }

		    return output;
        }

		// returns case record id and array of attachments to be downloaded/linked after creating case
		function createCase(inspectionId, mapping, inspectionResult, orderId) {
			var output = {
				caseRecordId: null,
				attachments: []
			};

			try {
				var reportAttId = inspectionResult[0]['InspectionResult']['AttachmentId'] || null;
				if (reportAttId != null) {
					output.attachments.push({
						attachmentId: reportAttId,
						questionLabel: 'Report'
					});
				}
			} catch (e) {
				log.debug({
					title: 'REPORT_ATTACHMENT',
					details: JSON.stringify({
						reportAttId: reportAttId,
						e: e
					})
				});
			}

			var caseRecord = record.create({
				type: record.Type.SUPPORT_CASE
			});

			var assignedTo = getClientAdvocate(orderId);
			if (forceFloat(assignedTo) > 0) {
                caseRecord.setValue({
                    fieldId: 'assigned',
                    value: assignedTo
                });
            }

			caseRecord.setValue({
				fieldId: globals.records.case.fields.inspectionId,
				value: inspectionId
			});

			if (orderId != null) {
				log.debug({title: 'set.entity', details: getEntity(orderId)});

				caseRecord.setValue({
					fieldId: 'company',
					value: getEntity(orderId)
				});

				caseRecord.setValue({
					fieldId: 'transaction',
					value: orderId
				});
			}

			var saveRecord = false;

			log.debug({title: 'mapping', details: JSON.stringify(mapping)});

			for (var m in mapping) {
				var questionLinkId = mapping[m].questionLinkId || null;
				var mapCaseField = mapping[m].mapCaseField || null;
				var defaultValue = mapping[m].defaultValue || null;
				var boxAttachment = mapping[m].boxAttachment || false;

				if (defaultValue == 'true') {
					defaultValue = true;
				} else if (defaultValue == 'false') {
					defaultValue = false;
				}

				if (questionLinkId != null && boxAttachment == true) {
					log.debug({title: 'map.inspectionResult.attachment', details: JSON.stringify(inspectionResult)});

					var answers = inspectionResult || [];

					for (var i in answers) {
						log.debug({title: 'answer[i]', details: JSON.stringify(answers[i])});

						if (questionLinkId == answers[i]['InspectionQuestion']['InspectionQuestionLinkId']) {
							var attachmentId = answers[i]['AttachmentId'] || null;
							var questionLabel = answers[i]['InspectionQuestion']['QuestionLabel'] || null;

							if (attachmentId != null && questionLabel != null) {
								output.attachments.push({
									attachmentId: attachmentId,
									questionLabel: questionLabel
								});
							}
						}
					}
				} else if (questionLinkId != null && mapCaseField != null) {
					log.debug({title: 'map.inspectionResult', details: JSON.stringify(inspectionResult)});

					var answers = inspectionResult || [];

					for (var i in answers) {
						log.debug({title: 'answer[i]', details: JSON.stringify(answers[i])});

						if (questionLinkId == answers[i]['InspectionQuestion']['InspectionQuestionLinkId']) {
							var choiceList = answers[i]['InspectionQuestionChoiceResult'] || [];

							log.debug({title: 'choicelist', details: JSON.stringify(choiceList)});

							if (choiceList.length == 0) {
								log.debug({
									title: 'setvalue',
									details: 'field=' + mapCaseField + ',value=' + answers[i].Value
								});

								caseRecord.setValue({
									fieldId: mapCaseField,
									value: answers[i].Value
								});
							} else {
								log.debug({
									title: 'setvalue.choice',
									details: 'field=' + mapCaseField + ',text=' + getChoiceText(answers[i])
								});

								caseRecord.setText({
									fieldId: mapCaseField,
									text: getChoiceText(answers[i])
								});
							}

							saveRecord = true;
						}
					}
				} else if (questionLinkId == null && mapCaseField != null) {
					log.debug({
						title: 'setvalue.default',
						details: 'field=' + mapCaseField + ',default=' + defaultValue
					});

					if (util.isString(defaultValue) && defaultValue.lastIndexOf('salesorder.') === 0) {
						defaultValue = lookupField(search.Type.SALES_ORDER, orderId, defaultValue.split('.')[1]);
					}

					caseRecord.setValue({
						fieldId: mapCaseField,
						value: defaultValue
					});

					saveRecord = true;
				}
			}

			var caseId = null;

			if (saveRecord) {
				try {
					log.debug({title: 'saveRecord', details: JSON.stringify(caseRecord)});

					caseId = caseRecord.save();
				} catch (e) {
					log.error({
						title: 'CASE_CREATE',
						details: JSON.stringify({
							inspectionId: inspectionId,
							e: e
						})
					});
				}

				if (caseId != null) {
					try {
						record.attach({
							record: {
								type: record.Type.SUPPORT_CASE,
								id: caseId
							},
							to: {
								type: 'transaction',
								id: orderId
							}
						});

						output.caseRecordId = caseId;
					} catch (e) {
						log.error({
							title: 'CASE_ATTACH',
							details: JSON.stringify({
								caseId: caseId,
								e: e
							})
						});
					}
				}
			}

			return output;
		}


		function lookupField(recType, recId, fieldId) {
			var output = null;

			try {
				output = search.lookupFields({
					type: recType,
					id: recId,
					columns: [fieldId]
				})[fieldId] || null;

				if (util.isArray(output)) {
					if (output.length > 0) {
						output = output[0].value;
					} else {
						output = null;
					}
				}
			} catch (e) {
				log.error({
					title: 'lookupField',
					details: JSON.stringify({
						recType: recType,
						recId: recId,
						fieldId: fieldId,
						e: e
					})
				});
			}

			log.debug({
				title: 'lookupField',
				details: JSON.stringify({
					recType: recType,
					recId: recId,
					fieldId: fieldId,
					output: output
				})
			});

			return output;
		}

		function getChoiceText(inspectionLine) {
			var output = '';

			try {
				output = inspectionLine['InspectionQuestionChoiceResult'][0]['InspectionQuestionChoice']['Value'] || '';
			} catch (e) {
				log.error({
					title: 'GET_CHOICE_TEXT',
					details: JSON.stringify({
						e: e,
						inspectionLine: inspectionLine
					})
				});
			}

			return output;
		}

		function mimeFileType(contentType) {
			var lookup = {};

			log.debug({title: 'mimeFileType', details: contentType});

			lookup['application/x-autocad'] = file.Type.AUTOCAD;
			lookup['image/x-xbitmap'] = file.Type.BMPIMAGE;
			lookup['text/csv'] = file.Type.CSV;
			lookup['application/vnd.ms-excel'] = file.Type.EXCEL;
			lookup['application/x-shockwave-flash'] = file.Type.FLASH;
			lookup['image/gif'] = file.Type.GIFIMAGE;
			lookup['application/?x-?gzip-?compressed'] = file.Type.GZIP;
			lookup['text/html'] = file.Type.HTMLDOC;
			lookup['image/ico'] = file.Type.ICON;
			lookup['text/javascript'] = file.Type.JAVASCRIPT;
			lookup['image/jpeg'] = file.Type.JPGIMAGE;
			lookup['application/json'] = file.Type.JSON;
			lookup['message/rfc822'] = file.Type.MESSAGERFC;
			lookup['audio/mpeg'] = file.Type.MP3;
			lookup['video/mpeg'] = file.Type.MPEGMOVIE;
			lookup['application/vnd.ms-project'] = file.Type.MSPROJECT;
			lookup['application/pdf'] = file.Type.PDF;
			lookup['image/pjpeg'] = file.Type.PJPGIMAGE;
			lookup['text/plain'] = file.Type.PLAINTEXT;
			lookup['image/x-png'] = file.Type.PNGIMAGE;
			lookup['image/png'] = file.Type.PNGIMAGE;
			lookup['application/postscript'] = file.Type.POSTSCRIPT;
			lookup['application/?vnd.?ms-?powerpoint'] = file.Type.POWERPOINT;
			lookup['video/quicktime'] = file.Type.QUICKTIME;
			lookup['application/rtf'] = file.Type.RTF;
			lookup['application/sms'] = file.Type.SMS;
			lookup['text/css'] = file.Type.STYLESHEET;
			lookup['image/tiff'] = file.Type.TIFFIMAGE;
			lookup['application/vnd.visio'] = file.Type.VISIO;
			lookup['application/msword'] = file.Type.WORD;
			lookup['text/xml'] = file.Type.XMLDOC;
			lookup['application/zip'] = file.Type.ZIP;

			if (lookup.hasOwnProperty(contentType)) {
				return lookup[contentType];
			} else {
				return null;
			}
		}

		return {
			getInputData: getInputData,
			map: map,
			reduce: reduce,
			summarize: summarize
		}

	});