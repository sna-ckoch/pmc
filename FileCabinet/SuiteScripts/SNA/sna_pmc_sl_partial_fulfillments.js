/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Adds partial delivery line to sales order
 * Done in suitelet so other scripts fire on update
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/01/27                           ckoch           Initial version
 *
 */
define(['N/record', 'N/http', 'N/url', 'N/https', 'N/search'], function (record, http, url, https, search) {

	function onRequest(context) {
		log.debug({title: 'REQUEST', details: JSON.stringify(context)});

		if (context.request.method == http.Method.GET) {
			handleGet(context);
		} else {
			context.response.write({output: 'Request not supported'});
		}
	}

	function handleGet(context) {
		var soId = context.request.parameters['custpage_soid'] || null;
		var ifId = context.request.parameters['custpage_ifid'] || null;
		var tranId = context.request.parameters['custpage_tranid'] || null;
		var itemId = context.request.parameters['custpage_itemid'] || null;
		var action = context.request.parameters['custpage_action'] || null;
		var retry = (context.request.parameters['custpage_retry'] == 'T');

		log.debug({
			title: 'handleGet.params',
			details: {
				soId: soId,
				ifId: ifId,
				tranId: tranId,
				itemId: itemId,
				action: action,
				retry: retry
			}
		});

		if (soId != null && itemId != null && action == 'update') {
			var retryUpdate = false;

			try {
				updateSalesOrder(soId, ifId, tranId, itemId);
			} catch (e) {
				log.error({
					title: 'updateSalesOrder',
					details: {
						soId: soId,
						ifId: ifId,
						tranId: tranId,
						itemId: itemId,
						e: e
					}
				});

				if ((e || {})['name'] == 'RCRD_HAS_BEEN_CHANGED' && retry == false) {
					retryUpdate = true;
				}
			}

			if (retryUpdate) {
				try {
					log.debug({
						title: 'updateSalesOrder.retry',
						details: 'Unable to save record changes, trying one more time'
					});

					var suitelet = url.resolveScript({
						scriptId: 'customscript_sna_pmc_sl_partial_delivery',
						deploymentId: 'customdeploy_sna_pmc_sl_partial_delretry',
						params: {
							'custpage_soid': soId,
							'custpage_ifid': ifId,
							'custpage_tranid': tranId,
							'custpage_itemid': itemId,
							'custpage_action': action,
							'custpage_retry': 'T'
						},
						returnExternalUrl: true
					});

					// using suitelet so ue's trigger
					https.get.promise({url: suitelet});
				} catch (e) {
					log.error({
						title: 'updateSalesOrder.retry',
						details: {
							soId: soId,
							ifId: ifId,
							tranId: tranId,
							itemId: itemId,
							e: e
						}
					});
				}
			}
		} else {
			context.response.write({output: 'Request not supported'});
		}
	}

	function updateSalesOrder(soId, ifId, tranId, itemId) {
		var unlinkedFulfillments = getUnlinkedFulfillments(soId);

		var newItemDesc = unlinkedFulfillments.join(' | ');

		if (itemId != null) {
			if (unlinkedFulfillments.length > 0 && findExistingLine(soId, itemId, newItemDesc)) {
				log.debug({
					title: 'updateSalesOrder.skip',
					details: {
						message: 'Line already exists, not updating',
						soId: soId,
						itemId: itemId,
						newItemDesc: newItemDesc
					}
				});

				return;
			}
		}

		var soRec = record.load({
			type: record.Type.SALES_ORDER,
			id: soId,
			isDynamic: true
		});

		var existingLine = -1;

		for (var i = 0; i < soRec.getLineCount({sublistId: 'item'}); i++) {
			var lineItemId = soRec.getSublistValue({sublistId: 'item', fieldId: 'item', line: i});
			var lineItemDesc = soRec.getSublistValue({sublistId: 'item', fieldId: 'description', line: i}) || '';

			if (lineItemId == itemId) {
				var parts = lineItemDesc.split(' | ');

				for (var p in parts) {
					if ((unlinkedFulfillments.indexOf(parts[p]) > -1) || (parts.indexOf(tranId) > -1)) {
						existingLine = i;
						break;
					}
				}

				if (existingLine > -1) {
					break;
				}
			}
		}

		if (existingLine > -1) {
			soRec.selectLine({sublistId: 'item', line: existingLine});
		} else {
			soRec.selectNewLine({sublistId: 'item'});
			soRec.setCurrentSublistValue({sublistId: 'item', fieldId: 'item', value: itemId});
			soRec.setCurrentSublistValue({sublistId: 'item', fieldId: 'isclosed', value: true});
		}

		soRec.setCurrentSublistValue({sublistId: 'item', fieldId: 'description', value: newItemDesc});
		soRec.commitLine({sublistId: 'item'});

		soRec.save({ignoreMandatoryFields: true});
	}

	function findExistingLine(soId, itemId, newItemDesc) {
		var output = false;

		search.create({
			type: search.Type.SALES_ORDER,
			filters: [
				['internalid', 'anyof', soId],
				'and',
				['item', 'anyof', itemId],
				'and',
				['memo', 'is', newItemDesc]
			],
			columns: ['internalid']
		}).run().each(function (result) {
			output = true;
			return false;
		});

		return output;
	}

	function getUnlinkedFulfillments(soId) {
		var output = [];

		var colTranId = search.createColumn({name: 'tranid', summary: search.Summary.GROUP, sort: search.Sort.ASC});

		search.create({
			type: search.Type.ITEM_FULFILLMENT,
			filters: [
				['mainline', 'is', 'T'],
				'and',
				['custbody_sna_pmc_job_completion', 'anyof', '@NONE@'],
				'and',
				['item.custitem_sna_pmc_no_job_completion', 'is', 'F'],
				'and',
				[
					['custbody_sna_pmc_child_salesorder', 'anyof', soId],
					'or',
					['createdfrom', 'anyof', soId]
				]
			],
			columns: [colTranId]
		}).run().each(function (result) {
			output.push(result.getValue(colTranId));

			return true;
		});

		log.debug({
			title: 'unlinkedFulfillments',
			details: output
		});

		return output;
	}

	return {
		onRequest: onRequest
	}

});