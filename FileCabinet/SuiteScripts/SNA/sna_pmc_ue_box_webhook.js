/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 * Script brief description: 
   Handles creating/deleting Box webhooks on record events
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/12/07                           ckoch           Initial version
 *
 */
define(['./sna_pmc_mod_box_globals.js'], function (boxGlobals) {

    function beforeSubmit(context) {
        var events = {};
        events[context.UserEventType.CREATE] = handleCreate;
        events[context.UserEventType.DELETE] = handleDelete;

        if (typeof events[context.type] === 'function') {
            events[context.type](context);
        }
    }

    function handleCreate(context) {
        boxGlobals.createWebhook(context.newRecord);
    }

    function handleDelete(context) {
        var webhookId = context.oldRecord.getValue({ fieldId: boxGlobals.records.webhook.fields.webhookId }) || null;

        if (webhookId != null) {
            boxGlobals.deleteWebhook(webhookId);
        }
    }

    return {
        beforeSubmit: beforeSubmit
    }

});