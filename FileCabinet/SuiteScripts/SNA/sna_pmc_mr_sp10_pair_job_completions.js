/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Searches for job completions that haven't been paired with a fulfillment and attempts again
 *
 * Reason a job completion wouldn't be paired with an item fulfillment is if servicepro
 * hasn't yet updated the line numbers on the sales order - we need that to know what line
 * item to update
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2021/04/01                           ckoch           Initial version
 *
 */
define(['N/record', 'N/search'], function (record, search) {

	function getInputData(context) {
		return search.create({
			type: 'customrecord_sna_pmc_job_completion',
			filters: [
				['custrecord_sna_pmc_jc_matched', 'is', 'F'],
				'and',
				['custrecord_sna_pmc_jc_linenumber', 'isnotempty', ''],
				'and',
				['isinactive', 'is', 'F']
			],
			columns: ['internalid']
		});
	}

	function map(context) {
		log.debug({title: 'MAP.CONTEXT', details: context});

		// a UE on the job completion record handles pairing so we just need to load/save it to trigger

		record.load({
			type: 'customrecord_sna_pmc_job_completion',
			id: context.key
		}).save();
	}

	function summarize(context) {
		log.debug({title: 'SUMMARIZE', details: JSON.stringify(context)});
	}

	return {
		getInputData: getInputData,
		map: map,
		summarize: summarize
	}

});