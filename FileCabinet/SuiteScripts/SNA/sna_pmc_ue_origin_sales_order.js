/**
 * Copyright (c) 2021, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.0
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Populates Origin Sales Order field on sales orders (text value)
 * - if PRIMARY/WORK ORDER (DEPENDENT) = Dependent(or Work Order), then populate custbody_esp_parent_order
 * - else, populate with current record / self reference
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2021/10/21                           ckoch           initial version
 *
 */
define(['N/record', 'N/search'], function (record, search) {

	function afterSubmit(context) {
		var router = {};
		router[context.UserEventType.CREATE] = updateOrigin;

		if (typeof router[context.type] !== 'function') {
			return;
		}

		router[context.type](context);
	}

	function updateOrigin(context) {
		var recType = context.newRecord.type;
		var recId = context.newRecord.id;

		var origin = getOrigin(recId);

		if (!isEmpty(origin)) {
			try {
				record.submitFields({
					type: recType,
					id: recId,
					values: {
						'custbody_origin_sales_order': origin
					}
				});
			} catch (e) {
				log.error({
					title: 'updateOrigin',
					details: {
						recType: recType,
						recId: recId,
						origin: origin,
						e: e
					}
				});
			}
		}
	}

	function getOrigin(orderId) {
		var output = null;

		try {
			var lookups = search.lookupFields({
				type: search.Type.SALES_ORDER,
				id: orderId,
				columns: [
					'tranid',
					'custbody_esp_parent_order.tranid',
					'custbody_esp_primary_dependent_order'
				]
			});

			log.debug({
				title: 'getOrigin.lookups',
				details: {
					orderId: orderId,
					lookups: lookups
				}
			});

			output = nullIfEmpty(lookups['tranid']);

			var orderType = lookups['custbody_esp_primary_dependent_order'];

			if (util.isArray(orderType)) {
				if (orderType.length === 1) {
					if (orderType[0].value === '2') {
						output = nullIfEmpty(lookups['custbody_esp_parent_order.tranid']);
					}
				}
			}
		} catch (e) {
			log.error({
				title: 'getOrigin.error',
				details: {
					orderId: orderId,
					e: e
				}
			});
		}

		log.debug({
			title: 'getOrigin.output',
			details: {
				orderId: orderId,
				output: output
			}
		});

		return output;
	}

	function isEmpty(stValue) {
		return ((stValue === '' || stValue == null || stValue == undefined) || (stValue.constructor === Array && stValue.length == 0) || (stValue.constructor === Object && (function (v) {
			for (var k in v)
				return false;
			return true;
		})(stValue)));
	}

	function nullIfEmpty(what) {
		return (isEmpty(what) ? null : what);
	}

	return {
		afterSubmit: afterSubmit
	}

});