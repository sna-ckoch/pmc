/**
 * Copyright (c) 2022, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.0
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Creates "Imported Employee Expense" records from Corporate Card Expense custom records
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2022/02/24                           ckoch           Initial version
 *
 */
define(['N/record'], function (record) {

	var RECORDS = {
		corpCardExpense: {
			id: 'customrecord_sna_pmc_corp_card_exp',
			fields: {
				expTranId: 'custrecord_sna_pmc_cce_tranid',
				expSource: 'custrecord_sna_pmc_cce_source',
				expDate: 'custrecord_sna_pmc_cce_date',
				expEmployee: 'custrecord_sna_pmc_cce_employee',
				expMemo: 'custrecord_sna_pmc_cce_memo',
				expAmount: 'custrecord_sna_pmc_cce_amount',
				importedExpense: 'custrecord_sna_pmc_cce_imp_emp_exp'
			}
		}
	};

	function beforeSubmit(context) {
		var events = {};
		events[context.UserEventType.CREATE] = upsertExpense;
		events[context.UserEventType.EDIT] = upsertExpense;

		if (typeof events[context.type] === 'function') {
			events[context.type](context);
		}
	}

	function upsertExpense(context) {
		var rec = context.newRecord;

		var impExp = rec.getValue({fieldId: RECORDS.corpCardExpense.fields.importedExpense}) || null;

		if (impExp === null) {
			var impRec = record.create({type: 'importedemployeeexpense'});

			impRec.setValue({
				fieldId: 'transactionid',
				value: rec.getValue({fieldId: RECORDS.corpCardExpense.fields.expTranId})
			}).setValue({
				fieldId: 'sourcetype',
				value: rec.getValue({fieldId: RECORDS.corpCardExpense.fields.expSource})
			}).setValue({
				fieldId: 'chargedate',
				value: rec.getValue({fieldId: RECORDS.corpCardExpense.fields.expDate})
			}).setValue({
				fieldId: 'employee',
				value: rec.getValue({fieldId: RECORDS.corpCardExpense.fields.expEmployee})
			}).setValue({
				fieldId: 'memo',
				value: rec.getValue({fieldId: RECORDS.corpCardExpense.fields.expMemo})
			}).setValue({
				fieldId: 'billedamount',
				value: rec.getValue({fieldId: RECORDS.corpCardExpense.fields.expAmount})
			});

			var impRecId = impRec.save();

			rec.setValue({
				fieldId: RECORDS.corpCardExpense.fields.importedExpense,
				value: impRecId
			});
		}
	}

	return {
		beforeSubmit: beforeSubmit
	}

});