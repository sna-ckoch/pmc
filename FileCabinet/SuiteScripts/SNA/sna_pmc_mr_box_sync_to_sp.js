/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 * 
 * Script brief description: 
 * Attempts to upload all existing Box File to Service Pro Note records with no SP Note ID
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/Dec/09                           ckoch           Initial version
 *
 */
define(['N/search', './sna_pmc_mod_box_globals.js'], function (search, boxGlobals) {

    function getInputData() {
        return search.create({
            type: boxGlobals.records.boxToSP.id,
            filters: [
                [boxGlobals.records.boxToSP.fields.spNoteId, 'isempty', ''],
                'and',
                ['isinactive', 'is', 'F']
            ],
            columns: ['internalid']
        });
    }

    function map(context) {
        log.debug({ title: 'MAP', details: context });

        boxGlobals.boxFileRecordToSP(JSON.parse(context.value).id);
    }

    function summarize(context) {
        log.debug({ title: 'SUMMARIZE', details: JSON.stringify(context) });
    }

    return {
        getInputData: getInputData,
        map: map,
        summarize: summarize
    }

});