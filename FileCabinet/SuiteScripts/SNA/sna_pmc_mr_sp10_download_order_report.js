/**
 * Copyright (c) 2021, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Downloads pdf reports for completed orders
 * Different process than inspection reports--
 * -- have to make request and get guid, then check status of guid until attachmentid available, then get attachment
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 20201/10/05                          ckoch           initial version
 */
define(['N/runtime', 'N/record', 'N/search', 'N/file', './sna_pmc_mod_sp10_globals.js', './sna_pmc_mod_box_globals.js'],
	function (runtime, record, search, file, globals, boxGlobals) {

		function getInputData(context) {
			var orders = [];

			var cacheKey = '__clock_order_pdf';

			var lastClock = nullIfEmpty(runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_sp10_dlord_lastclock'}));
			if (lastClock == null) {
				lastClock = globals.getCache().get({
					key: cacheKey
				});
			}

			if (lastClock == null) {
				var d = new Date();
				d.setHours(d.getHours() - 24); // default to 24hr lookback if no last run time
				lastClock = d.toISOString();
			}
			log.debug({title: 'getInputData.lastClock', details: lastClock});

			var maxPages = 25;
			var pages = 0;

			try {
				// get all inspection results since last clock
				var url = '/Order/?$select=Id,ExternalId&$filter=CompletedDateTime ge ' + lastClock + ' and CompletedById ne null and ExternalId ne null';

				do {
					log.debug({title: 'getInputData.request', details: url});
					var response = globals.callapi(url, true, 'get');
					log.debug({title: 'getInputData.response', details: response});

					// fix and store the clock from this request to be used next time
					var newClock = response.d['__clock'] || null;

					if (newClock != null && pages === 0) {
						var parts = newClock.split(/\D+/);
						if (parts.length == 6) {
							// yyyy-mm-dd-hh-mm-ss UTC
							var d = new Date(Date.UTC(parts[0], --parts[1], parts[2], parts[3], parts[4], parts[5]));
							d.setMinutes(d.getMinutes() - 10);
							newClock = d.toISOString();

							log.debug({title: 'getInputData.newClock', details: newClock});

							globals.getCache().put({
								key: cacheKey,
								value: newClock
							});
						}
					}

					// pages of 20, gives us url to next page if any
					url = response.d['__next'] || null;

					if (url != null) {
						url = url.replace('/api/', '/'); // callapi already has this bit
						log.debug({title: 'getInputData.next', details: url});
					}

					orders = orders.concat(response.d.results);

					pages++;
				} while (url != null && pages < maxPages);
			} catch (e) {
				log.error({title: 'getInputData', details: e});
			}

			log.debug({title: 'getInputData.orders.length', details: orders.length});

			return orders;
		}

		function map(context) {
			var result = JSON.parse(context.value);

			log.debug({title: 'map.result', details: result});

			try {
				var orderId = result['Id'] || null;
				var externalId = result['ExternalId'] || null;

				if (orderId != null && externalId != null) {
					var guid = globals.callapi('/Order/Order?id=' + orderId + '&formatId=1', true, 'post', {}).d.results;

					log.debug({
						title: 'map',
						details: {
							orderId: orderId,
							externalId: externalId,
							guid: guid
						}
					});

					if (!isEmpty(guid)) {
						context.write({
							key: guid,
							value: {
								orderId: orderId,
								externalId: externalId,
								guid: guid
							}
						});
					}
				}
			} catch (e) {
				log.error({title: 'map.error', details: e});
			}
		}

		function reduce(context) {
			var values = context.values.map(JSON.parse);

			log.debug({
				title: 'reduce.values',
				details: values
			});

			if (util.isArray(values) === false || values.length === 0) return;

			try {
				var orderId = values[0].orderId;
				var guid = values[0].guid;
				var externalId = values[0].externalId;

				if (findExistingReport(orderId)) {
					log.debug({
						title: 'reduce.dupeCheck',
						details: {
							message: 'Found existing report, exiting',
							orderId: orderId,
							guid: guid,
							externalId: externalId
						}
					});
					return;
				}

				var attachmentId = forceFloat(globals.callapi('/Executing/Ping?guid=' + guid, true, 'post', {}).d.results);

				log.debug({
					title: 'reduce.attachmentId',
					details: {
						guid: guid,
						externalId: externalId,
						attachmentId: attachmentId
					}
				});

				if (attachmentId === 0) {
					// add to retry cache
					log.debug({title:'reduce.attachmentId',details:'add to retry queue'});
				} else {
					var attachmentFile = getAttachment(attachmentId);
					log.debug({title:'reduce.attachmentFile',details:attachmentFile});

					var downloadFolder = nullIfEmpty(runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_sp10_dlord_folderid'}));

					if (externalId && attachmentFile) {
						attachmentFile.folder = Number(downloadFolder);

						record.create({
							type: boxGlobals.records.spToBox.id
						}).setValue({
							fieldId: boxGlobals.records.spToBox.fields.emailFrom,
							value: 'reports@downloader.com'
						}).setValue({
							fieldId: boxGlobals.records.spToBox.fields.emailSubject,
							value: 'Order ID ' + orderId + ' Report'
						}).setValue({
							fieldId: boxGlobals.records.spToBox.fields.emailMessage,
							value: 'Order Report'
						}).setValue({
							fieldId: 'custrecord_sna_pmc_box2sp_test',
							value: true
						}).setValue({
							fieldId: 'custrecord_sna_pmc_box2sp_resultid',
							value: 'order-' + orderId
						}).setValue({
							fieldId: boxGlobals.records.spToBox.fields.salesOrder,
							value: externalId
						}).setValue({
							fieldId: boxGlobals.records.spToBox.fields.emailAttachment,
							value: attachmentFile.save()
						}).save();
					}
				}
			} catch (e) {
				log.error({
					title: 'reduce.error',
					details: {
						values: values,
						e: e
					}
				});
			}
		}

		function summarize(context) {
			log.debug({title: 'summarize', details: context});
		}

		function isEmpty(stValue) {
			return ((stValue === '' || stValue == null || stValue == undefined) || (stValue.constructor === Array && stValue.length == 0) || (stValue.constructor === Object && (function (v) {
				for (var k in v)
					return false;
				return true;
			})(stValue)));
		}

		function nullIfEmpty(what) {
			return (isEmpty(what) ? null : what);
		}

		function forceFloat(what) {
			return (isNaN(parseFloat(what)) ? 0 : parseFloat(what));
		}

		function findExistingReport(resultId) {
			var output = false;

			if (!isEmpty(resultId)) {
				search.create({
					type: boxGlobals.records.spToBox.id,
					filters: [
						['custrecord_sna_pmc_box2sp_resultid', 'is', 'order-' + resultId]
					],
					columns: ['internalid']
				}).run().each(function(result){
					output = true;
					return false;
				});
			}

			log.debug({
				title: 'findExistingReport',
				details: {
					resultId: resultId,
					output: output
				}
			});

			return output;
		}

		function getAttachment(attachmentId) {
			var output = null;

			var attachmentFile = null;

			try {
				attachmentFile = globals.callapi('/Attachment/GetAttachment?id=' + attachmentId, false, 'get');

				log.debug({title: 'attachmentFile', details: attachmentFile});
			} catch (e) {
				log.error({
					title: 'DOWNLOAD_ATTACHMENT',
					details: {
						attachmentId: attachmentId,
						e: e
					}
				});
			}

			var contentDisp = nullIfEmpty(attachmentFile.headers['Content-Disposition']);

			if (attachmentFile != null && contentDisp != null) {
				var attFileName = null;

				try {
					attFileName = /filename[^;=\n]*=(?:(\\?['"])(.*?)\1|(?:[^\s]+'.*?')?([^;\n]*))/.exec(contentDisp)[3] || null;
				} catch (e) {
					log.error({
						title: 'FILENAME_PARSE',
						details: {
							contentDisp: contentDisp,
							e: e
						}
					});
				}

				if (isEmpty(attFileName)) {
					try {
						attFileName = /filename="(.[^"]*)"/.exec(contentDisp)[1] || null;
					} catch (e) {
						log.error({
							title: 'FILENAME_PARSE',
							details: {
								contentDisp: contentDisp,
								e: e
							}
						});
					}

				}

				var attFileType = mimeFileType(attachmentFile.headers['Content-Type']);

				var filePrefix = runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_sp10_dlord_prefix'});

				if (!isEmpty(filePrefix)) {
					attFileName = filePrefix + attFileName;
				}

				log.debug({title: 'attFileName', details: attFileName});
				log.debug({title: 'attFileType', details: attFileType});

				if (attFileName != null && attFileType != null) {
					try {
						output = file.create({
							name: attFileName,
							fileType: attFileType,
							contents: attachmentFile.body
						});
					} catch (e) {
						log.error({
							title: 'FILE_CREATE',
							details: {
								attFileName: attFileName,
								attFileType: attFileType,
								e: e
							}
						});
					}
				}
			}

			return output;
		}

		function mimeFileType(contentType) {
			var lookup = {};

			log.debug({title: 'mimeFileType', details: contentType});

			lookup['application/x-autocad'] = file.Type.AUTOCAD;
			lookup['image/x-xbitmap'] = file.Type.BMPIMAGE;
			lookup['text/csv'] = file.Type.CSV;
			lookup['application/vnd.ms-excel'] = file.Type.EXCEL;
			lookup['application/x-shockwave-flash'] = file.Type.FLASH;
			lookup['image/gif'] = file.Type.GIFIMAGE;
			lookup['application/?x-?gzip-?compressed'] = file.Type.GZIP;
			lookup['text/html'] = file.Type.HTMLDOC;
			lookup['image/ico'] = file.Type.ICON;
			lookup['text/javascript'] = file.Type.JAVASCRIPT;
			lookup['image/jpeg'] = file.Type.JPGIMAGE;
			lookup['application/json'] = file.Type.JSON;
			lookup['message/rfc822'] = file.Type.MESSAGERFC;
			lookup['audio/mpeg'] = file.Type.MP3;
			lookup['video/mpeg'] = file.Type.MPEGMOVIE;
			lookup['application/vnd.ms-project'] = file.Type.MSPROJECT;
			lookup['application/pdf'] = file.Type.PDF;
			lookup['image/pjpeg'] = file.Type.PJPGIMAGE;
			lookup['text/plain'] = file.Type.PLAINTEXT;
			lookup['image/x-png'] = file.Type.PNGIMAGE;
			lookup['image/png'] = file.Type.PNGIMAGE;
			lookup['application/postscript'] = file.Type.POSTSCRIPT;
			lookup['application/?vnd.?ms-?powerpoint'] = file.Type.POWERPOINT;
			lookup['video/quicktime'] = file.Type.QUICKTIME;
			lookup['application/rtf'] = file.Type.RTF;
			lookup['application/sms'] = file.Type.SMS;
			lookup['text/css'] = file.Type.STYLESHEET;
			lookup['image/tiff'] = file.Type.TIFFIMAGE;
			lookup['application/vnd.visio'] = file.Type.VISIO;
			lookup['application/msword'] = file.Type.WORD;
			lookup['text/xml'] = file.Type.XMLDOC;
			lookup['application/zip'] = file.Type.ZIP;

			if (lookup.hasOwnProperty(contentType)) {
				return lookup[contentType];
			} else {
				return null;
			}
		}

		return {
			getInputData: getInputData,
			map: map,
			reduce: reduce,
			summarize: summarize
		}

	});