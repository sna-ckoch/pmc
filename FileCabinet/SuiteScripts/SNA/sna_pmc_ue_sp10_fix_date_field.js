/**
 * Copyright (c) 2021, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Takes "Date" line level text field populated by ServicePro and transfers it to a custom Date type field
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2021/05/27                           ckoch           Initial version
 *
 */
define(['N/format', '/SuiteScripts/SNA/sna_pmc_mod_sp10_globals.js'], function (format, globals) {

	function beforeSubmit(context) {
		var router = {};
		router[context.UserEventType.CREATE] = updateLineDates;
		router[context.UserEventType.EDIT] = updateLineDates;

		if (typeof router[context.type] !== 'function') {
			return;
		}

		router[context.type](context);
	}

	function updateLineDates(context) {
		var rec = context.newRecord;

		var lineCount = rec.getLineCount({sublistId: 'item'}) || 0;

		for (var i = 0; i < lineCount; i++) {
			var preApproved = rec.getSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_pmc_expense_preapproved',
				line: i
			}) || null;

			var techEntered = rec.getSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_sna_sp10_tech_entered',
				line: i
			}) || null;

			var completedBy = rec.getSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_sna_sp10_completed_by',
				line: i
			}) || null;

			if (preApproved == true && forceFloat(techEntered) > 0 && forceFloat(completedBy) == 0) {
				rec.setSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_sna_sp10_completed_by',
					value: techEntered,
					line: i
				});
			}

			var textDate = rec.getSublistValue({
				sublistId: 'item',
				fieldId: 'custcolsp_techdate',
				line: i
			}) || null;

			var override = rec.getSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_sna_pmc_sp10_date_override',
				line: i
			}) || false;

			if (textDate != null && override == false) {
				var dd = null;
				try {
					dd = new Date(textDate);
				} catch (e) {
					log.error('bad date');
				}

				if (util.isDate(dd)) {
					var curDate = rec.getSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_sna_pmc_sp10_date',
						line: i
					}) || null;

					if (dd != curDate) {
						rec.setSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_sna_pmc_sp10_date',
							value: dd,
							line: i
						});
					}
				} else {
					rec.setSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_sna_pmc_sp10_date',
						value: null,
						line: i
					});
				}
			}
		}
	}

	function forceFloat(what) {
		return (isNaN(parseFloat(what)) ? 0 : parseFloat(what));
	}

	return {
		beforeSubmit: beforeSubmit
	}

});