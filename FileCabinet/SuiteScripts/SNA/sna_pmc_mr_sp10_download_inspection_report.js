/**
 * Copyright (c) 2021, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Downloads pdf reports for completed inspections
 * Creates ServicePro Email to Box record for each so pdf is sent to box
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2021/08/26                           ckoch           Initial version
 *
 */
define(['N/runtime', 'N/record', 'N/search', 'N/file', './sna_pmc_mod_sp10_globals.js', './sna_pmc_mod_box_globals.js'],
	function (runtime, record, search, file, globals, boxGlobals) {

		function getInputData(context) {
			var inspections = [];

			var inspectionLinkId = nullIfEmpty(runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_mr_sp10_dip_insp_link'}));
			if (inspectionLinkId == null) {
				log.error({title: 'Inspection Link ID parameter is blank. Exiting'});
				return;
			}

			var cacheKey = '__clock_pdf_' + inspectionLinkId;

			var lastClock = nullIfEmpty(runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_mr_sp10_dip_lastclock'}));
			if (lastClock == null) {
				lastClock = globals.getCache().get({
					key: cacheKey
				});
			}

			if (lastClock == null) {
				var d = new Date();
				d.setHours(d.getHours() - 24); // default to 24hr lookback if no last run time
				lastClock = d.toISOString();
			}
			log.debug({title: 'LASTCLOCK_FINAL', details: lastClock});

			var maxPages = 25;
			var pages = 0;

			try {
				// get all inspection results since last clock
				var url = '/InspectionResult?$filter=(Inspection/InspectionLinkId eq ' + inspectionLinkId;
				url += ' and ((CompletedDateTime ge ' + lastClock + ') or ';
				url += ' (CreatedDateTime ge ' + lastClock + ' and CompletedDateTime ne null) or ';
				url += ' (LastUpdatedDateTime ge ' + lastClock + ' and CompletedDateTime ne null)))';
				url += '&$select=AttachmentId,OrderLine/OrderSegment/ExternalId';
				url += '&$expand=OrderLine/OrderSegment';

				do {
					log.debug({title: 'GET_INSPECTIONS_REQ', details: url});
					var response = globals.callapi(url, true, 'get');
					log.debug({title: 'GET_INSPECTIONS_RES', details: JSON.stringify(response)});

					// fix and store the clock from this request to be used next time
					var newClock = response.d['__clock'] || null;

					if (newClock != null) {
						var parts = newClock.split(/\D+/);
						if (parts.length == 6) {
							// yyyy-mm-dd-hh-mm-ss UTC
							var d = new Date(Date.UTC(parts[0], --parts[1], parts[2], parts[3], parts[4], parts[5]));
							d.setMinutes(d.getMinutes() - 10);
							newClock = d.toISOString();

							log.debug({title: 'NEWCLOCK_FINAL', details: newClock});

							globals.getCache().put({
								key: cacheKey,
								value: newClock
							});
						}
					}

					// pages of 20, gives us url to next page if any
					url = response.d['__next'] || null;

					if (url != null) {
						url = url.replace('/api/', '/'); // callapi already has this bit
						log.debug({title: 'NEXT_FINAL', details: url});
					}

					inspections = inspections.concat(response.d.results);

					pages++;
				} while (url != null && pages < maxPages);
			} catch (e) {
				log.error({title: 'GET_ERROR', details: JSON.stringify(e)});
			}

			log.debug({title: 'INSPECTIONS_LEN', details: inspections.length});

			return inspections;
		}

		function map(context) {
			var result = JSON.parse(context.value);

			log.debug({title: 'MAP.RESULT', details: result});

			var reportFile = null;
			var salesOrderId = null;

			try {
				var attachmentId = result['AttachmentId'] || null;
				if (attachmentId) {
					reportFile = getAttachment(attachmentId);
				}
			} catch (e) {
				log.debug({
					title: 'map.reportFile',
					details: {
						result: result,
						e: e
					}
				});
			}
			try {
				salesOrderId = result['OrderLine'][0]['OrderSegment']['ExternalId'] || null;
			} catch (e) {
				log.debug({
					title: 'map.externalId',
					details: {
						result: result,
						e: e
					}
				});
			}

			var existingRecord = findExistingReport(result['Id']);

			if (!existingRecord && (reportFile || salesOrderId)) {
				var emailRec = record.create({
					type: boxGlobals.records.spToBox.id
				}).setValue({
					fieldId: boxGlobals.records.spToBox.fields.emailFrom,
					value: 'reports@downloader.com'
				}).setValue({
					fieldId: boxGlobals.records.spToBox.fields.emailSubject,
					value: 'Inspection ID ' + result['Id'] + ' Report'
				}).setValue({
					fieldId: boxGlobals.records.spToBox.fields.emailMessage,
					value: 'Inspection Report'
				}).setValue({
					fieldId: 'custrecord_sna_pmc_box2sp_test',
					value: true
				}).setValue({
					fieldId: 'custrecord_sna_pmc_box2sp_resultid',
					value: result['Id']
				});

				var downloadFolder = nullIfEmpty(runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_mr_sp10_dip_folderid'}));

				if (reportFile && downloadFolder) {
					var reportFileId = null;

					try {
						reportFile.folder = Number(downloadFolder);
						reportFileId = reportFile.save();
					} catch (e) {
						log.error({
							title: 'map.saveReport',
							details: {
								reportFile: reportFile,
								e: e
							}
						});
					}

					if (reportFileId) {
						emailRec.setValue({
							fieldId: boxGlobals.records.spToBox.fields.emailAttachment,
							value: reportFileId
						});
					}
				}

				if (salesOrderId) {
					emailRec.setValue({
						fieldId: boxGlobals.records.spToBox.fields.salesOrder,
						value: salesOrderId
					});
				}

				try {
					emailRec.save();
				} catch (e) {
					log.error({
						title: 'map.recSave',
						details: {
							e: e,
							emailRec: emailRec
						}
					});
				}
			}
		}

		function findExistingReport(resultId) {
			var output = false;

			if (!isEmpty(resultId)) {
				search.create({
					type: boxGlobals.records.spToBox.id,
					filters: [
						['custrecord_sna_pmc_box2sp_resultid', 'is', resultId]
					],
					columns: ['internalid']
				}).run().each(function (result) {
					output = true;
					return false;
				});
			}

			log.debug({
				title: 'findExistingReport',
				details: {
					resultId: resultId,
					output: output
				}
			});

			return output;
		}

		function getAttachment(attachmentId) {
			var output = null;

			var attachmentFile = null;

			try {
				attachmentFile = globals.callapi('/Attachment/GetAttachment?id=' + attachmentId, false, 'get');

				log.debug({title: 'attachmentFile', details: attachmentFile});
			} catch (e) {
				log.error({
					title: 'DOWNLOAD_ATTACHMENT',
					details: {
						attachmentId: attachmentId,
						e: e
					}
				});
			}

			var contentDisp = nullIfEmpty(attachmentFile.headers['Content-Disposition']);

			if (attachmentFile != null && contentDisp != null) {
				var attFileName = null;
				try {
					attFileName = /filename[^;\n]*=(UTF-\d['"]*)?((['"]).*?[.]$\2|[^;\n]*)?/.exec(contentDisp)[2] || null;
					//attFileName = /filename[^;=\n]*=(?:(\\?['"])(.*?)\1|(?:[^\s]+'.*?')?([^;\n]*))/.exec(contentDisp)[2] || null;
				} catch (e) {
					log.error({
						title: 'FILENAME_PARSE',
						details: {
							contentDisp: contentDisp,
							e: e
						}
					});
				}

				var attFileType = mimeFileType(attachmentFile.headers['Content-Type']);

				var filePrefix = runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_mr_sp10_dip_prefix'});

				if (!isEmpty(filePrefix)) {
					attFileName = filePrefix + attFileName;
				}

				log.debug({title: 'attFileName', details: attFileName});
				log.debug({title: 'attFileType', details: attFileType});

				if (attFileName != null && attFileType != null) {
					try {
						output = file.create({
							name: attFileName,
							fileType: attFileType,
							contents: attachmentFile.body
						});
					} catch (e) {
						log.error({
							title: 'FILE_CREATE',
							details: {
								attFileName: attFileName,
								attFileType: attFileType,
								e: e
							}
						});
					}
				}
			}

			return output;
		}

		function summarize(context) {
			log.debug({title: 'SUMMARIZE', details: JSON.stringify(context)});
		}

		function isEmpty(stValue) {
			return ((stValue === '' || stValue == null || stValue == undefined) || (stValue.constructor === Array && stValue.length == 0) || (stValue.constructor === Object && (function (v) {
				for (var k in v)
					return false;
				return true;
			})(stValue)));
		}

		function nullIfEmpty(what) {
			return (isEmpty(what) ? null : what);
		}

		function mimeFileType(contentType) {
			var lookup = {};

			log.debug({title: 'mimeFileType', details: contentType});

			lookup['application/x-autocad'] = file.Type.AUTOCAD;
			lookup['image/x-xbitmap'] = file.Type.BMPIMAGE;
			lookup['text/csv'] = file.Type.CSV;
			lookup['application/vnd.ms-excel'] = file.Type.EXCEL;
			lookup['application/x-shockwave-flash'] = file.Type.FLASH;
			lookup['image/gif'] = file.Type.GIFIMAGE;
			lookup['application/?x-?gzip-?compressed'] = file.Type.GZIP;
			lookup['text/html'] = file.Type.HTMLDOC;
			lookup['image/ico'] = file.Type.ICON;
			lookup['text/javascript'] = file.Type.JAVASCRIPT;
			lookup['image/jpeg'] = file.Type.JPGIMAGE;
			lookup['application/json'] = file.Type.JSON;
			lookup['message/rfc822'] = file.Type.MESSAGERFC;
			lookup['audio/mpeg'] = file.Type.MP3;
			lookup['video/mpeg'] = file.Type.MPEGMOVIE;
			lookup['application/vnd.ms-project'] = file.Type.MSPROJECT;
			lookup['application/pdf'] = file.Type.PDF;
			lookup['image/pjpeg'] = file.Type.PJPGIMAGE;
			lookup['text/plain'] = file.Type.PLAINTEXT;
			lookup['image/x-png'] = file.Type.PNGIMAGE;
			lookup['image/png'] = file.Type.PNGIMAGE;
			lookup['application/postscript'] = file.Type.POSTSCRIPT;
			lookup['application/?vnd.?ms-?powerpoint'] = file.Type.POWERPOINT;
			lookup['video/quicktime'] = file.Type.QUICKTIME;
			lookup['application/rtf'] = file.Type.RTF;
			lookup['application/sms'] = file.Type.SMS;
			lookup['text/css'] = file.Type.STYLESHEET;
			lookup['image/tiff'] = file.Type.TIFFIMAGE;
			lookup['application/vnd.visio'] = file.Type.VISIO;
			lookup['application/msword'] = file.Type.WORD;
			lookup['text/xml'] = file.Type.XMLDOC;
			lookup['application/zip'] = file.Type.ZIP;

			if (lookup.hasOwnProperty(contentType)) {
				return lookup[contentType];
			} else {
				return null;
			}
		}

		return {
			getInputData: getInputData,
			map: map,
			summarize: summarize
		}

	});