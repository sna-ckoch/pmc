/**
 * Copyright (c) 2021, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType WorkflowActionScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Simply loads the record, checks "send to servicepro" box and save it
 * Done this way so that it triggers integration UE script
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2021/09/09                           ckoch           Initial version
 *
 */
define(['N/record'], function (record) {

	function onAction(context) {
		try {
			var recType = context.newRecord.type;
			var recId = context.newRecord.id;

			record.load({
				type: recType,
				id: recId
			}).setValue({
				fieldId: 'custbodysendtoservicepro',
				value: true
			}).setValue({
				fieldId: 'custbody_pmc_current_weight',
				value: '51'
			}).setValue({
				fieldId: 'custbody_pmc_previous_weight',
				value: '50'
			}).setValue({
				fieldId: 'custbody_pmc_status_weight',
				value: '8'
			}).setValue({
				fieldId: 'custbody_esp_roll_up_spro_time',
				value: true
			}).save({
				ignoreMandatoryFields: true
			});
		} catch (e) {
			log.error({
				title: 'onAction.save',
				details: {
					recType: recType,
					recId: recId,
					e: e
				}
			});
		}
	}

	return {
		onAction: onAction
	}

});