/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType WorkflowActionScript
 * @NModuleScope SameAccount
 * 
 * Script brief description: 
 * Calls suitelet to create 'box file to service pro note' records
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/12/09                           ckoch           Initial version
 *
 */
define(['N/redirect'], function (redirect) {

    function onAction(context) {
        redirect.toSuitelet({
            scriptId: 'customscript_sna_pmc_sl_box_files',
            deploymentId: 'customdeploy_sna_pmc_sl_box_files',
            parameters: {
                'custpage_rectype': context.newRecord.type,
                'custpage_recid': context.newRecord.id
            }
        });
    }

    return {
        onAction: onAction
    }

});