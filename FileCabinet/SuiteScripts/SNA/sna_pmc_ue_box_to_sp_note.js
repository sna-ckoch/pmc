/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 * Script brief description: 
   Sends an API call to Service Pro to create an order note for the record
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/12/07                           ckoch           Initial version
 *
 */
define(['./sna_pmc_mod_box_globals.js'], function (boxGlobals) {

    function afterSubmit(context) {
        var events = {};
        events[context.UserEventType.CREATE] = handleCreate;

        if (typeof events[context.type] === 'function') {
            events[context.type](context);
        }
    }

    function handleCreate(context) {
        boxGlobals.boxFileRecordToSP(context.newRecord.id);
    }

    return {
        afterSubmit: afterSubmit
    }

});