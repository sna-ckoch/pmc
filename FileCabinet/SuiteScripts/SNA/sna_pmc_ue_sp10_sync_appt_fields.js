/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 * Script brief description: 
   Updates list/record fields on Appointment record
   Uses ID fields on Appointment record to lookup existing list values by Id, or add new
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/12/02                           ckoch           Deprecated - added to upsert function in module
 * 2020/11/30                           ckoch           Initial version
 *
 */
define(['N/search', 'N/record', './sna_pmc_mod_sp10_globals.js'], function (search, record, globals) {

    function afterSubmit(context) {
        var statusId = context.newRecord.getValue({ fieldId: globals.records.appointment.fields.nsAppointmentStatusId }) || null;
        var statusName = context.newRecord.getValue({ fieldId: globals.records.appointment.fields.nsAppointmentStatusName }) || null;

        var techId = context.newRecord.getValue({ fieldId: globals.records.appointment.fields.nsAssignedTechId }) || null;
        var techName = context.newRecord.getValue({ fieldId: globals.records.appointment.fields.nsAssignedTechName }) || null;

        var values = {};
        values[globals.records.appointment.fields.nsApptStatus] = null;
        values[globals.records.appointment.fields.nsAssignedTech] = null;

        if (statusId != null && statusName != null) {
            values[globals.records.appointment.fields.nsApptStatus] = getOrCreateListValue(statusId,
                statusName, globals.records.appointmentStatus.id, globals.records.appointmentStatus.fields.Id);
        }

        if (techId != null && techName != null) {
            values[globals.records.appointment.fields.nsAssignedTech] = getOrCreateListValue(techId,
                techName, globals.records.serviceTech.id, globals.records.serviceTech.fields.Id);
        }

        record.submitFields({
            type: context.newRecord.type,
            id: context.newRecord.id,
            values: values
        });
    }

    function getOrCreateListValue(lookupId, lookupName, recordType, recordIdField) {
        var output = null;

        try {
            search.create({
                type: recordType,
                filters: [
                    [recordIdField, 'is', lookupId],
                    'and',
                    ['isinactive', 'is', 'F']
                ],
                columns: ['internalid']
            }).run().each(function (result) {
                output = result.id;
                return false; // only want first one
            });
        } catch (e) {
            var info = {
                lookupId: lookupId,
                recordType: recordType,
                recordIdField: recordIdField,
                e: JSON.stringify(e)
            };
            log.debug({ title: 'LIST_LOOKUP', details: JSON.stringify(info) });
        }

        if (output == null) {
            try {
                output = record.create({
                    type: recordType
                }).setValue({
                    fieldId: 'name',
                    value: lookupName
                }).setValue({
                    fieldId: recordIdField,
                    value: lookupId
                }).save();
            } catch (e) {
                var info = {
                    lookupId: lookupId,
                    recordType: recordType,
                    recordIdField: recordIdField,
                    e: JSON.stringify(e)
                };
                log.error({ title: 'LIST_CREATE', details: JSON.stringify(info) });
            }
        }

        return output;
    }

    return {
        afterSubmit: afterSubmit
    }

});