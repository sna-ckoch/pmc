/**
 * Copyright (c) 2021, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Searches for ServicePro Email To Box records where
 * - Sales Order != null
 * - Box Upload Email = null
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 07/21/21                             ckoch           Initial version
 *
 */
define(['N/record', 'N/search'], function (record, search) {

	function getInputData(context) {
		return search.create({
			type: 'customrecord_sna_pmc_sp10_email_to_box',
			filters: [
				['custrecord_sna_pmc_box2sp_sales_order', 'isnotempty', ''],
				'and',
				['custrecord_sna_pmc_box2sp_box_email', 'isempty', ''],
				'and',
				['isinactive', 'is', 'F']
			],
			columns: ['internalid']
		});
	}

	function map(context) {
		// a UE on the record handles pairing so we just need to load/save it to trigger

		record.load({
			type: 'customrecord_sna_pmc_sp10_email_to_box',
			id: context.key
		}).save();
	}

	function summarize(context) {
		log.debug({title: 'SUMMARIZE', details: JSON.stringify(context)});
	}

	return {
		getInputData: getInputData,
		map: map,
		summarize: summarize
	}

});