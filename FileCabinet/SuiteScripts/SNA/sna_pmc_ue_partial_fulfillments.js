/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Adds Job Completion inspection lines to sales orders or child orders
 * Prints packing slip and sends to ServicePro as order note
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/01/26                           ckoch           Initial version
 * 2020/03/02                           ckoch           Updates to support new parent/child design
 *
 */
define(['N/record', 'N/search', 'N/runtime', 'N/url', 'N/https', 'N/render', './sna_pmc_mod_sp10_globals.js'],
	function (record, search, runtime, url, https, render, spGlobals) {

		function afterSubmit(context) {
			var events = {};
			events[context.UserEventType.CREATE] = handleCreate;
			events[context.UserEventType.EDIT] = handleEdit;
			events[context.UserEventType.DELETE] = handleDelete;

			if (typeof events[context.type] === 'function') {
				events[context.type](context);
			}
		}

		function handleCreate(context) {
			var rec = context.newRecord;

			if (forceFloat(rec.id) <= spGlobals.MIN_INTERNAL_ID) {
				log.debug({
					title: 'handleCreate.MIN_INTERNAL_ID',
					details: 'Not running, existing transaction - id ' + rec.id + ' <= min ' + spGlobals.MIN_INTERNAL_ID
				});

				return;
			}

			if (checkItems(rec.id) == false) {
				log.debug({title: 'handleCreate.checkItems', details: 'Not running - no items require job completion'});

				return;
			}

			var info = getFulfillmentInfo(rec.id);
			var salesOrder = (info.childOrder ? info.childOrder : info.createdFrom);

			if (salesOrder == null) {
				info.salesOrder = salesOrder;
				log.debug({title: 'handleCreate.salesOrderNull', details: info});

				return;
			}

			if (info.jobCompletion != null) {
				info.message = 'Record already associated with Job Completion, exiting';
				log.debug({title: 'handleCreate.jobCompletion', details: info});

				return;
			}

			addInspectionLine(salesOrder, rec.id, info.tranId);

			if (info.noteId != null) {
				log.debug({title: 'handleCreate.noteId', details: 'Note already created, exiting'});

				return;
			}

			var sendToSP = lookupField(search.Type.SALES_ORDER, salesOrder, 'custbodysendtoservicepro');

			if (sendToSP) {
				var noteId = createFulfillmentNote(salesOrder, rec.id, info.tranId);

				if (noteId != null) {
					try {
						record.submitFields({
							type: rec.type,
							id: rec.id,
							values: {
								'custbody_sna_pmc_sp10_note_id': noteId
							}
						});
					} catch (e) {
						log.error({
							title: 'handleCreate.updateNoteId',
							details: {
								recType: rec.type,
								recId: rec.id,
								noteId: noteId,
								e: e
							}
						});
					}
				}
			}
		}

		function handleEdit(context) {
			var oldRec = context.oldRecord;
			var newRec = context.newRecord;

			if (forceFloat(newRec.id) <= spGlobals.MIN_INTERNAL_ID) {
				log.debug({
					title: 'handleEdit.MIN_INTERNAL_ID',
					details: 'Not running, existing transaction - id ' + newRec.id + ' <= min ' + spGlobals.MIN_INTERNAL_ID
				});

				return;
			}

			var oldNoteId = nullIfEmpty(oldRec.getValue({fieldId: 'custbody_sna_pmc_sp10_note_id'}));
			var newNoteId = nullIfEmpty(newRec.getValue({fieldId: 'custbody_sna_pmc_sp10_note_id'}));

			log.debug({
				title: 'handleEdit.noteId',
				details: {
					oldNoteId: oldNoteId,
					newNoteId: newNoteId
				}
			});

			if (oldNoteId != null && oldNoteId != '' && newNoteId == null) {
				log.debug({title: 'handleEdit.noteCheck', details: 'Deleting note id ' + oldNoteId});

				var delResponse = spGlobals.callapi('/OrderNote?id=' + oldNoteId, false, 'delete');

				log.debug({title: 'handleEdit.apiResponse', details: delResponse});
			}

			log.debug({title: 'handleEdit', details: 'Calling handleCreate'});

			handleCreate(context);
		}

		function handleDelete(context) {
			log.debug({title: 'handleDelete', details: context.oldRecord.id});

			var rec = context.oldRecord;

			if (forceFloat(rec.id) <= spGlobals.MIN_INTERNAL_ID) {
				log.debug({
					title: 'handleDelete.MIN_INTERNAL_ID',
					details: 'Not running, existing transaction - id ' + rec.id + ' <= min ' + spGlobals.MIN_INTERNAL_ID
				});

				return;
			}

			var noteId = rec.getValue({fieldId: 'custbody_sna_pmc_sp10_note_id'});

			log.debug({title: 'handleDelete.noteId', details: noteId});

			if (noteId != '' && noteId != null) {
				var delResponse = spGlobals.callapi('/OrderNote?id=' + noteId, false, 'delete');

				log.debug({title: 'handleDelete.apiResponse', details: delResponse});
			}

			var createdFrom = rec.getValue({fieldId: 'createdfrom'});
			var childOrder = rec.getValue({fieldId: 'custbody_sna_pmc_child_salesorder'});
			var salesOrder = ((childOrder != null && childOrder != '') ? childOrder : createdFrom);

			// updates line with new list of IF #'s, or blank if none
			addInspectionLine(salesOrder, null, rec.getValue({fieldId: 'tranid'}));
		}

		function checkItems(ifId) {
			var output = false;

			search.create({
				type: search.Type.ITEM_FULFILLMENT,
				filters: [
					['internalid', 'anyof', ifId],
					'and',
					['item.custitem_sna_pmc_no_job_completion', 'is', 'F']
				],
				columns: ['internalid']
			}).run().each(function (result) {
				output = true;
				return false;
			});

			return output;
		}

		function createFulfillmentNote(orderId, ifId, tranId) {
			var output = null;

			var spOrderId = spGlobals.getSpOrderIdAPI(orderId);
			var deliveryForm = nullIfEmpty(runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_partial_delivery_form'}));
			var noteTypeId = nullIfEmpty(runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_note_type_id_if'}));

			log.debug({title: 'createFulfillmentNote.spOrderId', details: spOrderId});
			log.debug({title: 'createFulfillmentNote.deliveryForm', details: deliveryForm});

			if (spOrderId != null && deliveryForm != null) {
				var pdf = render.packingSlip({
					entityId: Number(ifId),
					printMode: render.PrintMode.PDF,
					formId: Number(deliveryForm)
				});

				var info = getFulfillmentInfo(ifId);
				if (info.noteId != null) {
					log.debug({
						title: 'createFulfillmentNote.lastSecondCheck',
						details: 'Order note already created for fulfillment, exiting'
					});
					return;
				}

				try {
					output = spGlobals.uploadFileNote(String(spOrderId), tranId + '.pdf', tranId, 'application/pdf', pdf.getContents());

					log.debug({title: 'createFulfillmentNote.noteId.apiResponse', details: output});
				} catch (e) {
					log.error({
						title: 'createFulfillmentNote.noteId.apiResponse',
						details: {
							orderId: orderId,
							spOrderId: spOrderId,
							e: e
						}
					});
				}

				if (!isEmpty(output) && !isEmpty(noteTypeId)) {
					try {
						var patchLoad = {
							"Id": output,
							"NoteTypeId": noteTypeId
						};
						spGlobals.callapi('/OrderNote', false, 'patch', patchLoad);
					} catch (e) {
						log.error({
							title: 'NOTE_UPDATE_NOTETYPE',
							details: {
								e: e
							}
						});
					}
				}
			}

			return nullIfEmpty(output);
		}

		function getFulfillmentInfo(ifId) {
			var output = {
				createdFrom: null,
				childOrder: null,
				noteId: null,
				status: null,
				tranId: null
			};

			try {
				var lookups = search.lookupFields({
					type: search.Type.ITEM_FULFILLMENT,
					id: ifId,
					columns: ['createdfrom', 'custbody_sna_pmc_child_salesorder', 'custbody_sna_pmc_sp10_note_id', 'status', 'tranid', 'custbody_sna_pmc_job_completion']
				});

				output.createdFrom = getSelectValue(lookups['createdfrom']);
				output.childOrder = getSelectValue(lookups['custbody_sna_pmc_child_salesorder']);
				output.noteId = nullIfEmpty(lookups['custbody_sna_pmc_sp10_note_id']);
				output.status = getSelectValue(lookups['status']);
				output.tranId = nullIfEmpty(lookups['tranid']);
				output.jobCompletion = getSelectValue(lookups['custbody_sna_pmc_job_completion']);
			} catch (e) {
				log.error({
					title: 'getFulfillmentInfo',
					details: {
						ifId: ifId,
						e: e
					}
				});
			}

			return output;
		}

		function getSelectValue(val) {
			var output = null;

			if (util.isArray(val)) {
				if (val.length == 1) {
					output = val[0].value;
				}
			}

			return nullIfEmpty(output);
		}

		function addInspectionLine(orderId, fulfillId, description) {
			var partialItem = nullIfEmpty(runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_partial_delivery_item'}));

			log.debug({title: 'addInspectionLine.partialItem', details: partialItem});

			if (partialItem) {
				try {
					var suitelet = url.resolveScript({
						scriptId: 'customscript_sna_pmc_sl_partial_delivery',
						deploymentId: 'customdeploy_sna_pmc_sl_partial_delivery',
						params: {
							'custpage_soid': orderId,
							'custpage_ifid': fulfillId,
							'custpage_tranid': description,
							'custpage_itemid': partialItem,
							'custpage_action': 'update'
						},
						returnExternalUrl: true
					});

					// using suitelet so ue's trigger
					https.get.promise({url: suitelet});
				} catch (e) {
					log.error({
						title: 'addInspectionLine',
						details: {
							orderId: orderId,
							fulfillId: fulfillId,
							description: description,
							e: e
						}
					});
				}
			}
		}

		function lookupField(recType, recId, fieldId) {
			var output = null;

			try {
				output = search.lookupFields({
					type: recType,
					id: recId,
					columns: [fieldId]
				})[fieldId] || null;

				if (util.isArray(output)) {
					if (output.length > 0) {
						output = output[0].value;
					} else {
						output = null;
					}
				}
			} catch (e) {
				log.error({
					title: 'lookupField',
					details: {
						recType: recType,
						recId: recId,
						fieldId: fieldId,
						e: e
					}
				});
			}

			log.debug({
				title: 'lookupField',
				details: {
					recType: recType,
					recId: recId,
					fieldId: fieldId,
					output: output
				}
			});

			return output;
		}

		function forceFloat(what) {
			return (isNaN(parseFloat(what)) ? 0 : parseFloat(what));
		}

		function isEmpty(stValue) {
			return ((stValue === '' || stValue == null || stValue == undefined) || (stValue.constructor === Array && stValue.length == 0) || (stValue.constructor === Object && (function (v) {
				for (var k in v)
					return false;
				return true;
			})(stValue)));
		}

		function nullIfEmpty(what) {
			return (isEmpty(what) ? null : what);
		}

		return {
			afterSubmit: afterSubmit
		}

	});