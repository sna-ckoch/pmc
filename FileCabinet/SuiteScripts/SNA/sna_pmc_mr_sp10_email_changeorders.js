/**
 * Copyright (c) 2022, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.0
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Downloads pdf reports for completed inspections (specialized for change orders)
 * Emails pdf to sales order (1) sales rep (2) client advocate (3) project manager
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2022/02/08                           ckoch           Initial version
 *
 */
define(['N/runtime', 'N/file', 'N/render', 'N/email', 'N/search', './sna_pmc_mod_sp10_globals.js'],
	function (runtime, file, render, email, search, globals) {

		function getInputData(context) {
			var inspections = [];

			var inspectionLinkId = nullIfEmpty(runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_sp10_emailco_linkid'}));
			if (inspectionLinkId == null) {
				log.error({title: 'Inspection Link ID parameter is blank. Exiting'});
				return;
			}

			var cacheKey = '__clock_co_' + inspectionLinkId;

			var lastClock = globals.getCache().get({
				key: cacheKey
			});

			if (lastClock === null) {
				var lookbackHours = nullIfEmpty(runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_sp10_emailco_lookback'}));
				if (lookbackHours === null) {
					lookbackHours = 24;
				}

				var d = new Date();
				d.setHours(d.getHours() - lookbackHours);
				lastClock = d.toISOString();
			}


			log.debug({title: 'lastClock', details: lastClock});

			var maxPages = 25;
			var pages = 0;

			try {
				// get all inspection results since last clock
				var url = '/InspectionResult?$filter=(Inspection/InspectionLinkId eq ' + inspectionLinkId;
				url += ' and ((CompletedDateTime ge ' + lastClock + ') or ';
				url += ' (CreatedDateTime ge ' + lastClock + ' and CompletedDateTime ne null) or ';
				url += ' (LastUpdatedDateTime ge ' + lastClock + ' and CompletedDateTime ne null)))';
				url += '&$select=AttachmentId,CompletedTech,CompletedTech/Id,CompletedTech/EmailAddress,';
				url += 'CompletedTech/ServiceTechName,OrderLine/OrderSegment/ExternalId';
				url += '&$expand=CompletedTech,OrderLine/OrderSegment';

				do {
					log.debug({title: 'GET_INSPECTIONS_REQ', details: url});
					var response = globals.callapi(url, true, 'get');
					log.debug({title: 'GET_INSPECTIONS_RES', details: JSON.stringify(response)});

					var newClock = response.d['__clock'] || null;
					if (newClock != null && pages === 0) {
						var parts = newClock.split(/\D+/);
						if (parts.length === 6) {
							// yyyy-mm-dd-hh-mm-ss UTC
							var d = new Date(Date.UTC(parts[0], --parts[1], parts[2], parts[3], parts[4], parts[5]));
							d.setMinutes(d.getMinutes() - 10);
							newClock = d.toISOString();

							log.debug({title: 'NEWCLOCK_FINAL', details: newClock});

							globals.getCache().put({
								key: cacheKey,
								value: newClock
							});
						}
					}

					// pages of 20, gives us url to next page if any
					url = response.d['__next'] || null;

					if (url != null) {
						url = url.replace('/api/', '/'); // callapi already has this bit
						log.debug({title: 'NEXT_FINAL', details: url});
					}

					inspections = inspections.concat(response.d.results);

					pages++;
				} while (url != null && pages < maxPages);
			} catch (e) {
				log.error({title: 'GET_ERROR', details: JSON.stringify(e)});
			}

			log.debug({title: 'INSPECTIONS_LEN', details: inspections.length});

			return inspections;
		}

		function map(context) {
			var result = JSON.parse(context.value);

			log.debug({title: 'MAP.RESULT', details: result});

			var reportFile = null;
			var reportFileId = null;
			var salesOrderId = null;
			var salesOrderInfo = {tranid: null, recips: []};

			try {
				salesOrderId = result['OrderLine'][0]['OrderSegment']['ExternalId'] || null;
				if (salesOrderId != null) {
					salesOrderInfo = getOrderInfo(salesOrderId);
				}

				var attachmentId = result['AttachmentId'] || null;
				if (attachmentId) {
					reportFile = getAttachment(attachmentId);
				}
			} catch (e) {
				log.error({
					title: 'map.resultInfo',
					details: {
						result: result,
						e: e,
						salesOrderId: salesOrderId,
						salesOrderInfo: salesOrderInfo
					}
				});
			}

			if (reportFile && salesOrderId && salesOrderInfo.recips.length > 0) {
				var downloadFolder = nullIfEmpty(runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_sp10_emailco_tempfold'}));

				if (reportFile && downloadFolder) {
					try {
						reportFile.folder = Number(downloadFolder);
						reportFile.isOnline = true;
						reportFileId = reportFile.save();
					} catch (e) {
						log.error({
							title: 'map.saveReport',
							details: {
								reportFile: reportFile,
								e: e
							}
						});
					}

					if (reportFileId) {
						var emailTemplate = nullIfEmpty(runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_sp10_emailco_template'}));
						var emailAuthor = nullIfEmpty(runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_sp10_emailco_authorid'}));

						var emailMerge = render.mergeEmail({
							templateId: Number(emailTemplate),
							transactionId: Number(salesOrderId)
						});

						email.send({
							author: Number(emailAuthor),
							recipients: salesOrderInfo.recips,
							subject: emailMerge.subject,
							body: emailMerge.body,
							attachments: [file.load({id: reportFileId})],
							relatedRecords: {
								transactionId: Number(salesOrderId)
							}
						});

						file.delete({
							id: reportFileId
						});
					}
				}
			}
		}

		function getOrderInfo(soId) {
			var output = null;

			try {
				var lookups = search.lookupFields({
					type: search.Type.SALES_ORDER,
					id: soId,
					columns: ['tranid', 'salesrep.email', 'custbody_pmc_project_coordinator.email', 'custbody_pmc_project_manager.email']
				});

				if (!isEmpty(lookups['tranid'])) {
					output = {
						tranid: lookups['tranid'],
						recips: []
					};

					var salesRep = lookups['salesrep.email'];
					var clientAdv = lookups['custbody_pmc_project_coordinator.email'];
					var projMgr = lookups['custbody_pmc_project_manager.email'];

					if (!isEmpty(salesRep)) {
						output.recips.push(salesRep);
					}
					if (!isEmpty(clientAdv)) {
						output.recips.push(clientAdv);
					}
					if (!isEmpty(projMgr)) {
						output.recips.push(projMgr);
					}
				}
			} catch (e) {
				log.error({
					title: 'getOrderInfo',
					details: {
						soId: soId,
						e: e
					}
				});
			}

			return output;
		}

		function getAttachment(attachmentId) {
			var output = null;

			var attachmentFile = null;

			try {
				attachmentFile = globals.callapi('/Attachment/GetAttachment?id=' + attachmentId, false, 'get');

				log.debug({title: 'attachmentFile', details: attachmentFile});
			} catch (e) {
				log.error({
					title: 'DOWNLOAD_ATTACHMENT',
					details: {
						attachmentId: attachmentId,
						e: e
					}
				});
			}

			var contentDisp = nullIfEmpty(attachmentFile.headers['Content-Disposition']);

			if (attachmentFile != null && contentDisp != null) {
				var attFileName = null;
				try {
					attFileName = getFileName(contentDisp);
					//attFileName = /filename[^;\n]*=(UTF-\d['"]*)?((['"]).*?[.]$\2|[^;\n]*)?/.exec(contentDisp)[2] || null;
					//attFileName = /filename[^;=\n]*=(?:(\\?['"])(.*?)\1|(?:[^\s]+'.*?')?([^;\n]*))/.exec(contentDisp)[2] || null;
				} catch (e) {
					log.error({
						title: 'FILENAME_PARSE',
						details: {
							contentDisp: contentDisp,
							e: e
						}
					});
				}

				var attFileType = mimeFileType(attachmentFile.headers['Content-Type']);

				log.debug({title: 'attFileName', details: attFileName});
				log.debug({title: 'attFileType', details: attFileType});

				if (attFileName != null && attFileType != null) {
					try {
						output = file.create({
							name: attFileName,
							fileType: attFileType,
							contents: attachmentFile.body
						});
					} catch (e) {
						log.error({
							title: 'FILE_CREATE',
							details: {
								attFileName: attFileName,
								attFileType: attFileType,
								e: e
							}
						});
					}
				}
			}

			return output;
		}

		function summarize(context) {
			log.debug({title: 'SUMMARIZE', details: JSON.stringify(context)});
		}

		function isEmpty(stValue) {
			return ((stValue === '' || stValue == null || stValue == undefined) || (stValue.constructor === Array && stValue.length == 0) || (stValue.constructor === Object && (function (v) {
				for (var k in v)
					return false;
				return true;
			})(stValue)));
		}

		function nullIfEmpty(what) {
			return (isEmpty(what) ? null : what);
		}

		function getFileName(contentDisp) {
			var utfPattern = /filename\*=UTF-8''([\w%\-\.]+)(?:; ?|$)/i;
			var asciiPattern = /filename=(["']?)(.*?[^\\])\1(?:; ?|$)/i;

			var output = null;

			if (utfPattern.test(contentDisp)) {
				output = decodeURIComponent(utfPattern.exec(contentDisp)[1]);
			} else {
				var matches = asciiPattern.exec(contentDisp);
				if (matches != null && matches[2]) {
					output = matches[2];
				}
			}

			return output;
		}

		function mimeFileType(contentType) {
			var lookup = {};

			log.debug({title: 'mimeFileType', details: contentType});

			lookup['application/x-autocad'] = file.Type.AUTOCAD;
			lookup['image/x-xbitmap'] = file.Type.BMPIMAGE;
			lookup['text/csv'] = file.Type.CSV;
			lookup['application/vnd.ms-excel'] = file.Type.EXCEL;
			lookup['application/x-shockwave-flash'] = file.Type.FLASH;
			lookup['image/gif'] = file.Type.GIFIMAGE;
			lookup['application/?x-?gzip-?compressed'] = file.Type.GZIP;
			lookup['text/html'] = file.Type.HTMLDOC;
			lookup['image/ico'] = file.Type.ICON;
			lookup['text/javascript'] = file.Type.JAVASCRIPT;
			lookup['image/jpeg'] = file.Type.JPGIMAGE;
			lookup['application/json'] = file.Type.JSON;
			lookup['message/rfc822'] = file.Type.MESSAGERFC;
			lookup['audio/mpeg'] = file.Type.MP3;
			lookup['video/mpeg'] = file.Type.MPEGMOVIE;
			lookup['application/vnd.ms-project'] = file.Type.MSPROJECT;
			lookup['application/pdf'] = file.Type.PDF;
			lookup['image/pjpeg'] = file.Type.PJPGIMAGE;
			lookup['text/plain'] = file.Type.PLAINTEXT;
			lookup['image/x-png'] = file.Type.PNGIMAGE;
			lookup['image/png'] = file.Type.PNGIMAGE;
			lookup['application/postscript'] = file.Type.POSTSCRIPT;
			lookup['application/?vnd.?ms-?powerpoint'] = file.Type.POWERPOINT;
			lookup['video/quicktime'] = file.Type.QUICKTIME;
			lookup['application/rtf'] = file.Type.RTF;
			lookup['application/sms'] = file.Type.SMS;
			lookup['text/css'] = file.Type.STYLESHEET;
			lookup['image/tiff'] = file.Type.TIFFIMAGE;
			lookup['application/vnd.visio'] = file.Type.VISIO;
			lookup['application/msword'] = file.Type.WORD;
			lookup['text/xml'] = file.Type.XMLDOC;
			lookup['application/zip'] = file.Type.ZIP;

			if (lookup.hasOwnProperty(contentType)) {
				return lookup[contentType];
			} else {
				return null;
			}
		}

		return {
			getInputData: getInputData,
			map: map,
			summarize: summarize
		}

	});