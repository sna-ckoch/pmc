/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 * 
 * Script brief description: 
   Called by Box webhooks to trigger a Box File to Service Pro Note record upsert
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/12/07                           ckoch           Initial version
 *
 */
define(['N/http', './sna_pmc_mod_box_globals.js'], function (http, boxGlobals) {

    function onRequest(context) {
        log.debug({ title: 'REQUEST', details: JSON.stringify(context) });

        if (context.request.method == http.Method.POST) {
            handlePost(context);
        } else {
            context.response.write({ output: 'Request not supported' });
        }
    }

    function handlePost(context) {
        var payload = JSON.parse(context.request.body);

        var trigger = payload.trigger;
        var itemId = payload.source.id;
        var itemName = payload.source.name;
        var itemVersion = null;
        if (payload.source.file_version) {
            itemVersion = payload.source.file_version.id;
        }

        var salesOrder = boxGlobals.getOrderFromExtKey(context.request.parameters['custpage_extkey']);

        boxGlobals.createBoxToSPRecord(itemId, itemVersion, itemName, salesOrder, trigger);
    }

    return {
        onRequest: onRequest
    }

});