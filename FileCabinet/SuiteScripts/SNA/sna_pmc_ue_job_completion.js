/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Attempts to link an unlinked job completion record with an item fulfillment
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/03/05                           ckoch           Initial version
 *
 */
define(['N/search', 'N/record'], function (search, record) {

	function afterSubmit(context) {
		var events = {};
		events[context.UserEventType.CREATE] = handleCreate;
		events[context.UserEventType.EDIT] = handleCreate;

		if (typeof events[context.type] === 'function') {
			events[context.type](context);
		}
	}

	function handleCreate(context) {
		var rec = context.newRecord;
		var inspectionId = lookupField(rec.type, rec.id, 'custrecord_sna_pmc_jc_resultid');
		var lineId = lookupField(rec.type, rec.id, 'custrecord_sna_pmc_jc_linenumber');
		var isMatched = lookupField(rec.type, rec.id, 'custrecord_sna_pmc_jc_matched');

		if (lineId != null && isMatched != 'T' && isMatched != true) {
			var fulfillments = findFulfillments(lineId);
			log.debug('fulfillments', fulfillments);

			var matched = false;

			if (fulfillments.length > 0) {
				var values = {};
				values['custbody_sna_pmc_job_completion'] = rec.id;

				var jcPdf = findJobCompletionPdf(' | Job Completion | ' + inspectionId);

				if (jcPdf != null) {
					values['custbody_sna_pmc_job_completion_pdf'] = jcPdf;
				}

				for (var i = 0; i < fulfillments.length; i++) {
					try {
						record.submitFields({
							type: record.Type.ITEM_FULFILLMENT,
							id: fulfillments[i],
							values: values
						});
						matched = true;
					} catch (e) {
						log.error({
							title: 'FULFILLMENT_UPDATE',
							details: e
						});
					}
				}

				if (matched) {
					try {
						record.submitFields({
							type: rec.type,
							id: rec.id,
							values: {
								'custrecord_sna_pmc_jc_matched': true
							}
						});
					} catch (e) {
						log.error({
							title: 'STAGING_UPDATE',
							details: e
						});
					}
				}
			}
		}
	}

	function handleCreateOld(context) {
		var rec = context.newRecord;
		var lineId = lookupField(rec.type, rec.id, 'custrecord_sna_pmc_jc_linenumber');
		var linkedRec = lookupField(rec.type, rec.id, 'custrecord_sna_pmc_jc_itemfulfillment');

		if (lineId != null && linkedRec == null) {
			var ifRec = findFulfillment(lineId);

			if (ifRec != null) {
				try {
					record.submitFields({
						type: rec.type,
						id: rec.id,
						values: {
							'custrecord_sna_pmc_jc_itemfulfillment': ifRec
						}
					});
				} catch (e) {
					log.error({
						title: 'STAGING_UPDATE',
						details: e
					});
				}

				try {
					record.submitFields({
						type: record.Type.ITEM_FULFILLMENT,
						id: ifRec,
						values: {
							'custbody_sna_pmc_job_completion': rec.id
						}
					});
				} catch (e) {
					log.error({
						title: 'FULFILLMENT_UPDATE',
						details: e
					});
				}
			}
		}
	}

	function findFulfillments(lineId) {
		var output = [];

		search.create({
			type: search.Type.SALES_ORDER,
			filters: [
				['custcol_linenumber', 'is', lineId]
			],
			columns: ['memo']
		}).run().each(function (result) {
			var desc = nullIfEmpty(result.getValue({name: 'memo'}));

			if (desc) {
				var parts = desc.split(' | ');

				var tranIds = [];
				parts.map(function (val) {
					var v = nullIfEmpty(val);
					if (v) {
						tranIds.push(v.trim());
					}
				});

				if (tranIds.length > 0) {
					var filters = [];
					filters[0] = [];
					filters[0][0] = ['mainline', 'is', 'T'];
					filters[0][1] = 'and',
					filters[0][2] = [];

					for (var t in tranIds) {
						filters[0][2].push(['numbertext', 'is', tranIds[t]]);
						filters[0][2].push('or');
					}

					filters[0][2].pop();

					search.create({
						type: search.Type.ITEM_FULFILLMENT,
						filters: filters,
						columns: ['internalid']
					}).run().each(function (result) {
						output.push(result.id);
						return true;
					});
				}
			}

			return false;
		});

		return output;
	}

	function findFulfillment(lineId) {
		var output = null;

		search.create({
			type: search.Type.SALES_ORDER,
			filters: [
				['custcol_linenumber', 'is', lineId],
				'and',
				['custcol_sna_linked_fulfillment', 'noneof', '@NONE@']
			],
			columns: ['custcol_sna_linked_fulfillment']
		}).run().each(function (result) {
			output = nullIfEmpty(result.getValue({name: 'custcol_sna_linked_fulfillment'}));

			return false;
		});

		return output;
	}

	function findJobCompletionPdf(searchString) {
		var output = null;

		search.create({
			type: 'customrecord_sna_pmc_sp10_email_to_box',
			filters: [
				['custrecord_sna_pmc_box2sp_email_subject', 'contains', searchString],
				'and',
				['custrecord_sna_pmc_box2sp_email_file', 'noneof', '@NONE@'],
				'and',
				['isinactive', 'is', 'F']
			],
			columns: ['custrecord_sna_pmc_box2sp_email_file']
		}).run().each(function(result){
			output = nullIfEmpty(result.getValue({name: 'custrecord_sna_pmc_box2sp_email_file'}));

			return false;
		});

		return output;
	}

	function isEmpty(stValue) {
		return ((stValue === '' || stValue == null || stValue == undefined) || (stValue.constructor === Array && stValue.length == 0) || (stValue.constructor === Object && (function (v) {
			for (var k in v)
				return false;
			return true;
		})(stValue)));
	}

	function nullIfEmpty(what) {
		return (isEmpty(what) ? null : what);
	}

	function lookupField(recType, recId, fieldId) {
		var output = null;

		try {
			output = search.lookupFields({
				type: recType,
				id: recId,
				columns: [fieldId]
			})[fieldId] || null;

			if (util.isArray(output)) {
				if (output.length == 1) {
					output = nullIfEmpty(output[0].value);
				} else {
					output = null;
				}
			}
		} catch (e) {
			log.error({
				title: 'lookupField',
				details: JSON.stringify({
					recType: recType,
					recId: recId,
					fieldId: fieldId,
					e: e
				})
			});
		}

		log.debug({
			title: 'lookupField',
			details: JSON.stringify({
				recType: recType,
				recId: recId,
				fieldId: fieldId,
				output: output
			})
		});

		return nullIfEmpty(output);
	}

	return {
		afterSubmit: afterSubmit
	}

});