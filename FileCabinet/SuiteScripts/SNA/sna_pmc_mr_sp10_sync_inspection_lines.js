/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Syncs the answers from an inspection to line level fields on transactions
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/01/27                           ckoch           Initial version
 *
 */
define(['N/runtime', 'N/search', 'N/record', 'N/query', 'N/file', './sna_pmc_mod_sp10_globals.js'],
	function (runtime, search, record, query, file, globals) {

		function getInputData(context) {
			var inspections = [];

			var inspectionLinkId = runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_sp10_insp_link_id'}) || null;
			if (inspectionLinkId == null) {
				log.error({title: 'Inspection Link ID parameter is blank. Exiting'});
				return;
			}

			var cacheKey = '__clock_' + inspectionLinkId;

			var lastClock = runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_sp10_insp_clock_over'}) || null;
			if (lastClock == null) {
				lastClock = globals.getCache().get({
					key: cacheKey
				});
			}

			if (lastClock == null) {
				var d = new Date();
				d.setHours(d.getHours() - 24); // default to 24hr lookback if no last run time
				lastClock = d.toISOString();
			}
			log.debug({title: 'LASTCLOCK_FINAL', details: lastClock});

			var maxPages = 20;
			var pages = 0;

			try {
				// get all inspection results since last clock
				//var url = '/InspectionQuestionResult?$filter=InspectionResult/Id eq 139';
				var url = '/InspectionQuestionResult?$filter=(InspectionResult/Inspection/InspectionLinkId eq ' + inspectionLinkId;
				//url += ' and InspectionResult/CompletedDateTime ge ' + lastClock;
				url += ' and ((InspectionResult/CompletedDateTime ge ' + lastClock + ') or ';
				url += ' (InspectionResult/CreatedDateTime ge ' + lastClock + ' and InspectionResult/CompletedDateTime ne null) or ';
				url += ' (InspectionResult/LastUpdatedDateTime ge ' + lastClock + ' and InspectionResult/CompletedDateTime ne null)))';
				url += '&$expand=InspectionResult,InspectionResult/CompletedTech,InspectionResult/OrderLine,InspectionQuestion,InspectionQuestionChoiceResult,InspectionQuestionChoiceResult/InspectionQuestionChoice';

				do {
					log.debug({title: 'GET_INSPECTIONS_REQ', details: url});
					var response = globals.callapi(url, true, 'get');
					log.debug({title: 'GET_INSPECTIONS_RES', details: JSON.stringify(response)});

					// fix and store the clock from this request to be used next time
					var newClock = response.d['__clock'] || null;

					if (newClock != null && pages === 0) {
						var parts = newClock.split(/\D+/);
						if (parts.length == 6) {
							// yyyy-mm-dd-hh-mm-ss UTC
							var d = new Date(Date.UTC(parts[0], --parts[1], parts[2], parts[3], parts[4], parts[5]));
							newClock = d.toISOString();

							log.debug({title: 'NEWCLOCK_FINAL', details: newClock});

							globals.getCache().put({
								key: cacheKey,
								value: newClock
							});
						}
					}

					// pages of 20, gives us url to next page if any
					url = response.d['__next'] || null;

					if (url != null) {
						url = url.replace('/api/', '/'); // callapi already has this bit
						log.debug({title: 'NEXT_FINAL', details: url});
					}

					inspections = inspections.concat(response.d.results);

					pages++;
				} while (url != null && pages < maxPages);
			} catch (e) {
				log.error({title: 'GET_ERROR', details: JSON.stringify(e)});
			}

			log.debug({title: 'INSPECTIONS_LEN', details: inspections.length});

			return inspections;
		}

		function map(context) {
			var result = JSON.parse(context.value);

			var masterResultId = null;
			try {
				masterResultId = result['InspectionResult']['MasterInspectionResultId'] || null;
			} catch (e) {
				log.error({title: 'MAP_INSPECTION', details: JSON.stringify(e)});
			}

			if (masterResultId != null) {
				context.write({
					key: masterResultId,
					value: result
				});
			}
		}

		function reduce(context) {
			var answers = context.values.map(JSON.parse);

			log.debug({title: 'reduce', details: 'answers=' + answers.length});

			updateLineFields(answers);
		}

		function updateLineFields(answers) {
			if (answers.length == 0) {
				log.debug({title: 'No answers. Exiting'});
				return;
			}

			var fieldMapping = null;
			try {
				fieldMapping = JSON.parse(runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_sp10_insp_mapping'}));
			} catch (e) {
				log.error({title: 'PARSE_MAPPING', details: JSON.stringify(e)});
			}
			if (fieldMapping == null) {
				log.debug({title: 'Field Mapping parameter is blank or invalid. Exiting'});
				return;
			}

			var boxQuestionIds = runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_sp10_box_linkids'}) || null;
			if (util.isString(boxQuestionIds)) {
				boxQuestionIds = boxQuestionIds.split(',');
			} else {
				boxQuestionIds = [];
			}

			var lineId = null;
			try {
				lineId = answers[0]['InspectionResult']['OrderLine'][0]['Id'] || null; // all answers will be linked to the same order line
			} catch (e) {
				log.error({
					title: 'updateLineFields.parseLineId',
					details: JSON.stringify(e)
				});
			}
			if (lineId == null) {
				log.debug({title: 'Line id not found. Exiting'});
				return;
			}

			log.debug({title: 'updateLineFields.orderLookup', details: lineId});

			var transactionRecord = getTransactionRec(lineId);
			if (transactionRecord == null) {
				log.debug({title: 'Transaction not found with matching line id. Exiting'});
				return;
			}

			var saveRecord = false;
			var completedByRecId = null;

			if (util.isObject(answers[0]['InspectionResult']['CompletedTech'])) {
				var completedById = nullIfEmpty(answers[0]['InspectionResult']['CompletedTech']['Id']);
				var completedByName = nullIfEmpty(answers[0]['InspectionResult']['CompletedTech']['ServiceTechName']);
				log.debug({
					title: 'updateLineFields.completedBy',
					details: {
						completedById: completedById,
						completedByName: completedByName
					}
				});
				if (!isEmpty(completedById) && !isEmpty(completedByName)) {
					completedByRecId = upsertListRecord(globals.records.serviceTech.id, globals.records.serviceTech.fields.Id, completedById, completedByName)
				}
			}

			for (var i = 0; i < transactionRecord.getLineCount({sublistId: 'item'}); i++) {
				var txnLineId = transactionRecord.getSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_linenumber',
					line: i
				});
				log.debug('check line', i);
				log.debug('check line.txnLineId', txnLineId);
				log.debug('check line.lineId', lineId);
				if (txnLineId == lineId) {
					log.debug({title: 'updateLineFields.foundLine', details: i});

					if (fieldMapping.hasOwnProperty('completedby')) {
						var fieldId = fieldMapping['completedby'];

						if (util.isString(fieldId)) {
							transactionRecord.setSublistValue({
								sublistId: 'item',
								fieldId: fieldId,
								line: i,
								value: completedByRecId
							});
						}
					}

					for (var a in answers) {
						var questionLinkId = nullIfEmpty(answers[a]['InspectionQuestion']['InspectionQuestionLinkId']);

						log.debug({
							title: 'updateLineFields.mapping',
							details: 'Looking for ' + questionLinkId + ' in ' + JSON.stringify(fieldMapping)
						});

						if (fieldMapping.hasOwnProperty(questionLinkId)) {
							var fieldId = fieldMapping[questionLinkId];
							var fieldValue = answers[a]['Value'];

							var choiceList = answers[a]['InspectionQuestionChoiceResult'] || [];
							if (choiceList.length == 1 && util.isObject(fieldMapping[questionLinkId])) {
								var choiceLinkId = nullIfEmpty(choiceList[0]['InspectionQuestionChoice']['InspectionQuestionChoiceLinkId']);
								var choiceValue = nullIfEmpty(choiceList[0]['InspectionQuestionChoice']['Value']);

								fieldId = nullIfEmpty(fieldMapping[questionLinkId]['fieldId']);

								var builtIn = nullIfEmpty(fieldMapping[questionLinkId]['builtIn']);
								var listRecord = nullIfEmpty(fieldMapping[questionLinkId]['recordType']);
								var idField = nullIfEmpty(fieldMapping[questionLinkId]['idField']);

								var listValue = null;

								if (isEmpty(builtIn)) {
									listValue = upsertListRecord(listRecord, idField, choiceLinkId, choiceValue);
								} else {
									if (builtIn == 'expensecategory') {
										listValue = lookupExpenseType(choiceLinkId);
									}
								}

								if (!isEmpty(listValue)) {
									fieldValue = listValue;
								}
							}

							if (util.isString(fieldId)) {
								log.debug({
									title: 'Updating line field value',
									details: fieldId + ' <- ' + fieldValue
								});

								transactionRecord.setSublistValue({
									sublistId: 'item',
									fieldId: fieldId,
									line: i,
									value: fieldValue
								});
							}

							saveRecord = true;
						} else if (boxQuestionIds.indexOf(questionLinkId) > -1) {
							var attachmentId = nullIfEmpty(answers[a]['AttachmentId']);

							if (attachmentId != null) {
								try {
									var fileId = getAttachment(attachmentId);

									var tranId = transactionRecord.getValue({fieldId: 'tranid'});
									var resultId = answers[a]['InspectionResult']['Id'];

									record.create({
										type: 'customrecord_sna_pmc_sp10_email_to_box'
									}).setValue({
										fieldId: 'custrecord_sna_pmc_box2sp_email_from',
										value: 'inspections@servicepro10.com'
									}).setValue({
										fieldId: 'custrecord_sna_pmc_box2sp_email_subject',
										value: 'INSPECTION | ' + tranId + ' | ' + resultId
									}).setValue({
										fieldId: 'custrecord_sna_pmc_box2sp_email_message',
										value: 'Inspection Attachment'
									}).setValue({
										fieldId: 'custrecord_sna_pmc_box2sp_email_file',
										value: fileId
									}).save();
								} catch (e) {
									log.error({
										title: 'CREATE_SP2BOX_REC',
										details: {
											attachmentId: attachmentId,
											fileId: fileId,
											e: e
										}
									});
								}
							}
						}
					}
				}
			}

			log.debug({title: 'updateLineFields.saveRecord', details: saveRecord + ', ' + transactionRecord.id});

			if (saveRecord) {
				try {
					transactionRecord.save();
				} catch (e) {
					log.error({
						title: 'SAVE_TRANSACTION',
						details: JSON.stringify(e)
					});
				}
			}
		}

		function getAttachment(attachmentId) {
			var output = null;

			var folderId = nullIfEmpty(runtime.getCurrentScript().getParameter({name: 'custscript_sna_pmc_sp10_temp_folderid'}));
			if (isEmpty(folderId)) {
				return;
			}

			var attachmentFile = null;

			try {
				attachmentFile = globals.callapi('/Attachment/GetAttachment?id=' + attachmentId, false, 'get');
			} catch (e) {
				log.error({
					title: 'DOWNLOAD_ATTACHMENT',
					details: {
						attachmentId: attachmentId,
						e: e
					}
				});
			}

			if (!isEmpty(attachmentFile) && !isEmpty(attachmentFile.headers['Content-Disposition'])) {
				var attFileName = null;
				try {
					attFileName = /filename[^;=\n]*=(?:(\\?['"])(.*?)\1|(?:[^\s]+'.*?')?([^;\n]*))/.exec(attachmentFile.headers['Content-Disposition'])[3] || null;
					//attFileName = attachmentId + '_' + /filename="?(.[^"]*)"?/.exec(attachmentFile.headers['Content-Disposition'])[1];
				} catch (e) {
					log.error({
						title: 'FILENAME_PARSE',
						details: {
							contentDisp: attachmentFile.headers['Content-Disposition'],
							e: e
						}
					});
				}

				var attFileType = mimeFileType(attachmentFile.headers['Content-Type']);
				log.debug('attFileName', attFileName);
				log.debug('attFileType', attFileType);
				if (attFileName != null && attFileType != null) {
					try {
						output = file.create({
							folder: Number(folderId),
							name: attFileName,
							fileType: attFileType,
							contents: attachmentFile.body
						}).save();
					} catch (e) {
						log.error({
							title: 'FILE_CREATE',
							details: JSON.stringify({
								attFileName: attFileName,
								attFileType: attFileType,
								e: e
							})
						});
					}
				}
			}

			return output;
		}

		function lookupExpenseType(linkId) {
			var output = null;

			try {
				var ql = 'SELECT ExpenseCategory.id FROM ExpenseCategory WHERE ExpenseCategory.custrecord_sna_pmc_sp10_exp_cat_id = ?';

				var results = query.runSuiteQL({
					query: ql,
					params: [linkId]
				}).asMappedResults();

				if (results.length == 1) {
					output = results[0].id;
				}
			} catch (e) {
				log.error({
					title: 'LOOKUP_EXPENSE_TYPE',
					details: {
						linkId: linkId,
						e: e
					}
				});
			}

			return output;
		}

		function upsertListRecord(listRecord, idField, choiceLinkId, choiceValue) {
			var output = null;

			if (!isEmpty(listRecord) && !isEmpty(idField) && !isEmpty(choiceLinkId) && !isEmpty(choiceValue)) {
				var recid = null;

				search.create({
					type: listRecord,
					filters: [
						[idField, 'is', choiceLinkId]
					],
					columns: ['internalid']
				}).run().each(function (result) {
					output = result.id;
					return false;
				});

				if (isEmpty(output)) {
					try {
						output = record.create({
							type: listRecord
						}).setValue({
							fieldId: 'name',
							value: choiceValue
						}).setValue({
							fieldId: idField,
							value: choiceLinkId
						}).save();
					} catch (e) {
						log.error({
							title: 'UPSERT_LISTRECORD',
							details: {
								listRecord: listRecord,
								idField: idField,
								choiceLinkId: choiceLinkId,
								choiceValue: choiceValue,
								e: e
							}
						});
					}
				}
			}

			return output;
		}

		function getTransactionRec(lineId) {
			var output = null;

			var txnId = null;

			search.create({
				type: search.Type.SALES_ORDER,
				filters: [
					['custcol_linenumber', 'is', lineId]
				],
				columns: ['internalid']
			}).run().each(function (result) {
				if (txnId == null) {
					txnId = result.id;
					return true;
				} else {
					txnId = null; // if more than 1 result, set it back to null so we dont update anything
					return false;
				}
			});

			log.debug({title: 'getTransactionRec.txnId', details: lineId + ' -> ' + txnId});

			if (txnId != null) {
				output = record.load({
					type: record.Type.SALES_ORDER,
					id: txnId
				});
			}

			return output;
		}

		function summarize(context) {
			log.debug({title: 'SUMMARIZE', details: JSON.stringify(context)});
		}

		function isEmpty(stValue) {
			return ((stValue === '' || stValue == null || stValue == undefined) || (stValue.constructor === Array && stValue.length == 0) || (stValue.constructor === Object && (function (v) {
				for (var k in v)
					return false;
				return true;
			})(stValue)));
		}

		function nullIfEmpty(what) {
			return (isEmpty(what) ? null : what);
		}

		function mimeFileType(contentType) {
			var lookup = {};

			log.debug({title: 'mimeFileType', details: contentType});

			lookup['application/x-autocad'] = file.Type.AUTOCAD;
			lookup['image/x-xbitmap'] = file.Type.BMPIMAGE;
			lookup['text/csv'] = file.Type.CSV;
			lookup['application/vnd.ms-excel'] = file.Type.EXCEL;
			lookup['application/x-shockwave-flash'] = file.Type.FLASH;
			lookup['image/gif'] = file.Type.GIFIMAGE;
			lookup['application/?x-?gzip-?compressed'] = file.Type.GZIP;
			lookup['text/html'] = file.Type.HTMLDOC;
			lookup['image/ico'] = file.Type.ICON;
			lookup['text/javascript'] = file.Type.JAVASCRIPT;
			lookup['image/jpeg'] = file.Type.JPGIMAGE;
			lookup['application/json'] = file.Type.JSON;
			lookup['message/rfc822'] = file.Type.MESSAGERFC;
			lookup['audio/mpeg'] = file.Type.MP3;
			lookup['video/mpeg'] = file.Type.MPEGMOVIE;
			lookup['application/vnd.ms-project'] = file.Type.MSPROJECT;
			lookup['application/pdf'] = file.Type.PDF;
			lookup['image/pjpeg'] = file.Type.PJPGIMAGE;
			lookup['text/plain'] = file.Type.PLAINTEXT;
			lookup['image/x-png'] = file.Type.PNGIMAGE;
			lookup['application/postscript'] = file.Type.POSTSCRIPT;
			lookup['application/?vnd.?ms-?powerpoint'] = file.Type.POWERPOINT;
			lookup['video/quicktime'] = file.Type.QUICKTIME;
			lookup['application/rtf'] = file.Type.RTF;
			lookup['application/sms'] = file.Type.SMS;
			lookup['text/css'] = file.Type.STYLESHEET;
			lookup['image/tiff'] = file.Type.TIFFIMAGE;
			lookup['application/vnd.visio'] = file.Type.VISIO;
			lookup['application/msword'] = file.Type.WORD;
			lookup['text/xml'] = file.Type.XMLDOC;
			lookup['application/zip'] = file.Type.ZIP;

			if (lookup.hasOwnProperty(contentType)) {
				return lookup[contentType];
			} else {
				return null;
			}
		}

		return {
			getInputData: getInputData,
			map: map,
			reduce: reduce,
			summarize: summarize
		}

	});