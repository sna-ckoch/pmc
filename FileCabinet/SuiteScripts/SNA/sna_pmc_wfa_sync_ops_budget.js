/**
 * Copyright (c) 2021, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType WorkflowActionScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Looks up OPS BUDGET values from primary order (created from) and updates
 * WO OPS BUDGET field if different
 * "PRIMARY SO LINE ID" on WO = "LINE NUMBER" on primary
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2021/06/21                           ckoch           Initial version
 *
 */
define(['N/record', 'N/search'], function (record, search) {

	function onAction(context) {
		var primaryLines = getPrimaryLines(context);

		log.debug({
			title: 'onAction.primaryLines',
			details: primaryLines
		});

		if (Object.keys(primaryLines).length > 0) {
			var rec = record.load({
				type: context.newRecord.type,
				id: context.newRecord.id
			});

			var saveRecord = false;

			var lineCount = rec.getLineCount({sublistId: 'item'});

			log.debug({
				title: 'onAction.lineCount',
				details: lineCount
			});

			for (var i = 0; i < lineCount; i++) {
				var parentLine = rec.getSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_esp_primary_so_line_id',
					line: i
				});

				if (primaryLines.hasOwnProperty(parentLine)) {
					var primaryBudget = primaryLines[parentLine];

					var woBudget = rec.getSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_esp_wo_ops_budget',
						line: i
					});

					log.debug({
						title: 'onAction.checkLine',
						details: woBudget + ' == ' + primaryBudget + '?'
					});

					if (primaryBudget != woBudget) {
						log.debug({
							title: 'onAction.updateLine',
							details: 'updating line ' + i + ' budget val from ' + woBudget + ' to ' + primaryBudget
						});

						rec.setSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_esp_wo_ops_budget',
							value: primaryBudget,
							line: i
						});

						saveRecord = true;
					}
				}
			}

			if (saveRecord) {
				try {
					rec.save({
						ignoreMandatoryFields: true
					});
				} catch (e) {
					log.error({
						title: 'onAction.save',
						details: e
					});
				}
			}
		}
	}

	function getPrimaryLines(context) {
		var output = {};

		var createdFrom = null;
		try {
			createdFrom = context.newRecord.getValue({fieldId: 'custbody_esp_parent_order'}) || null;
		} catch (e) {
			log.debug({
				title: 'getPrimaryLines',
				details: e
			});
		}

		if (createdFrom != null) {
			search.create({
				type: search.Type.SALES_ORDER,
				filters: [
					['internalid', 'anyof', createdFrom],
					'and',
					['custcol_pmc_line_number', 'isnotempty', '']
				],
				columns: [
					'line',
					'custcol_pmc_ops_budget'
				]
			}).run().each(function(result){
				var lineNum = result.getValue({name: 'line'}) || null;
				var opsBudget =forceFloat(result.getValue({name: 'custcol_pmc_ops_budget'}));

				output[lineNum] = opsBudget;

				return true;
			});
		}

		return output;
	}

	function forceFloat(what) {
		return (isNaN(parseFloat(what)) ? 0 : parseFloat(what));
	}

	return {
		onAction: onAction
	}

});