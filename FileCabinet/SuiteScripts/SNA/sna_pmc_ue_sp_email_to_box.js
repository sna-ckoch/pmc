/**
 * Copyright (c) 2020, ScaleNorth Advisors LLC and/or its affiliates. All rights reserved.
 *
 * @author ckoch
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 *
 * Script brief description:
 * Uploads files from "Service Pro Email to Box" to box
 * Email capture plugin creates these records
 * This script will search "Box Record Folder" records to find the box folder associated with the order
 *  - If no "Service Pro In" folder exists within, one will be created
 * File from this record will be uploaded to "Service Pro In", then deleted from the file cabinet if successful
 *
 * Revision History:
 *
 * Date              Issue/Case         Author          Issue Fix Summary
 * =============================================================================================
 * 2020/12/15                           ckoch           Initial version
 *
 */
define(['N/search', 'N/record', 'N/email', 'N/file', 'N/runtime', './sna_pmc_mod_box_globals.js'],
	function (search, record, email, file, runtime, boxGlobals) {

		function beforeSubmit(context) {
			if (context.type != context.UserEventType.CREATE && context.type != context.UserEventType.EDIT) return;

			var rec = context.newRecord;

			var send2box = rec.getValue({fieldId: 'custrecord_sna_pmc_box2sp_send2box'}) || false;

			var salesOrder = rec.getValue({fieldId: boxGlobals.records.spToBox.fields.salesOrder}) || null;
			var subj = rec.getValue({fieldId: boxGlobals.records.spToBox.fields.emailSubject}) || null;
			var matches = /SO\d+/.exec(subj);

			if (matches && !salesOrder) {
				var tranId = matches[0];

				var results = search.create({
					type: search.Type.SALES_ORDER,
					filters: [
						['numbertext', 'is', tranId],
						'and',
						['mainline', 'is', 'T']
					],
					columns: ['internalid']
				}).runPaged();

				if (results.count == 1) {
					salesOrder = results.fetch({
						index: 0
					}).data[0].id;

					if (salesOrder) {
						rec.setValue({
							fieldId: boxGlobals.records.spToBox.fields.salesOrder,
							value: salesOrder
						});
					}
				}
			}

			if (salesOrder && send2box) {
				try {
					processRecord(rec);
				} catch (e) {
					log.error({
						title: 'processRecord',
						details: {
							recId: salesOrder,
							e: e
						}
					});
				}
			}
		}

		function processRecord(rec) {
			var salesOrderId = rec.getValue({fieldId: boxGlobals.records.spToBox.fields.salesOrder}) || null;
			var fileId = rec.getValue({fieldId: boxGlobals.records.spToBox.fields.emailAttachment}) || null;
			var uploadEmail = rec.getValue({fieldId: boxGlobals.records.spToBox.fields.boxUploadEmail}) || null;

			if (salesOrderId != null && fileId != null && uploadEmail == null) {
				var boxFolderId = boxGlobals.getBoxFolderId('salesorder', salesOrderId);

				if (boxFolderId == null) {
					boxFolderId = createNewBoxFolder(salesOrderId);
				}

				var inboundFolder = getOrCreateInboundFolder(boxFolderId);

				if (inboundFolder.id != null && inboundFolder.email != null) {
					rec.setValue({
						fieldId: boxGlobals.records.spToBox.fields.boxFolderId,
						value: inboundFolder.id
					});

					rec.setValue({
						fieldId: boxGlobals.records.spToBox.fields.boxUploadEmail,
						value: inboundFolder.email
					});

					email.send({
						author: runtime.getCurrentScript().getParameter({
							name: 'custscript_sna_pmc_box_email_sender'
						}),
						recipients: [
							inboundFolder.email
						],
						subject: rec.getValue({
							fieldId: boxGlobals.records.spToBox.fields.emailSubject
						}) || 'Service Pro File',
						body: rec.getValue({
							fieldId: boxGlobals.records.spToBox.fields.emailMessage
						}) || 'Service Pro File',
						attachments: [
							file.load({
								id: fileId
							})
						]
					});
				}
			}
		}

		function afterSubmit(context) {
			if (context.type != context.UserEventType.CREATE && context.type != context.UserEventType.EDIT) return;

			var rec = context.newRecord;

			var salesOrderId = rec.getValue({fieldId: boxGlobals.records.spToBox.fields.salesOrder}) || null;
			var fileId = rec.getValue({fieldId: boxGlobals.records.spToBox.fields.emailAttachment}) || null;
			var uploadEmail = rec.getValue({fieldId: boxGlobals.records.spToBox.fields.boxUploadEmail}) || null;

			if (salesOrderId != null && fileId != null && uploadEmail == null) {
				var boxFolderId = boxGlobals.getBoxFolderId('salesorder', salesOrderId);

				if (boxFolderId == null) {
					boxFolderId = createNewBoxFolder(salesOrderId);
				}

				var inboundFolder = getOrCreateInboundFolder(boxFolderId);

				if (inboundFolder.id != null && inboundFolder.email != null) {
					email.send({
						author: runtime.getCurrentScript().getParameter({
							name: 'custscript_sna_pmc_box_email_sender'
						}),
						recipients: [
							inboundFolder.email
						],
						subject: rec.getValue({
							fieldId: boxGlobals.records.spToBox.fields.emailSubject
						}) || 'Service Pro File',
						body: rec.getValue({
							fieldId: boxGlobals.records.spToBox.fields.emailMessage
						}) || 'Service Pro File',
						attachments: [
							file.load({
								id: fileId
							})
						]
					});

					var values = {};
					values[boxGlobals.records.spToBox.fields.boxFolderId] = inboundFolder.id;
					values[boxGlobals.records.spToBox.fields.boxUploadEmail] = inboundFolder.email;

					record.submitFields({
						type: rec.type,
						id: rec.id,
						values: values
					});

					// TODO delete file
				}
			}
		}

		function createNewBoxFolder(salesOrderId) {
			var output = null;

			var tranId = getTranId(salesOrderId);

			if (tranId) {
				var parentFolderId = runtime.getCurrentScript().getParameter({
					name: 'custscript_sna_pmc_box_parent_folderid'
				});

				var orderFolder = boxGlobals.makeFolder(tranId, parentFolderId);

				output = (orderFolder || {}).id || null;

				if (output != null) {
					try {
						record.create({
							type: boxGlobals.records.boxRecordFolder.id
						}).setValue({
							fieldId: boxGlobals.records.boxRecordFolder.fields.recordType,
							value: 'salesorder'
						}).setValue({
							fieldId: boxGlobals.records.boxRecordFolder.fields.recordId,
							value: salesOrderId
						}).setValue({
							fieldId: boxGlobals.records.boxRecordFolder.fields.folderId,
							value: output
						}).save();
					} catch (e) {
						log.error({
							title: 'CREATE_BOX_FOLDER_REC',
							details: JSON.stringify({
								salesOrderId: salesOrderId,
								e: JSON.stringify(e)
							})
						});
					}
				}
			}

			return output;
		}

		function getTranId(salesOrderId) {
			var output = null;

			try {
				output = search.lookupFields({
					type: search.Type.SALES_ORDER,
					id: salesOrderId,
					columns: ['tranid']
				})['tranid'] || null;
			} catch (e) {
				log.debug({
					title: 'GET_TRANID',
					details: JSON.stringify({
						salesOrderId: salesOrderId,
						e: JSON.stringify(e)
					})
				});
			}

			return output;
		}

		function getOrCreateInboundFolder(boxFolderId) {
			var output = {
				id: null,
				email: null
			};

			if (boxFolderId != null) {
				try {
					var inbound = getInboundFolder(boxFolderId);

					if (inbound == null) {
						var inboundFolder = runtime.getCurrentScript().getParameter({
							name: 'custscript_sna_pmc_box_inbound_folder'
						}) || 'Service Pro In';

						var response = boxGlobals.makeFolder(inboundFolder, boxFolderId, true);

						inbound = {
							id: response.id,
							email: (response.folder_upload_email || {}).email
						}
					}

					if (inbound != null) {
						output = inbound;
					}
				} catch (e) {
					log.error({
						title: 'GET_OR_CREATE_INBOUND_FOLDER',
						details: JSON.stringify({
							boxFolderId: boxFolderId,
							e: JSON.stringify(e)
						})
					});
				}
			}

			return output;
		}

		// TODO change to get-or-make type
		function getInboundFolder(boxFolderId) {
			var output = null;

			if (boxFolderId != null) {
				var folderContents = boxGlobals.listFolder(boxFolderId);

				if (util.isArray(folderContents['entries'])) {
					var inboundFolder = runtime.getCurrentScript().getParameter({
						name: 'custscript_sna_pmc_box_inbound_folder'
					}) || 'Service Pro In';

					for (var f in folderContents.entries) {
						var itemType = folderContents.entries[f].type || null;
						var itemName = folderContents.entries[f].name || '';

						if (itemType == 'folder' && itemName.toLowerCase() == inboundFolder.toLowerCase()) {
							var folderInfo = boxGlobals.getFolderInfo(folderContents.entries[f].id);

							output = {
								id: folderInfo.id,
								email: (folderInfo.folder_upload_email || {}).email
							}
						}
					}
				}
			}

			return output;
		}

		return {
			beforeSubmit: beforeSubmit
		}

	});