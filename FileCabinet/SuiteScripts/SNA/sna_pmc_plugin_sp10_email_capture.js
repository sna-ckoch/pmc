function process(email) {
    var TEMP_FOLDER_ID = 4202910; // CHANGE ME

	nlapiLogExecution('DEBUG','EMAIL START','-------------------------------------------------------------------');
	nlapiLogExecution('DEBUG','from',);

	var to = email.getTo();
	for (var indexTo in to) {
		nlapiLogExecution('DEBUG','to['+indexTo+']',JSON.stringify(to[indexTo]));
	}

	var cc = email.getCc();
   	for (var indexCc in cc) {
		nlapiLogExecution('DEBUG','cc['+indexCc+']',JSON.stringify(cc[indexCc]));
	}

	nlapiLogExecution('DEBUG','replyTo',JSON.stringify(email.getReplyTo()));
	nlapiLogExecution('DEBUG','sentDate',email.getSentDate());
	nlapiLogExecution('DEBUG','subject',email.getSubject());
	nlapiLogExecution('DEBUG','textBody',email.getTextBody());
	nlapiLogExecution('DEBUG','htmlBody',email.getHtmlBody());

	var attachments = email.getAttachments();
	for (var indexAtt in attachments) {
		nlapiLogExecution('DEBUG', 'attachments['+indexAtt+']', 'Name: ' + attachments[indexAtt].getName() + ', Type: ' + attachments[indexAtt].getType() + ', Content Length: ' + attachments[indexAtt].getValue().length);
	}

    nlapiLogExecution('DEBUG','EMAIL END','-------------------------------------------------------------------');
    nlapiLogExecution('ERROR','email',JSON.stringify(email));
    try {
        var rec = nlapiCreateRecord('customrecord_sna_pmc_sp10_email_to_box');

        rec.setFieldValue('custrecord_sna_pmc_box2sp_email_from', email.getFrom());
        rec.setFieldValue('custrecord_sna_pmc_box2sp_email_subject', email.getSubject());
        rec.setFieldValue('custrecord_sna_pmc_box2sp_email_message', email.getHtmlBody());
        
        if (attachments.length > 0) {
            var fileRec = nlapiCreateFile(attachments[0].getName(), attachments[0].getType(), attachments[0].getValue());
            fileRec.setFolder(TEMP_FOLDER_ID);

            fileId = nlapiSubmitFile(fileRec);

            rec.setFieldValue('custrecord_sna_pmc_box2sp_email_file', fileId);
        }

        nlapiSubmitRecord(rec);
    } catch (e) {
        nlapiLogExecution('ERROR','email',JSON.stringify(email));
        nlapiLogExecution('ERROR','error',JSON.stringify(e));
    }
}